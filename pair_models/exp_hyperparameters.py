'''Run experiment with certain values of hyperparameters

Usage:
    exp-hyperparameters.py [options]
    
Options:
    --aggregate=<agg>    Aggregate function to summarize f_j (either max or sum)
    --activate=<act>     Activation function for the neural network (cube, sigmoid or tanh)
    --dropprob=<dp>      The dropout rate (0 means no dropout)
    --use_coref=<b>      Turn on non-pronoun coreferent expression (True or False)
    --learning_rate=<lr> Speed of training (a float number between 0 and 0.1)
    --seed=<s>           A random seed to make sure that the result is deterministic
'''

from pair_models import model
from pair_models.model import cube_func, Model
import isrl
from pair_models.extract_ontonotes_frames import cols
from theano import tensor as T
import os
from docopt import docopt

def translate_example_with_coref(e, frame_and_role, other_roles, candidates, batch_size):
    pr = frame_and_role[e[cols.EXAMPLE_FRAME_AND_ROLE], :cols.FR_ROLE+1]
    rg = other_roles[e[cols.EXAMPLE_OTHER_ROLES_START]:e[cols.EXAMPLE_OTHER_ROLES_END], :cols.OTHER_ROLES_COREF+1]
    c = candidates[e[cols.EXAMPLE_CANDIDATE_START]:e[cols.EXAMPLE_CANDIDATE_END], :cols.CANDIDATE_COREF+1]
    y = e[cols.EXAMPLE_GOLD_ANSWER]
    return pr, rg, c, y, batch_size

def translate_example_without_coref(e, frame_and_role, other_roles, candidates, batch_size):
    pr = frame_and_role[e[cols.EXAMPLE_FRAME_AND_ROLE], :cols.FR_ROLE+1]
    rg = other_roles[e[cols.EXAMPLE_OTHER_ROLES_START]:e[cols.EXAMPLE_OTHER_ROLES_END], :cols.OTHER_ROLES_COREF]
    c = candidates[e[cols.EXAMPLE_CANDIDATE_START]:e[cols.EXAMPLE_CANDIDATE_END], :cols.CANDIDATE_COREF]
    y = e[cols.EXAMPLE_GOLD_ANSWER]
    return pr, rg, c, y, batch_size

if __name__ == '__main__':
    arguments = docopt(__doc__)
    print('Arguments: ', arguments) 
    isrl.setup_experiment(int(arguments['--seed']))
    
    if arguments['--use_coref'].lower() == 'true':
        num_filler_feats = num_role_feats = 2
        translate_example_func = translate_example_with_coref
    else:
        assert arguments['--use_coref'].lower() == 'false', 'Incorrect value for use_coref'
        num_filler_feats = num_role_feats = 1
        translate_example_func = translate_example_without_coref
        
    if arguments['--activate'] == 'cube':
        activation_func = cube_func
    elif arguments['--activate'] == 'sigmoid':
        activation_func = T.nnet.sigmoid
    elif arguments['--activate'] == 'tanh':
        activation_func = T.tanh
    else:
        raise ValueError("Unknown activation function: '%s'" %arguments['--activate'])
    
    if arguments['--aggregate'] == 'sum':
        aggregate_func = T.sum
    elif arguments['--aggregate'] == 'max':
        aggregate_func = T.max
    else:
        raise ValueError("Unknown aggregate function: '%s'" %arguments['--aggregate'])
    
    dropprob = float(arguments['--dropprob'])
    model.learning_rate = float(arguments['--learning_rate'])
    m = Model(False, activation_func, aggregate_func, dropprob)
#     model.max_epochs = 1 # for debugging
    model_name = ('coherence_model-lr_%f-' %model.learning_rate + 
                  '-'.join(str(arguments[k]) for k in sorted(arguments)))
    model_path = os.path.join(isrl.out_dir, model_name + '.npz')
    best_acc = model.train(m, translate_example_func, model_path, num_filler_feats, num_role_feats)
    print('Best accuracy: ', best_acc)