import random
import isrl
import os
import subprocess
import re
import csv
import sys
from tabulate import tabulate

num_processes = 8

if __name__ == '__main__':
    isrl.setup_experiment(3920523)
#     model.max_epochs = 5 # for debugging
    experiments = [('max', 'cube', 0, False),
                   ('max', 'cube', 0.25, False),
                   ('max', 'cube', 0.5, False),
                   ('max', 'tanh', 0, False),
                   ('max', 'sigmoid', 0, False),
                   ('max', 'cube', 0, True),
                   ('max', 'cube', 0.25, True),
                   ('max', 'cube', 0.5, True),
                   ('sum', 'cube', 0, False),
                   ('sum', 'cube', 0.25, False),
                   ('sum', 'cube', 0.5, False),
                   ('sum', 'tanh', 0, False),
                   ('sum', 'sigmoid', 0, False),]
    # add random hyperparameters
    experiments2 = []
    count = 0
    for ex in experiments:
        for _ in range(3):
            count += 1
            lr = 10 ** (random.random()-2)
            seed = random.randint(0, 1000000)
            path = os.path.join(isrl.out_dir, 'exp_hyperparameters-%02d.out' %count)
            experiments2.append(ex + (lr, seed, path))
    # run experiments in parallel
    for i in range(0, len(experiments2), num_processes):
        processes = []
        for j in range(i, min(i+num_processes, len(experiments2))):
            cmd = ('python3 -m pair_models.exp_hyperparameters --aggregate=%s '
                   '--activate=%s --dropprob=%f --use_coref %s '
                   '--learning_rate %f --seed %d > %s 2>&1' %experiments2[j])
            print(cmd)
            processes.append(subprocess.Popen(cmd, shell=True))
        for p in processes:
            p.wait()
    # get back results
    results = [('aggregate_func', 'activate_func', 'dropprob', 'learning_rate', 
                'seed', 'best_accuracy')]
    for ex in experiments2:
        path = ex[-1]
        with open(path) as f:
            s = f.read()
        m = re.search(r'Best accuracy: ([\.\d]+)$', s)
        if m:
            results.append(ex[:-1] + (m.group(1),))
        else:
            results.append(ex[:-1] + ('NaN',))
    # present results
    writer = csv.writer(sys.stdout)
    writer.writerows(results)
    print(tabulate(results, tablefmt="latex"))