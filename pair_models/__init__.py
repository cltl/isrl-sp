import os
from feizabadi_pado import candidate_func

class BaseDNIModelWrapper(object):
    
    def __init__(self, predict_func, indexer):
        self.predict_func = predict_func
        self.indexer = indexer
        self.candidate_func = candidate_func
    
    def __call__(self, doc, frame_elem, pos, role, overt_roles, overt_fillers):
        candidates = list(self.candidate_func(doc, frame_elem, overt_fillers))
        i = self.predict(doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates)
        if i is not None:
            return candidates[i].get('id') 
        return None