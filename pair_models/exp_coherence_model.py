"""Run experiments on coherence (MultiWay) models 

Usage:
  exp_coherence_model.py <random_seed> [--model=<path>]
"""
from docopt import docopt
from pair_models import model, BaseDNIModelWrapper
from pair_models.model import model_func
from pair_models.extract_ontonotes_frames import cols
from data import mark_role

def translate_example(e, frame_and_role, other_roles, candidates, batch_size):
    pr = frame_and_role[e[cols.EXAMPLE_FRAME_AND_ROLE], :cols.FR_ROLE+1]
    rg = other_roles[e[cols.EXAMPLE_OTHER_ROLES_START]:e[cols.EXAMPLE_OTHER_ROLES_END], :cols.OTHER_ROLES_COREF+1]
    c = candidates[e[cols.EXAMPLE_CANDIDATE_START]:e[cols.EXAMPLE_CANDIDATE_END], :cols.CANDIDATE_COREF+1]
    y = e[cols.EXAMPLE_GOLD_ANSWER]
    return pr, rg, c, y, batch_size

class ModelWrapper(BaseDNIModelWrapper):
    
    def predict(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        pr = [frame_elem.get('name'), mark_role(role)]
        rg = [[mark_role(r), doc.head_word(g), doc.nonpronoun_coref(g)] 
              for r, g in zip(overt_roles, overt_fillers)]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] for arg in candidates]
        return self.predict_func(self.indexer.index(pr),
                                 self.indexer.index(rg, ndmin=2), 
                                 self.indexer.index(c))

model_name = 'coherence-model'

if __name__ == '__main__':
    arguments = docopt(__doc__)
    import isrl
    isrl.setup_experiment(arguments['<random_seed>'])
    model_path = arguments.get('--model')
    if not model_path:
        model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)
        model.learning_rate = 0.01
        model.train(model_func(), translate_example, model_path, 2, 2)
    print('Model: ' + model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
    isrl.on5v_cv.resolve_dnis(m, model_name)
    