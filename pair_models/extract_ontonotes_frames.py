import numpy as np
import pickle
from data import Indexer, mark_role, get_wordnet_pos, lemmatizer
import ontonotes
import features
import ptb
from ontonotes import ontonotes_en, dev_docs, OntonotesDocument
import os
from isrl import version

class Paths(object):
    def __init__(self, root_dir):
        self.index_path = os.path.join(root_dir, 'ontonotes.index')
        self.examples_path = os.path.join(root_dir, 'ontonotes-examples.npz')

class __cols(object):
    def __init__(self):
        self.EXAMPLE_FRAME_AND_ROLE = 0
        self.EXAMPLE_OTHER_ROLES_START = 1
        self.EXAMPLE_OTHER_ROLES_END = 2
        self.EXAMPLE_CANDIDATE_START = 3
        self.EXAMPLE_CANDIDATE_END = 4
        self.EXAMPLE_GOLD_ANSWER = 5

        self.CANDIDATE_TOKEN = 0
        self.CANDIDATE_COREF = 1
        self.CANDIDATE_FEATURES_START = 2
        self.CANDIDATE_FEATURES_END = 7
        
        self.OTHER_ROLES_ROLE = 0
        self.OTHER_ROLES_TOKEN = 1
        self.OTHER_ROLES_COREF = 2

        self.FR_FRAME = 0
        self.FR_ROLE = 1
        self.FR_EXPECTED_ROLES = 2
        
cols = __cols()

def synsem_features_ontonotes(doc, sent, pred, arg, frame, role):
    '''
    A version of features.synsem_features that work with OntoNotes data format
    '''
    word = doc.deps()[sent][arg]['token']
    pos = doc.deps()[sent][arg]['pos']
    lemma = lemmatizer.lemmatize(word, get_wordnet_pos(pos) or 'n')
    return [features.expected_roles(None, None, None, frame, None), 
            doc.supersense_of(sent, arg) or '__NULL__',
            #lemma is not available in OntoNotes
            features.candidate_lemma_frequency_semeval.from_lemma(lemma), 
            doc.deps()[sent][arg]['pos'],
            ptb.find_constituent(doc.deps()[sent][arg]['tokens'], doc.trees()[sent]).label()
            ]

def non_pronoun_coref(doc, sent, head):
    '''
    Return the first non-pronoun coreferring word or, if there's none, return
    the provided word itself. 
    '''
    chain = doc.coref().get((sent, head))
    deps = doc.deps()
    if chain:
        for sent2, head2 in chain:
            if 'PRP' not in deps[sent2][head2]['pos']:
                return deps[sent2][head2]['token']
    return deps[sent][head]['token']

if __name__ == '__main__':
    root_dir = os.path.join('out', 'ontonotes-frames.%s' %version)
    os.makedirs(root_dir, exist_ok=True)
    paths = Paths(root_dir)
    
    frame_and_role = []
    other_roles = []
    candidates = []
    examples = []
    
    indexer = Indexer()
    doc_count = 0
    frame_count = 0
    print('Extracting from %s' %ontonotes_en)
    
    for base_path in ontonotes.list_docs(ontonotes_en):
        doc = OntonotesDocument(base_path)
        if os.path.relpath(doc.base_path, ontonotes_en) in dev_docs:
            print('Ignored one document because it is in dev set')
            continue
        for f in doc.iter_frames():
            if f.frame_elements:
                frame = f.predicate                  
                for i, fe in enumerate(f.frame_elements):
                    coreferring_heads = fe.filler_heads
                    if len(coreferring_heads) >= 1:
                        frame_and_role_index = len(frame_and_role)
                        frame_and_role.append(indexer.index([frame, mark_role(fe.role),
                                                             features.expected_roles(None, None, None, frame, None),]))
                        other_roles_start = len(other_roles)
                        other_roles_ids = (j for j in range(len(f.frame_elements)) if j != i)
                        for j in other_roles_ids:
                            for other_head in f.frame_elements[j].filler_heads:
                                role_j = f.frame_elements[j].role
                                head_token = doc.deps()[f.sent][other_head]['token']
                                other_coref = non_pronoun_coref(doc, f.sent, other_head)
                                ssf = synsem_features_ontonotes(doc, f.sent, None, other_head, frame, role_j)
                                other_roles.append(indexer.index(
                                    [role_j, head_token, other_coref] + ssf))
                        other_roles_stop = len(other_roles)
                        
                        # there might be more than one correct candidates (since they are coreferent)
                        for head in coreferring_heads:
                            candidates_start = len(candidates)
                            head_token = doc.deps()[f.sent][head]['token']
                            coref = non_pronoun_coref(doc, f.sent, head)
                            candidates.append(indexer.index([head_token, coref] +
                                                synsem_features_ontonotes(doc, f.sent, None, head, frame, fe.role)))       
                            for k, v in enumerate(doc.deps()[f.sent]):
                                if k not in fe.filler_heads:
                                    coref = non_pronoun_coref(doc, f.sent, k)
                                    candidates.append(indexer.index([v['token'], coref] +
                                                       synsem_features_ontonotes(doc, f.sent, None, k, frame, fe.role)))
                            candidates_stop = len(candidates)
                            examples.append([frame_and_role_index, 
                                             other_roles_start, other_roles_stop,
                                             candidates_start, candidates_stop, 
                                             0,])
                            
                        if np.random.rand() < 0.0001:
                            print('-------- example ---------')
                            print('Frame: %s, role: %s' %(f.predicate, fe.role))
                            print('Known explicit roles:')
                            for j, other_fe in enumerate(f.frame_elements):
                                if j != i and len(other_fe.filler_heads) >= 1:
                                    coreferent_heads = '='.join(doc.deps()[f.sent][h]['token'] 
                                                                for h in other_fe.filler_heads)
                                    print('\t%s\t%s' %(other_fe.role, coreferent_heads))
                            print('Candidates: ' + ' '.join(('*' if c in fe.filler_heads else '') + v['token'] 
                                                            for c, v in enumerate(doc.deps()[f.sent])))
#                     else:
#                         sys.stderr.write('No head was found for role %s, frame %s, sentence %d, file %s'
#                                          %(role, frame, f.sent, doc.base_path + ".*\n"))
                frame_count += 1
        doc_count += 1
#         if frame_count >= 100: break # for debugging
    indexer.seal(True)
    print("Read %d files" %doc_count)
    print("Found %d frames" %frame_count)
    print("Found %d roles" %len(examples))
    with open(paths.index_path, 'wb') as f: pickle.dump(indexer, f)
    np.savez(paths.examples_path, 
             frame_and_role=np.array(frame_and_role),
             other_roles=np.array(other_roles), 
             candidates=np.array(candidates), 
             examples=np.array(examples))
