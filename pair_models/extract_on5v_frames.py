import os
from ontonotes import OntonotesDocument, ontonotes_en
import features
import pickle
import conll
import numpy as np
import sys
from pair_models.extract_ontonotes_frames import non_pronoun_coref,\
    synsem_features_ontonotes, cols
from collections import OrderedDict
from conll import find_tokens
from on5v import read_on5v
from pair_models.model import ontonotes_paths
from isrl import version, silberer_frank_filter

on5v_path = 'data/conll_impl_roles_release/all.conll'

assert os.path.exists(ontonotes_paths.index_path), "Remember to run extract_ontonotes_frames.py first"
assert os.path.exists(on5v_path), "ON5V data file not found"

_extract_frames_ret = None

def extract_frames(on5v_examples_path):
    '''
    Extract frames from ON5V that have at least one DNI.
    The candidates are filtered according to Moor et al. (2013), footnote 12.
    
    Return a dictionary containing NumPy arrays encoded similar to what
    extract_ontonotes_frames returns.
    '''
    global _extract_frames_ret
    if _extract_frames_ret is not None:
        return _extract_frames_ret
    on5v = read_on5v()
    with open(ontonotes_paths.index_path, 'rb') as f: indexer = pickle.load(f)
    doc_paths = OrderedDict((f.doc, True) for f in on5v)
    pos2frame = dict(((f.doc, f.sent, f.offset), f) for f in on5v)

    frame_and_role = []
    other_roles = []
    candidates = []
    examples = []
    back_pointers = []

    num_correct_answers_not_among_candidates = 0    
    for doc_path in doc_paths:
        doc = OntonotesDocument(os.path.join(ontonotes_en, doc_path))
        found = 0
        for f in doc.iter_frames():
            f5v = pos2frame.get((doc_path, f.sent, f.offset))
            if f5v is not None:
                found += 1
                if not f.predicate.startswith(f5v.predicate):
                    sys.stderr.write('WARNING: Frame disagreement between OntoNotes and ON5V %s\n'
                                     %str((f.predicate, f5v.predicate, doc_path, f.sent, f.offset)))
                    continue
                for fe5v in f5v.frame_elements:
                    if fe5v.sent >= 0: # DNI is successfully resolved 
                        for s in range(max(f5v.sent-2, 0), f5v.sent+1):
                            print('s[%2d]: ' %(s-f5v.sent) +  
                                  ' '.join(d['token'] for d in doc.deps()[s]))
                        
                        frame_and_role_index = len(frame_and_role)
                        print('Document: %s' %doc_path)
                        print('Predicate position: (sent=%d, token=%d)' %(f.sent, f.offset))
                        print('Frame: %s, role: %s' %(f.predicate, fe5v.role))
                        frame_and_role.append(indexer.index([f.predicate, fe5v.role,
                                                             features.expected_roles(None, None, None, f.predicate, None),]))
                        head_token = doc.deps()[fe5v.sent][fe5v.filler_heads[0]]['token']
                        prefix = ' '.join(d['token'] for d in doc.deps()[fe5v.sent][max(fe5v.filler_heads[0]-5,0):fe5v.filler_heads[0]])
                        suffix = ' '.join(d['token'] for d in doc.deps()[fe5v.sent][fe5v.filler_heads[0]+1:fe5v.filler_heads[0]+6])
                        print('Correct filler: %s [%s] %s' %(prefix, head_token, suffix))
                        
                        other_roles_start = len(f.frame_elements)
                        print('other roles:')
                        for other_fe in f.frame_elements:
                            for other_head in other_fe.filler_heads:
                                head_token = doc.deps()[f.sent][other_head]['token']
                                other_coref = non_pronoun_coref(doc, f.sent, other_head)
                                print('\t%s: %s - %s' %(other_fe.role, head_token, other_coref))
                                ssf = synsem_features_ontonotes(doc, f.sent, None, other_head, f.predicate, other_fe.role)
                                other_roles.append(indexer.index([other_fe.role, head_token, other_coref] + ssf))
                        other_roles_stop = len(other_roles)
                        
                        print('candidate:')
                        candidates_start = len(candidates)
                        sent_start = max(f5v.sent-2, 0)
                        correct_answer_index = -1
                        for s in range(sent_start, f5v.sent+1):
                            heads = set()
                            for subtree in doc.trees()[s].subtrees():
                                if silberer_frank_filter(subtree):
                                    token_ids = set(t.token_id for t in subtree.terminals 
                                                    if t.token_id is not None)
                                    head = conll.find_head(token_ids, doc.deps()[s])
                                    if head is not None:
                                        heads.add(head)
                            for head in heads:
                                if (s, head) == (fe5v.sent, fe5v.filler_heads[0]):
                                    correct_answer_index = len(candidates) - candidates_start
                                head_token = doc.deps()[s][head]['token']
                                coref = non_pronoun_coref(doc, s, head)
                                candidates.append(indexer.index([head_token, coref] +
                                        synsem_features_ontonotes(doc, s, None, head, f5v.predicate, fe5v.role)))
                                print('\t%s - %s' %(head_token, coref))
                                back_pointers.append([doc, f.sent, f.offset, f5v.predicate, fe5v.role, s, head])
                        candidates_stop = len(candidates)
                        if correct_answer_index >= 0:
                            examples.append([frame_and_role_index, 
                                             other_roles_start, other_roles_stop,
                                             candidates_start, candidates_stop, 
                                             correct_answer_index])
                        else: # ignore the faulty example
                            sys.stderr.write('WARN: correct answer not found among candidates '
                                             '(doc: %s, sentence: %d, predicate: %s, role: %s).\n'
                                             %(doc_path, f5v.sent, f5v.predicate, fe5v.role))
                            num_correct_answers_not_among_candidates += 1
    print("Found %d examples" %len(examples))
    print("The correct answer is not among candidates in %d examples, maximum recall: %.2f%%"
          %(num_correct_answers_not_among_candidates, 100.0*num_correct_answers_not_among_candidates/len(examples)))
    _extract_frames_ret = (
            {'frame_and_role': np.array(frame_and_role),
            'other_roles': np.array(other_roles), 
            'candidates': np.array(candidates), 
            'examples': np.array(examples)
            }, back_pointers)
    return _extract_frames_ret
        

def print_decision(back_pointers, y, y_hat):        
    doc, pred_sent, pred_offset, pred, role, sent_hat, head_hat = back_pointers[y_hat]
    _, _, _, _, _, sent, head = back_pointers[y]
    print('\nDoc: ' + doc.base_path)
    print('Predicate position: (sent=%d, head=%d)' %(pred_sent, pred_offset))
    print('Predicate: %s, role: %s' %(pred, role))
    tokens_hat = find_tokens(head_hat, doc.deps()[sent_hat]) 
    str_hat = ' '.join(doc.deps()[sent_hat][i]['token'] for i in tokens_hat)
    print('Predicted: %s (sent=%d, head=%d)' 
          %(str_hat, sent_hat, head_hat))
    tokens = find_tokens(head, doc.deps()[sent]) 
    str_ = ' '.join(doc.deps()[sent][i]['token'] for i in tokens)
    print('Correct answer: %s (sent=%d, head=%d)' %(str_, sent, head))
        
def evaluate(model, example_func, n=1):
    sys.stdout.write('Reading data... ')
    data, back_pointers = extract_frames()
    frame_and_role = data['frame_and_role']
    other_roles = data['other_roles']
    candidates = data['candidates']
    examples = data['examples']
    sys.stdout.write('Done.\n')

    total_choices = 0
    eliminated_choices = 0
    acc = 0
    num_strict_correct = 0
    total = 0
    found_in_coref_chain = 0
    for i in range(examples.shape[0]):
        ex_tuple = example_func(examples[i], frame_and_role, other_roles, candidates, 1)
        y = examples[i, cols.EXAMPLE_GOLD_ANSWER]
        probs = model.predict_probs(*ex_tuple[:3])
        best_n = np.argsort(probs)[-n:]
        total_choices += len(probs)
        eliminated_choices += len(probs) - len(best_n)
        for y_hat in best_n:
            if y == y_hat:
                num_strict_correct += 1
                acc += 1
                break
            elif y >= 0:
                ex_start = examples[i, cols.EXAMPLE_CANDIDATE_START]
                doc, a, b, c, d, sent, head = back_pointers[ex_start+y]
                doc2, a2, b2, c2, d2, sent_hat, head_hat = back_pointers[ex_start+y_hat]
                assert (doc, a, b, c, d) == (doc2, a2, b2, c2, d2)
                tokens_hat = find_tokens(head_hat, doc.deps()[sent_hat])
                if sent == sent_hat and head in tokens_hat:
                    acc += 1
                    break
                else:
                    found = False
                    chain = doc.coref().get((sent, head)) or []
                    for (sent_coref, head_coref) in chain: 
                        if sent_coref == sent_hat and head_coref in tokens_hat: 
                            found = True
                            break
                    if found:
                        acc += 1
                        found_in_coref_chain += 1
                        break
                    else:
                        print_decision(back_pointers, ex_start+y, ex_start+y_hat)
        total += 1
    print('Accuracy: %.2f%% (%d / %d)' %(100.0*acc/total, acc, total))
    print('Eliminated %.2f%% choices (%d / %d)' %(100.0*eliminated_choices/total_choices, 
                                                  eliminated_choices, total_choices))
    print('Number found in coreference chain: %d' %found_in_coref_chain)
    print('Number of strict matches: %d' %num_strict_correct)
            
if __name__ == '__main__':
    on5v_examples_path = os.path.join('out', 'on5v-examples.%s.npz' %version)
    extract_frames(on5v_examples_path)
