"""Run experiments on coherence (MultiWay) + SynSem models 

Usage:
  exp_coherence_with_synsem_features.py <random_seed> [--model=<path>]
"""
from pair_models import model, BaseDNIModelWrapper
from pair_models.model import model_func
from features import synsem_features, expected_roles
from pair_models.extract_ontonotes_frames import cols
from docopt import docopt

def translate_example(e, frame_and_role, other_roles, candidates, batch_size):
    pr = frame_and_role[e[cols.EXAMPLE_FRAME_AND_ROLE]]
    rg = other_roles[e[cols.EXAMPLE_OTHER_ROLES_START]:e[cols.EXAMPLE_OTHER_ROLES_END], :cols.OTHER_ROLES_COREF+1]
    c = candidates[e[cols.EXAMPLE_CANDIDATE_START]:e[cols.EXAMPLE_CANDIDATE_END], :cols.CANDIDATE_FEATURES_END]
    y = e[cols.EXAMPLE_GOLD_ANSWER]
    return pr, rg, c, y, batch_size

class ModelWrapper(BaseDNIModelWrapper):
    
    def predict(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        frame = frame_elem.get('name')
        pr = [frame, role, expected_roles(doc, None, None, frame, None)]
        rg = [[r, doc.head_word(g), doc.nonpronoun_coref(g)] 
              for r, g in zip(overt_roles, overt_fillers)]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] + 
             synsem_features(doc, None, arg, frame, role) 
             for arg in candidates]
        return self.predict_func(self.indexer.index(pr),
                                 self.indexer.index(rg, ndmin=2), 
                                 self.indexer.index(c, ndmin=2))

model_name = 'coherence-with-synsem-features'

if __name__ == '__main__':
    arguments = docopt(__doc__)
    import isrl
    isrl.setup_experiment(arguments['<random_seed>'])
    model_path = arguments.get('--model')
    if not model_path:
        model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)
        model.learning_rate = 0.005
        model.train(model_func(), translate_example, 
                    model_path, len(synsem_features)+2, 2, 3)
    print('Model: ' + model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
    isrl.on5v_cv.resolve_dnis(m, model_name)