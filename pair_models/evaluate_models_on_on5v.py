from pair_models import extract_on5v_frames, exp_coherence_model, model
from glob import glob

if __name__ == '__main__':
    out_dir = 'out/2018-01-11-39bb50e/'
    model_paths = glob(out_dir + '*/coherence-model.pkl')
    for model_path in model_paths:
        m = model.load(model_path, exp_coherence_model.ModelWrapper)
        extract_on5v_frames.evaluate(m, exp_coherence_model.translate_example, n=1)

    