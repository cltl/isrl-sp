"""Run experiments on predicate-only (OneWay) models 

Usage:
  exp_predicate_only.py <random_seed> [--model=<path>]
"""
import numpy as np
from pair_models import model, BaseDNIModelWrapper
from pair_models.model import model_func
from pair_models.extract_ontonotes_frames import cols
from docopt import docopt

_empty_matrix = np.matrix([], dtype='int32')

def translate_example(e, frame_and_role, other_roles, candidates, batch_size):
    pr = frame_and_role[e[cols.EXAMPLE_FRAME_AND_ROLE], :cols.FR_ROLE+1]
    c = candidates[e[cols.EXAMPLE_CANDIDATE_START]:e[cols.EXAMPLE_CANDIDATE_END], 
                   :cols.CANDIDATE_COREF+1]
    y = e[cols.EXAMPLE_GOLD_ANSWER]
    return pr, _empty_matrix, c, y, batch_size

class ModelWrapper(BaseDNIModelWrapper):
    
    def predict(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        pr = [frame_elem.get('name'), role]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] for arg in candidates]
        return self.predict_func(self.indexer.index(pr), _empty_matrix, 
                                 self.indexer.index(c))
        
model_name = 'predicate-only'

if __name__ == '__main__':
    arguments = docopt(__doc__)
    import isrl
    isrl.setup_experiment(arguments['<random_seed>'])
    model_path = arguments.get('--model')
    if not model_path:
        model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)
        model.train(model_func(True), translate_example, model_path, 2, 0)
    print('Model: ' + model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
    isrl.on5v_cv.resolve_dnis(m, model_name)