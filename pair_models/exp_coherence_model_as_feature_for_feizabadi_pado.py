"""Run experiments on coherence (MultiWay) models 

Usage:
  exp_coherence_model.py <random_seed> --model=<path>
"""
import sys
import numpy as np
from pair_models.exp_coherence_model import model_name
import os
from pair_models import model
from isrl import pb_semeval, find_all_overt, Scorer
from data import Document, Pbparser, mark_role
from feizabadi_pado import feat_func
from feizabadi_pado import candidate_func, as_dict
import nltk
import pickle
import preprocess_on5v
import csv
from pair_models.model import ontonotes_paths
from docopt import docopt

def quantize(scores):
    return np.round(scores, decimals=1)

def gen_training_examples_dni_linking_augmented_features(doc, feature_func, m, indexer):
    examples = []
    for frame_elem in doc.frames:
        flags = list(frame_elem.findall('.//flag[@name="Definite_Interpretation"]'))
        if flags:
            overt_roles, overt_fillers = find_all_overt(frame_elem)
            frame = frame_elem.get('name')
            pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
            for flag in flags:
                fe_elem = flag.getparent()
                role = fe_elem.get('name')
                gold_filler = doc.id2elem[fe_elem.find('fenode').get('idref')]
                candidates = list(candidate_func(doc, frame_elem, overt_fillers))
                # calculate probabilities
                pr = [frame, mark_role(role)]
                rg = [[mark_role(r), doc.head_word(g), doc.nonpronoun_coref(g)]
                      for r, g in zip(overt_roles, overt_fillers)]
                c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] for arg in candidates]
                scores = m.predict_scores(indexer.index(pr),
                                          indexer.index(rg, ndmin=2), 
                                          indexer.index(c))
                scores = quantize(scores)
                # generate example
                for i, arg in enumerate(candidates):
                    feats = feature_func(doc, pred, arg, frame, role)
                    feats.append(scores[i])
                    gold_class = ('Yes' if arg == gold_filler else 'No')
                    examples.append((as_dict(feats), gold_class))
                    if gold_class == 'Yes': 
                        break
                else:
                    reason = ''
                    if doc.id2sent[pred.get('id')] - doc.id2sent[gold_filler.get('id')] not in [0,1,2]:
                        reason = 'because it is not in candidate window'
                    sys.stderr.write('WARN: found no correct filler among candidates %s\n' %reason)
    return examples

class AugmentedFeatureWrapper(object):
    
    def __init__(self, nb, ff, m, indexer):
        self.nb = nb
        self.ff = ff
        self.m = m
        self.indexer = indexer
    
    def __call__(self, doc, pred, pos, role, overt_roles, overt_fillers):
        candidates = list(candidate_func(doc, pred, overt_fillers))
        # calculate probabilities
        pr = [pred.get('name'), mark_role(role)]
        rg = [[mark_role(r), doc.head_word(g), doc.nonpronoun_coref(g)] 
              for r, g in zip(overt_roles, overt_fillers)]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] for arg in candidates]
        scores = self.m.predict_scores(self.indexer.index(pr),
                                       self.indexer.index(rg, ndmin=2), 
                                       self.indexer.index(c))
        scores = quantize(scores)
        # look at candidates
        for i, arg in enumerate(candidates):
            feats = self.ff(doc, pred, arg, pred.get('name'), role)
            feats.append(scores[i])
            predicted_y = self.nb.classify(as_dict(feats))
            if predicted_y == 'Yes':
                return candidates[i].get('id')
        return None

if __name__ == '__main__':
    arguments = docopt(__doc__)
    import isrl
    isrl.setup_experiment(arguments['<random_seed>'])
    model_path = arguments.get('--model')
    print('Model: ' + model_path)
    assert os.path.exists(model_path), 'Model not found, remember to run exp_coherence_model first'
    m = model.load_params(model_path)
    with open(ontonotes_paths.index_path, 'rb') as f: indexer = pickle.load(f)
    
    train_doc = Document(pb_semeval.train_path, Pbparser())
    examples = gen_training_examples_dni_linking_augmented_features(train_doc, feat_func, m, indexer)
    nb = nltk.NaiveBayesClassifier.train(examples)
    pb_semeval.resolve_dnis(AugmentedFeatureWrapper(nb, feat_func, m, indexer), 'feizabadi-pado')
    
    scorer = Scorer('semeval')
    for i, dir_cv in enumerate(preprocess_on5v.out_dir_cv):
        examples = []
        train_dir = os.path.join(dir_cv, 'train')
        test_dir = os.path.join(dir_cv, 'test')
        out_dir_cv = os.path.join(isrl.out_dir, 'coherence_model_as_feature_for_fp', '%02d' %i)
        for fname in os.listdir(train_dir):
            if fname.endswith('.xml'):
                path = os.path.join(train_dir, fname)
                train_doc = Document(path)
                examples.extend(gen_training_examples_dni_linking_augmented_features(train_doc, feat_func, m, indexer))
#         s = csv.writer(sys.stdout) # print out for debugging
#         for inp, out in examples:
#             s.writerow(tuple(inp.values()) + (out,))
        nb = nltk.NaiveBayesClassifier.train(examples)
        scorer.evaluate_dni_linking(test_dir, out_dir_cv, AugmentedFeatureWrapper(nb, feat_func, m, indexer))
    print(scorer.p_vals)
    print(scorer.r_vals)
    print(scorer.f1_vals)
