from data import read_embeddings
import pickle
import sys
import math
from collections import OrderedDict
import theano
from theano import function
from theano import tensor as T
import numpy as np
from theano.ifelse import ifelse
from theano.tensor.shared_randomstreams import RandomStreams
from pair_models import extract_ontonotes_frames
from time import time

def adagrad(grads, params, learning_rate=1.0, epsilon=1e-6):
    """Adagrad updates (borrow from Lasagne)
    Scale learning rates by dividing with the square root of accumulated
    squared gradients. See [1]_ for further description.
    Parameters
    ----------
    grads : list of gradient expressions
    params : list of shared variables
        The variables to generate update expressions for
    learning_rate : float or symbolic scalar
        The learning rate controlling the size of update steps
    epsilon : float or symbolic scalar
        Small value added for numerical stability
    Returns
    -------
    OrderedDict
        A dictionary mapping each parameter to its update expression
    Notes
    -----
    Using step size eta Adagrad calculates the learning rate for feature i at
    time step t as:
    .. math:: \\eta_{t,i} = \\frac{\\eta}
       {\\sqrt{\\sum^t_{t^\\prime} g^2_{t^\\prime,i}+\\epsilon}} g_{t,i}
    as such the learning rate is monotonically decreasing.
    Epsilon is not included in the typical formula, see [2]_.
    References
    ----------
    .. [1] Duchi, J., Hazan, E., & Singer, Y. (2011):
           Adaptive subgradient methods for online learning and stochastic
           optimization. JMLR, 12:2121-2159.
    .. [2] Chris Dyer:
           Notes on AdaGrad. http://www.ark.cs.cmu.edu/cdyer/adagrad.pdf
    """

    updates = OrderedDict()

    for param, grad in zip(params, grads):
        value = param.get_value(borrow=True)
        accu = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                             broadcastable=param.broadcastable)
        accu_new = accu + grad ** 2
        updates[accu] = accu_new
        updates[param] = param - (learning_rate * grad /
                                  T.sqrt(accu_new + epsilon))
    return updates

def override_embeddings(E, indexer, word2id, embeddings):
    count = 0
    for i, w in enumerate(indexer.tokens):
        i2 = word2id.get(w)
        if i2 is not None:
            E[i] = embeddings[i2]
            count += 1
    print('Override %d embeddings' %count)
    return E

class Model(object):

    def __init__(self, no_f, activation_func, aggregate_func, dropprob, gradient_clipping=5.0):
        '''
        gradient_clipping (float): protect against arithmetic overflow but 
                                   for some reason, slow down the training considerably
        '''
        self.no_f = no_f
        self.gradient_clipping = gradient_clipping
        srng = RandomStreams(seed=12345)
        
        pr = T.vector('pr', dtype='int64')
        rg = T.matrix('rg', dtype='int64')
        c = T.matrix('c', dtype='int64')
        y = T.scalar('y', dtype='int64')
        batch_size = T.scalar('batch_size')
        Wf = theano.shared(np.empty((0, 0), dtype='float32'), name='Wf')
        Wg = theano.shared(np.empty((0, 0), dtype='float32'), name='Wg')
        b = theano.shared(np.empty((0,), dtype='float32'), name='b')
        E_in = theano.shared(np.empty((0, 0), dtype='float32'), name='E_in')
        E_out = theano.shared(np.empty((0, 0), dtype='float32'), name='E_out')
        if no_f:
            self.params = [Wg, b, E_in, E_out]
        else:
            self.params = [Wf, Wg, b, E_in, E_out]
        
        def network(training):
            hidden_g = T.dot(E_in[pr].reshape((-1,)), Wg)
            hidden_g.name = 'hidden_g'
            if no_f:
                hidden = activation_func(hidden_g + b)
            else: 
                hidden_f = T.dot(E_in[rg].reshape((-1, Wf.shape[0])), Wf)
                hidden_f.name = 'hidden_f'
                hidden_f = ifelse(T.eq(rg.size, 0), T.zeros((2,Wf.shape[1])), hidden_f)
                hidden = activation_func(hidden_f + hidden_g + b) 
                hidden = aggregate_func(hidden, axis=0)
            hidden.name = 'hidden'
            if training and dropprob > 0:
                hidden1 = T.switch(srng.binomial(size=hidden.shape,p=1-dropprob), hidden, 0)
            else:
                if dropprob > 0:
                    hidden1 = (1-dropprob) * hidden
                else:
                    hidden1 = hidden
            hidden1.name = 'hidden1'
            c_emb = E_out[c].reshape((c.shape[0], -1))
            c_emb.name = 'candidate_embeddings'
            score = T.dot(hidden1, c_emb.T)
            score.name = 'score'
            probs = T.nnet.softmax(score).flatten()
            probs.name = 'probs'
            return score, probs
        
        training_score, o_dropout = network(True)
        training_nll = -T.log(o_dropout[y]) / batch_size
        training_y_hat = T.argmax(training_score)
        training_acc = T.eq(training_y_hat,y) / batch_size
        one_grads = T.grad(training_nll, self.params) 

        grad_updates = OrderedDict()
        self._grads = [theano.shared(np.empty_like(p.get_value()), name=p.name+'_grad') 
                      for p in self.params]
        for g, og in zip(self._grads, one_grads):
            grad_updates[g] = g+og
        if gradient_clipping == 0:
            self.grads = self._grads
        else:
            self.grads = [T.clip(g, -gradient_clipping, gradient_clipping) 
                          for g in self._grads] 

        self.accu_grads = function([pr, rg, c, y, batch_size], 
                                   [training_nll, training_acc, o_dropout], 
                                   updates=grad_updates, on_unused_input='ignore')
        self.regularize = function([], updates=OrderedDict((g, g+p*1e-4) 
                                                           for p, g in zip(self.params, self._grads)))
        self.clear_grads = function([], updates=OrderedDict((g, g*0) for g in self._grads))
        
        test_scores, test_probs = network(False)
        test_nll = -T.log(test_probs[y]) / batch_size
        test_y_hat = T.argmax(test_scores)
        test_y_hat.name = 'y_hat'
        test_acc = T.eq(test_y_hat,y) / batch_size
        
        self.predict = function([pr, rg, c], test_y_hat, on_unused_input='ignore')
        self.predict_probs = function([pr, rg, c], test_probs, on_unused_input='ignore')
        self.predict_scores = function([pr, rg, c], test_scores, on_unused_input='ignore')
        self.measure_test_perf = function([pr, rg, c, y, batch_size], 
                                          [test_nll, test_acc], on_unused_input='ignore')

    def init_params(self, indexer, word2id, embeddings, num_filler_features=1, 
                    num_role_features=1, num_pr_features=2):
        vocab_size = len(indexer.vocab)
        embedding_dim = embeddings.shape[1]
        if self.no_f:
            Wg, b, E_in, E_out = self.params
        else:
            Wf, Wg, b, E_in, E_out = self.params
            Wf.set_value(np.random.rand(embedding_dim*(num_role_features+1), 
                                           embedding_dim*num_filler_features)
                         .astype('float32')*0.02-0.01)
        Wg.set_value(np.random.rand(embedding_dim*num_pr_features, 
                                    embedding_dim*num_filler_features)
                     .astype('float32')*0.02-0.01)
        b.set_value(np.zeros((embedding_dim*num_filler_features,), dtype='float32'))
        E_in.set_value(override_embeddings(
            np.random.rand(vocab_size, embedding_dim).astype('float32')*0.02-0.01,
            indexer, word2id, embeddings))
        E_out.set_value(override_embeddings(
            np.random.rand(vocab_size, embedding_dim).astype('float32')*0.02-0.01,
            indexer, word2id, embeddings))
        for p, g in zip(self.params, self._grads):
            g.set_value(np.empty_like(p.get_value()))

cube_func = lambda x: T.power(x, 3)
model_func = lambda no_f=False: Model(no_f, cube_func, T.max, 0)
max_epochs = 1000
# max_epochs = 10 # for debugging
batch_size = 10000
learning_rate = 0.05
early_stopping = 100

ontonotes_paths = extract_ontonotes_frames.Paths('out/ontonotes-frames.2018-01-04-38f3f9d')

def train(model, example_func, model_path, num_filler_features, num_role_features, num_pr_features=2):
    '''
    num_filler_features: number of features used to represent a filler
                         (each feature is turned into a vector, all vectors
                         are concatenated to make a single vector representing
                         the filler)
    '''
    sys.stdout.write('Reading data... ')
    with open(ontonotes_paths.index_path, 'rb') as f:
        indexer = pickle.load(f)
    data = np.load(ontonotes_paths.examples_path)
    frame_and_role = data['frame_and_role']
    other_roles = data['other_roles']
    candidates = data['candidates']
    examples = data['examples']
    sys.stdout.write('Done.\n')

    # don't shuffle the examples here because you might break a contract with
    # exp_hyperparameters. They need to compare the performance against the same dataset.
    n = int(examples.shape[0]*0.9)
    train_examples = examples[:n]
    valid_examples = examples[n:]
    print('Train: %d, valid: %d' %(train_examples.shape[0], valid_examples.shape[0]))
    print('Building model... ')
    word2id, embeddings = read_embeddings()
    model.init_params(indexer, word2id, embeddings, num_filler_features, num_role_features, num_pr_features)
    step = function([], updates=adagrad(model.grads, model.params, learning_rate=learning_rate))
    print('Building model... Done.\n')
    
    last_epoch_without_improvement = -1
    best_acc = 0
    start_sec = time()
    for epoch in range(max_epochs):
        rows = np.random.randint(n, size=(batch_size,))
        model.clear_grads()
        nll, acc = 0.0, 0.0
        for i in range(batch_size):
            x = example_func(train_examples[rows[i]], frame_and_role, other_roles, 
                             candidates, batch_size)
            one_nll, one_acc, _ = model.accu_grads(*x)
            nll += one_nll
            acc += one_acc
        model.regularize()
        step()
           
        if epoch % 10 == 0 or epoch == max_epochs-1:
            # monitor
            print('Epoch %d:' %epoch)
            print('\tTrain NLL: %f' %nll)
            print('\tTrain accuracy: %f' %acc)
            nll, acc = 0.0, 0.0
            for i in range(valid_examples.shape[0]):
                one_nll, one_acc = model.measure_test_perf(*example_func(
                        valid_examples[i], frame_and_role, other_roles, 
                        candidates, valid_examples.shape[0]))
                nll += one_nll
                acc += one_acc
            print('\tValid NLL: %f' %nll)
            print('\tValid accuracy: %f' %acc)
            i = np.random.randint(valid_examples.shape[0])
            pr, rg, c, y, _ = example_func(valid_examples[i], frame_and_role, 
                                           other_roles, candidates, 1)
            predicted_y = model.predict(pr, rg, c)
            print('\tExample:')
            if len(pr) >= 2:
                print('\t\tPredicate: %s' %indexer.tokens[pr[0]])
                print('\t\tRole: %s' %indexer.tokens[pr[1]])
            if len(rg) > 0 and rg.shape[1] > 0:
                print('\t\tExplicit arguments:')
                for row in rg:
                    print('\t\t\t%s: %s' %(indexer.tokens[row[0]],
                                           ' - '.join(indexer.tokens[i] for i in row[1:])))
            print('\t\tPredicted filler: %s (%s)' %(' - '.join(str(indexer.tokens[i]) 
                                                               for i in c[predicted_y]),
                                             'correct' if predicted_y == y else 'wrong'))
            print('\tElapsed time: %.1f minutes' %((time()-start_sec)/60))
            if acc > best_acc:
                best_acc = acc
                with open(model_path, 'wb') as f:
                    pickle.dump(model, f)
                print('Best model saved into %s' %model_path)
                last_epoch_without_improvement = epoch
            else:
                if (epoch-last_epoch_without_improvement) >= early_stopping > 0: 
                    print("Stopped early because no improvement was observed "
                          "after %d iterations." %early_stopping)
                    break
    return best_acc

def load_params(model_path):
    with open(model_path, 'rb') as f:
        return pickle.load(f)
            
def load(params_path, wrapper):
    m = load_params(params_path)
    with open(ontonotes_paths.index_path, 'rb') as f:
        indexer = pickle.load(f)
    return wrapper(m.predict, indexer)

