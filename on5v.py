import os
import re
from collections import defaultdict
import sys
import ptb
import conll
from nltk.corpus.reader.wordnet import WordNetError
from nltk.corpus import wordnet as wn
import codecs
from data import Frame, FrameElement, Document, is_dni
from ontonotes import OntonotesDocument, ontonotes_en, ontonotes2tiger
from collections import OrderedDict
from lxml import etree
import isrl
import csv
import itertools

on5v_path = 'data/conll_impl_roles_release/all.conll'

def read_on5v(path=on5v_path):
    '''
    Returns a list of frames converted from ON5V format.
    '''
    frames = []
    num_empty_frames = 0
    num_none_args = 0
    num_explicit_roles = 0
    num_dnis = 0
    deps = {}
    with codecs.open(path, 'r', 'utf-8') as f:
        for line in f:
            line = line.strip()
            if line:
                doc, loc, predicate, args_str = line.split('\t')
                pred_sent, pred_tok = loc[1:].split('_')
                f = Frame(int(pred_sent), int(pred_tok), predicate, doc)
                
                args = []                
                if args_str != '-':
                    args_str = re.sub(r'\}$', '', re.sub(r'^\{', '', args_str))
                    for arg in args_str.split('),'):
                        role, toks = arg.split('=')
                        if re.match('None', role):
                            num_none_args += 1
                        else:
                            args.append(arg)
                if args:
                    if doc not in deps:
                        dep_path = os.path.join(ontonotes_en, doc + '.dep')
                        deps[doc] = conll.load_trees(dep_path)
                    for arg in args:
                        arg = re.sub(r'^Arg', 'A', arg).split('=')
                        role, toks = arg
                        res = re.findall('[+-]res$', role)
                        res = '' if len(res) == 0 else res[0]
                        role = re.sub('[+-]res$', '', role)
                        if res == '+res':
                            dni_sents = set(re.findall(r's(\d+)_', toks))
                            if len(dni_sents) != 1:
                                sys.stderr.write('WARNING: DNI is not in one sentence (line: %s)\n' %line)
                                continue
#                             assert len(dni_sents) == 1, line
                            dni_sent = int(list(dni_sents)[0])
                            toks = re.sub(r's\d+_', '', 
                                          re.sub(r'^\(', '',
                                          re.sub(r'\)$', '', toks)))
                            toks = toks.split(',')
                            toks = [int(val) for val in toks]
                            dni_head = conll.find_head(toks, deps[doc][dni_sent])
                            dni_str = [deps[doc][dni_sent][i]['token'] for i in toks]
                            f.frame_elements.append(FrameElement(role, [toks], [dni_head], [dni_str], [], [], dni_sent))
                            num_dnis += 1
                        elif res == '-res':
                            f.frame_elements.append(FrameElement(role, [], [], [], [], [], -1))
                        else:
                            num_explicit_roles += 1
                    frames.append(f)
                else:
                    num_empty_frames += 1
    print("Read %d frames from %s" %(len(frames), path))
    print("Found %d DNI" %num_dnis)
    print("Ignored %d arguments because the role is 'None'" %num_none_args)
    print("Ignored %d explicit roles" %num_explicit_roles)
    print("Ignored %d frames because they contain no frame elements" %num_empty_frames)
    return frames

def on5v2tiger(inp_path, out_dir):
    '''
    Extract frames from ON5V that have at least one DNI.
    The candidates are filtered according to Moor et al. (2013), footnote 12.
    '''
    on5v = read_on5v(inp_path)
    doc_paths = OrderedDict((f.doc, True) for f in on5v)
    pos2frame = dict(((f.doc, f.sent, f.offset), f) for f in on5v)

    written_paths = set()
    tiger_docs = []
    num_dnis = 0
    for doc_path in doc_paths:
        doc = OntonotesDocument(os.path.join(ontonotes_en, doc_path))
        tiger, frame_elems = ontonotes2tiger(doc)
        tiger_docs.append(tiger)
        
        found_dni = False
        for f in doc.iter_frames():
            f5v = pos2frame.get((doc_path, f.sent, f.offset))
            if f5v is not None:
                if not f.predicate.startswith(f5v.predicate):
                    sys.stderr.write('WARNING: Frame disagreement between OntoNotes and ON5V %s\n'
                                     %str((f.predicate, f5v.predicate, doc_path, f.sent, f.offset)))
                    continue
                for fe5v in f5v.frame_elements:
                    if fe5v.sent >= 0: # DNI is successfully resolved 
                        frame = frame_elems[(f.sent, f.offset)]
                        fe = etree.SubElement(frame, 'fe', id='s%d_dni%d' %(f.sent, num_dnis), name=fe5v.role)
                        fe_nt = ptb.find_constituent(fe5v.filler_tokens[0], doc.trees()[fe5v.sent])
                        etree.SubElement(fe, 'fenode', idref=fe_nt.nt_id)
                        etree.SubElement(fe, 'flag', name='Definite_Interpretation')
                        num_dnis += 1
                        found_dni = True
#                         if num_dnis > 10: return ## for debugging
        
        if found_dni:  
            out_path = os.path.join(out_dir, os.path.basename(doc_path) + '.xml')
            assert out_path not in written_paths
            with codecs.open(out_path, 'w', 'utf-8') as f:
                f.write(etree.tostring(tiger, pretty_print=True).decode('utf-8'))
            written_paths.add(out_path)
            
    print("Found %d DNIs" %num_dnis)
    print("Written to %d files" %len(written_paths))
    return tiger_docs
    
if __name__ == '__main__':
    isrl.setup_experiment(None)
    inp_dir = 'data/conll_impl_roles_release/bring/propbank'
    out_dir = os.path.join(isrl.out_dir, 'on5v-bring')
    os.makedirs(out_dir, exist_ok=True)
    table = [['Predicate', 'Frame', 'Role', 'Filler', 'Filler_corefs', 'Sentences']]
    for fname in os.listdir(inp_dir):
        inp_path = os.path.join(inp_dir, fname)
        xml_docs = on5v2tiger(inp_path, out_dir)
        for xml_doc in xml_docs:
            doc = Document(xml_doc)
            for frame_elem in doc.frames:
                for fe_elem in frame_elem.iterfind('fe'):
                    if is_dni(fe_elem):
                        pred_id = frame_elem.find('target/fenode').get('idref')
                        filler_id = fe_elem.find('fenode').get('idref')
                        sent_begin, sent_end = sorted([doc.id2sent[pred_id], doc.id2sent[filler_id]])
                        sent_words = ' '.join(itertools.chain(
                                *[doc.id2words[doc.sentences[i].get('id')] 
                                  for i in range(sent_begin, sent_end+1)]))
                        filler_corefs = doc.coref.get(filler_id, [])
                        table.append([' '.join(doc.id2words[pred_id]),
                                      frame_elem.get('name'),
                                      fe_elem.get('name'),
                                      ' '.join(doc.id2words[filler_id]),
                                      '='.join(' '.join(doc.id2words[fcoref]) 
                                                        for fcoref in filler_corefs),
                                      sent_words])
#         if len(table) > 5: break # for debugging
    w = csv.writer(sys.stdout)
    w.writerows(table)