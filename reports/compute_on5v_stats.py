from data import Document
import ontonotes
from ontonotes import ontonotes_en, OntonotesDocument
import os
from isrl import silberer_frank_filter_elem, remove_relation_tag
from collections import Counter

def compute_ontonotes_coverage():
    ''' This check the goodness of using OntoNotes as a training set for ON5V.
    It measures the portion of test instances (i.e. pairs of <predicate, role>)
    that is covered by OntoNotes.
    '''
    ontonotes_roles = set()
    for base_path in ontonotes.list_docs(ontonotes_en):
        doc = OntonotesDocument(base_path)
        for f in doc.iter_frames():
            for fe in f.frame_elements:
                ontonotes_roles.add((f.predicate, fe.role))
                
    on5v_roles = []
    on5v_dir = os.path.join('preprocessed_data', 'on5v', 'v1.0', 'all')
    for fname in os.listdir(on5v_dir):
        if fname.endswith('.xml'):
            path = os.path.join(on5v_dir, fname)
            doc = Document(path)
            for frame_elem in doc.frames:
                pred = frame_elem.get('name')
                for flag_elem in frame_elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
                    fe_elem = flag_elem.getparent()
                    role = fe_elem.get('name')
                    on5v_roles.append((pred, role))
            
    found = sum(1 for x in on5v_roles if x in ontonotes_roles)
    print('Coverage of OntoNotes over ON5V DNIs: %d/%d = %.2f%%' 
          %(found, len(on5v_roles), 100*found/len(on5v_roles)))
    uniq_on5v_roles = set(on5v_roles)
    found_uniq = len(uniq_on5v_roles.intersection(ontonotes_roles))
    print('Coverage of OntoNotes over ON5V unique pairs of (pred, role): %d/%d = %.2f%%'
          %(found_uniq, len(uniq_on5v_roles), 100*found_uniq/len(uniq_on5v_roles)))

def compute_silberer_frank_filter_coverage():
    num_filler = 0
    num_accepted = 0
    types = Counter()
    on5v_dir = os.path.join('preprocessed_data', 'on5v', 'v1.0', 'all')
    for fname in os.listdir(on5v_dir):
        if fname.endswith('.xml'):
            path = os.path.join(on5v_dir, fname)
            doc = Document(path)
            for frame_elem in doc.frames:
                for flag_elem in frame_elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
                    fe_elem = flag_elem.getparent()
                    gold_filler = doc.id2elem[fe_elem.find('fenode').get('idref')]
                    num_filler += 1
                    if silberer_frank_filter_elem(doc, gold_filler):
                        num_accepted += 1
                    else:
                        type_ = remove_relation_tag(gold_filler.get('cat') or gold_filler.get('pos'))
                        types[type_] += 1
    print('Number of gold filler: %d' %num_filler)
    print("... among which accepted by Silberer and Frank's filler: %d" %num_accepted)
    print("Filtered fillers have syntactic types: %s" %types)

if __name__ == '__main__':
    compute_ontonotes_coverage()
    compute_silberer_frank_filter_coverage()