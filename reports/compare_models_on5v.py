from collections import namedtuple, OrderedDict
from glob import glob
import os
import re
import pandas as pd
import numpy as np
from scipy.stats.stats import ttest_1samp

def parse_float_list(s):
    return [float(subs) for subs in re.findall('0(?:\.\d+)?', s)]

def extract_on5v_performance(path):
    with open(path) as f:
        s = f.read()
    return [parse_float_list(line) for line in 
            re.findall(r'\[0(?:\.\d+)?(?:, 0(?:\.\d+)?){9}\]', s)]

if __name__ == '__main__':
    data = []
    data2 = []
    
    baseline_path = 'out/2018-02-21-ba5a9ce/exp-baseline.py.out'
    p, r, f1 = extract_on5v_performance(baseline_path)
    data.append(('Baseline', np.mean(p), np.mean(r), np.mean(f1), '-', 1))
    data2.append(f1)
    baseline_f1 = np.mean(f1)
    
    feizabadi_pado_out_path = 'out/2018-02-21-ba5a9ce/exp_dni_on5v.py.out'
    p, r, f1, p_sf, r_sf, f1_sf = extract_on5v_performance(feizabadi_pado_out_path)
    data.append(('F&P', np.mean(p), np.mean(r), np.mean(f1), '-', 1))
    data.append(('F&P + S&F filter', np.mean(p_sf), np.mean(r_sf), np.mean(f1_sf), '-', 1))
    data2.append(f1)
    data2.append(f1_sf)

    neural_model_dir = 'out/2018-02-21-ba5a9ce/'
    ModelInfo = namedtuple('ModelInfo', ['name', 'fname'])
    neurl_models = [ModelInfo('OneWay', 'exp_predicate_only.py.out'), 
                    ModelInfo('MultiWay', 'exp_coherence_model.py.out'), 
                    ModelInfo('SynSem', 'exp_synsem.py.out'),
                    ModelInfo('OneWay+SynSem', 'exp_predicate_with_synsem_features.py.out'), 
                    ModelInfo('MultiWay+SynSem', 'exp_coherence_with_synsem_features.py.out')]
    for name, fname in neurl_models:
        paths = glob(os.path.join(neural_model_dir, '*', fname))
        results = []
        for path in paths:
            p, r, f1 = extract_on5v_performance(path)
            results.append((np.mean(p), np.mean(r), np.mean(f1)))
            data2.append(list(f1))
        p, r, f1 = zip(*results)
        _, p_val1 = ttest_1samp(f1, baseline_f1)
        data.append((name, np.mean(p), np.mean(r), np.mean(f1), np.std(f1), len(paths), p_val1))
        
    df = pd.DataFrame(data, columns = ['Model', 'P', 'R', 'F1', 'std(F1)', 
                                       'Num. tries', 'p_val(baseline)'])
    print(df)
            
    identity = lambda x: x
    to_pct = lambda x: '%.2f' %(x*100) if isinstance(x, float) else x
    df2 = df.transform(OrderedDict([('Model', identity), ('P', to_pct), ('R', to_pct),
                                    ('F1', to_pct), ('std(F1)', to_pct)]))
    print(df2.to_latex(index=False))

#     df3 = pd.DataFrame(data2, columns = ['CV%02d'%i for i in range(10)])
#     print(df3.mean(axis=0))
#     print(df3.std(axis=0))