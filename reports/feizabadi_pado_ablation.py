'''
Created on 22 Sep 2017

@author: cumeo
'''
import re
from tabulate import tabulate
import sys
import csv

def sum_two_numbers(strs):
    assert len(strs) == 2
    return int(strs[0]) + int(strs[1])

if __name__ == '__main__':
    table = [['Feature', 'TP', 'FP', 'FN', 'P', 'R', 'F1', 'delta']]
    log_path = input('Enter the path to log file: ')
    with open(log_path) as f:
        s = f.read()
    chunks = re.split('={10,}', s)
    it = iter(chunks)
    last_features = ''
    last_f1 = 0
    try:
        while True:
            chunk = next(it).strip()
            if chunk.startswith('Features'):
                feat = chunk[9:].strip()
                assert feat.startswith(last_features)
                new_feat = feat[len(last_features):].replace(',', '+')
                
                chunk = next(it)
                tp = sum_two_numbers(re.findall(r'True Positives:\s*(\d+)', chunk))
                fp = sum_two_numbers(re.findall(r'False Positives:\s*(\d+)', chunk))
                fn = sum_two_numbers(re.findall(r'False Negatives:\s*(\d+)', chunk))
                p = tp / (tp+fp) if tp > 0 else 0.0
                r = tp / (tp+fn) if tp > 0 else 0.0
                f1 = 2/(1/p + 1/r) if tp > 0 else 0.0
                
                row = [new_feat, str(tp), str(fp), str(fn), 
                       '%.2f' %(100*p), '%.2f' %(100*r), '%.2f' %(100*f1),
                       '%.2f' %(100*(f1-last_f1))]
                table.append(row)
                
                last_features = feat 
                last_f1 = f1
    except StopIteration:
        pass
    
    writer = csv.writer(sys.stdout)
    writer.writerows(table)
    print(tabulate(table, tablefmt="latex"))