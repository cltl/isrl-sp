'''
Created on 11 Mar 2017

@author: cumeo
'''
import sys
import os
import re
import numpy as np
import codecs
import traceback
import pandas as pd
from collections import OrderedDict, namedtuple
from glob import glob
from scipy.stats.stats import ttest_1samp

def compute_prf1(path):
    with codecs.open(path, 'r', 'utf-8') as f:
        content = f.read()
    tps = re.findall(r'True Positives: (\d+)', content)
    assert len(tps) == 2
    tp = int(tps[0]) + int(tps[1])
    fps = re.findall(r'False Positives: (\d+)', content)
    assert len(fps) == 2
    fp = int(fps[0]) + int(fps[1])
    fns = re.findall(r'False Negatives: (\d+)', content)
    assert len(fns) == 2
    fn = int(fns[0]) + int(fns[1])
#                         print(tps, fps, fns)
    p = tp / float(tp+fp)
    r = tp / float(tp+fn)
    f1 = 2 / (1/p + 1/r)
    return p, r, f1

if __name__ == '__main__':
    rows = []
    baseline_path = 'out/2018-02-21-ba5a9ce/exp-baseline.py.out'
    p, r, baseline_f1 = compute_prf1(baseline_path)
    rows.append(('Baseline', p, r, baseline_f1, '-', '-', 1))
    
    feizabadi_pado_out_path = 'out/2018-02-21-ba5a9ce/exp_dni_propbank.py.out'
    p, r, fp_f1 = compute_prf1(feizabadi_pado_out_path)
    rows.append(('F&P', p, r, fp_f1, '-', '-', 1))
    
    neural_root_dir = 'out/2018-02-21-ba5a9ce/'
    ModelInfo = namedtuple('ModelInfo', ['name', 'fname'])
    neurl_models = [ModelInfo('OneWay', 'exp_predicate_only.py.out'), 
                    ModelInfo('MultiWay', 'exp_coherence_model.py.out'), 
                    ModelInfo('SynSem', 'exp_synsem.py.out'),
                    ModelInfo('OneWay+SynSem', 'exp_predicate_with_synsem_features.py.out'), 
                    ModelInfo('MultiWay+SynSem', 'exp_coherence_with_synsem_features.py.out')]
    for name, fname in neurl_models:
        paths = glob(os.path.join(neural_root_dir, '*', fname))
        ps, rs, f1s = [], [], []
        for path in paths:
            p, r, f1 = compute_prf1(path)
            ps.append(p)
            rs.append(r)
            f1s.append(f1)
        _, p_val1 = ttest_1samp(f1s, baseline_f1)
        _, p_val2 = ttest_1samp(f1s, baseline_f1)
        if len(ps) > 0:
            rows.append((name, np.mean(ps), np.mean(rs), np.mean(f1s), np.std(f1s), 
                         np.min(f1s), np.max(f1s), len(f1s), 
                         p_val1, p_val2))
    
    df = pd.DataFrame(rows, columns=('Model', 'P', 'R', 'F1', 'std(F1)', 
                                     'min(F1)', 'max(F1)', 'num_attempts', 
                                     'p_val(baseline)', 'p_val(F&P)'))
    print(df)
    
    identity = lambda x: x
    to_pct = lambda x: '%.2f' %(x*100) if isinstance(x, float) else x
    df2 = df.transform(OrderedDict([('Model', identity), ('P', to_pct), ('R', to_pct),
                                    ('F1', to_pct), ('std(F1)', to_pct)]))
    print(df2.to_latex(index=False))
