README

The ON5V (OntoNotes 5 Verb Predicates) Non-local Role Linking Data Set provides annotations for non-local roles in the CONLL 2012 format.

Annotations are given in the FrameNet, PropBank, and VerbNet schemas.

Column 1: PATH
Path to Ontonotes Data, with sentence index

Column 2: ID
Sentence counter, starting at 0 for each sentence and the character offset, 
starting at 0 for each index (ex: s30_0 is the first token for sentence 29)

(Minh, Fri 28 Apr 2017) First they say "character offset" and then "token". After
looking at the file "a2e_0004.v2_gold_conll.bring", I think the correct 
interpretation is token. Empty terminals (e.g. *ICH*-1 and *PRO*) doesn't
count.

Column 3: Predicate
Predicate is either a Frame, PB predicate, or VerbNet class

Column 4: Roles
Give the role names, role IDs for each token in the role span. 
[+res] denotes roles that are resolvable,  [-res] for roles that are non-resolvable


(Minh, Tue 19 Sep 2017) roles that are not annotated with +/-res seem to be
reproduciton of explicit roles in OntoNotes. However, they ignored alls non-core
roles.