import os
import re
dir = "corpusXML"
for fname in os.listdir(dir):
  if re.search(r'\.xml$', fname):
    fname_out = fname[:-4] + '.txt'
    with open(dir + "/" + fname) as fin:
      with open(dir + "/" + fname_out, 'wt') as fout:
        for line in fin:
          m = re.search('<text>(.+)</text>', line)
          if m:
            raw_text = m.group(1)
            fout.write(raw_text)
            fout.write('\n')
        
