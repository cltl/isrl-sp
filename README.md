## Software requirements

The code is dependent on various popular software:

- Python 3 (doesn't work on Python 2.7)
- Java (tested on Java 7)
- Theano 1.0.1 (might not work on newer or older versions)
- SciPy/NumPy
- NLTK with WordNet data

For a complete list, please refer to [setup-vm.txt](setup-vm.txt). The file
details the steps I took to install an empty Ubuntu virtual machine 
(*read it, don't run it!*)

## Downloading data

Some corpora need to be downloaded, extracted and put under `data`:

- nombank.1.0
- ontonotes-release-5.0

Your `data` directory should look like this:

    data
    +-- nombank.1.0
    |   +-- Addresses-of-Multiple-Propositions
    |   +-- ADJADV.1.0
    |   +-- ...
    +-- ontonotes-release-5.0
    |   +-- data
    |   +-- docs
    |   +-- index.html
    |   +-- tools
    + ...

**IMPORTANT**: You need to patch OntoNotes using this command: 
`patch -p4 < ontonotes.patch` (executed from `data` directory) and remove the 
file `ontonotes-release-5.0/data/files/data/english/annotations/nw/p2.5_c2e/00/p2.5_c2e_0034.parse`, otherwises
a later preparation script will fail. The patch corrects some minor formatting
error in OntoNotes and the removed file contained an empty sentence which 
my script doesn't know how to handle.

## Running experiments

- Run all (including preparation scripts): `./run-all.sh`
- Reimplementation of Feizabadi and Pado (2015): run `python exp-feizabadi-pado.py`

## Preparation scripts

You don't need to run these scripts because their output is already placed into
`data` directory but if you want to study the code very carefully or reproduce
everything from scratch, you can run them one by one:

- `compute-role-percentage.py`: prepare data for the feature "Role Percentage" (see Table 2)
- `count-lemma-frequency-semeval.py`: prepare data for the feature "Word Frequency" (see Table 2)
- `extract-expected-roles.py`: prepare data for the feature "Expected roles" (see Table 2)
- `predominant-role-set.py`: prepare data for the first step in Section 3.2
- Obtaining supersenses:
    + Install [UKB](http://ixa2.si.ehu.es/ukb/) and prepare necessary databases
    + Run `extract-ukb-context.py < INPUT > OUTPUT` on datasets (PropBank format)
    + Edit `call_wsd.sh` to match your file names and run it
    + Run `convert-ukb-to-supersense.py INPUT > OUTPUT` to convert WordNet offsets into supersenses 
    + Put supersense files next to CoNLL files, the name should be exactly the same, except `.txt` is replaced by `.supersense`

## Reproduction

Make sure you have a clean version of the code (i.e. no unstaged modification/new file).

**Notice**: Because I didn't seed the Theano random generator, the numbers won't
be exactly the same. The mean F1 can be lower or higher but when taking into account
the standard deviation, the conclusions of the paper should hold.

### Preprocessing

1. `git checkout d8c47e8 && python3 count_lemma_frequency_ontonotes.py`
1. `git checkout cfbc1ca && python3 compute_role_percentage_ontonotes.py`
2. `git checkout 38f3f9d && python3 -m pair_models.extract_ontonotes_frames`

### Train and evaluate models 

1. Train models (ignore results): `git checkout 8b51285`
    1. Neural models: `scripts/run-before-lrec.job`
    2. Feizabadi & Pado on SemEval: `python3 -m feizabadi_pado.exp_dni_propbank`
    2. Feizabadi & Pado on ON5V: `python3 -m feizabadi_pado.exp_dni_on5v`
    3. Baseline: `python3 exp-baseline.py`
2. Evaluate: `git checkout ba5a9ce && scripts/rerun-before-lrec-final.job`
3. Report: `git checkout ee9ddcd`
    1. SemEval-2010 table: `python3 -m reports.compare_models`
    2. ON5V table: `python3 -m reports.compare_models_on5v`

## References

- Feizabadi, P. S., & Pado, S. (2015). Combining Seemingly Incompatible Corpora for Implicit Semantic Role Labeling. In Proceedings of the Fourth Joint Conference on Lexical and Computational Semantics (*SEM 2015 ) (pp. 40–50).
