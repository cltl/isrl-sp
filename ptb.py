from nltk.tree import ParentedTree, Tree
import codecs
import sys

root_dir = 'data/ptb'
# assert os.path.exists(root_dir), 'please link/put PENN TreeBank into data directory'

def _assign_token_ids(root, tok_id):
    assert not isinstance(root, str)
    root.token_id = None
    if len(root) == 1 and isinstance(root[0], str):
        if root.label() != '-NONE-':
            root.token_id = tok_id
            tok_id += 1
    else:
        for child in root:
            tok_id = _assign_token_ids(child, tok_id)
    return tok_id

def _find_terminals(root):
    assert sys.version_info >= (3, 0), 'Python2 will break _find_terminals method, please use Python3 instead.'
    assert isinstance(root, Tree)
    target_list = []
    if len(root) == 1 and isinstance(root[0], str):
        target_list.append(root)
    else:
        for child in root:
            assert child is not root
            target_list.extend(_find_terminals(child))
    root.terminals = target_list
    return target_list
    
def _tree_from_string(s):
    root = ParentedTree.fromstring(s)
    _find_terminals(root)
    _assign_token_ids(root, 0)
#     print(' '.join(t[0] for t in root.terminals))
#     print(' '.join(str(t.token_id) for t in root.terminals))
    return root

def _iter_syntax_trees(path):
    with codecs.open(path, 'r', 'utf-8') as f:
        parenthese_count = 0
        buf = []
        line = f.readline()
        while line != '':
            if parenthese_count == 0:
                s = ' '.join(buf).strip()
                if s: yield s
                del buf[:]
            buf.append(line)
            parenthese_count += sum(1 for c in line if c == '(')
            parenthese_count -= sum(1 for c in line if c == ')')
            line = f.readline()
        s = ' '.join(buf).strip()
        if s: yield s
    
def load_trees(path):
    trees = []
    for s in _iter_syntax_trees(path):
        root = _tree_from_string(s)
        trees.append(root)
    return trees

def find_constituent(token_ids, tree):
    '''
    Find the minimum constituent (subtree) that contains all the specified tokens.
    '''
    if not isinstance(tree, Tree): return None
    if not isinstance(token_ids, set): token_ids = set(token_ids)
    tree_token_ids = set(x.token_id for x in tree.terminals)
    if tree_token_ids.issuperset(token_ids):
        for c in tree:
            ret = find_constituent(token_ids, c)
            if ret is not None: return ret
        return tree
    return None
