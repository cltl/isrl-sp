from data import Document, Pbparser, add_fe_to_file
from collections import defaultdict
from lxml import etree
import itertools
import numpy as np
import subprocess, os, sys
import random
from datetime import date
import re
from util import dice
import shutil
from subprocess import check_output
   
out_dir = None

changed = bool(check_output(['git', 'diff']))
if changed:
    version = date.today().strftime('%Y-%m-%d') + '-' + 'working'
else:
    version = (check_output('git show -s --format=%ci', shell=True)[:10] +
               check_output('git show -s --format=-%h', shell=True).strip()).decode()
    

def setup_experiment(seed):
    global out_dir
    if seed is not None:
        random.seed(int(seed))
        np.random.seed(random.randint(0, 10000000))
        out_dir = os.path.join('out', version, str(seed))
    else:
        out_dir = os.path.join('out', version)
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    
    # Unbuffer output
    # Due to this issue it can't be unbuffered: http://bugs.python.org/issue17404
    # But setting buffer to 1 is practically the same
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'wt', 1)
    log_path = os.path.join(out_dir, os.path.basename(sys.argv[0]) + '.out')
    tee = subprocess.Popen(["tee", log_path], stdin=subprocess.PIPE)
    os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
    os.dup2(tee.stdin.fileno(), sys.stderr.fileno())
    sys.stderr.write('Output dir: %s\n' %out_dir)
    sys.stderr.write('Log is written to: %s\n' %log_path)

predominant_roles = defaultdict(set)

def read_predominant_roles(path='data/predominant-role-set-ontonotes.tsv'):
    with open(path) as f:
        for line in f:
            fields = line.strip().split('\t')
            if fields[2] != '_':
                predominant_roles[fields[0]] = set(fields[1].split(','))
    return predominant_roles

def find_all_overt(frame_elem):
    overt_roles = set()
    overt_fillers = set()
    for e in frame_elem.iterfind('fe'):
        if (e.find("flag[@name='Definite_Interpretation']") is None and
                e.find("flag[@name='Infinite_Interpretation']") is None and
                e.find('fenode') is not None):
            overt_roles.add(e.get('name'))
            overt_fillers.add(e.find('fenode').get('idref'))
    return overt_roles, overt_fillers

def xue_palmer_tunining(doc, frame_ref):
    ids = set()
    ref = doc.id2sparent.get(frame_ref)
    while ref is not None:
        ids.update(doc.id2schildren[ref])
        ref = doc.id2sparent.get(ref)
    return ids

def iter_candidates1(doc, frame_elem, prev_sents=2):
    curr_sent = doc.id2sent[frame_elem.get('id')]
    for sent in reversed(range(max(curr_sent-prev_sents, 0), curr_sent+1)):
        s = doc.sentences[sent]
        # by default it goes from small to big, left to right
        constituents = list(itertools.chain(s.iterfind('graph/terminals/t'),
                                            s.iterfind('graph/nonterminals/nt')))
        for e in constituents:
            yield e
    
def iter_candidates(doc, frame_elem, overt_fillers, prev_sents=2):
    frame_ref = frame_elem.find('target/fenode').get('idref')
    for e in iter_candidates1(doc, frame_elem, prev_sents):
            e_id = e.get('id')
            if e_id != frame_ref and e_id not in overt_fillers:
                yield e

# def keep_one_coreferent(doc, candidates):
#     all_coref_candidates = set()
#     for c in candidates:
#         if c.get('id') not in all_coref_candidates:
#             all_coref_candidates.update(doc.coref.get(c.get('id')) or [c.get('id')])
#             yield c

def keep_max_constituent(doc, candidates):
    for c in candidates:
        c_id = c.get('id')
        if not (c_id in doc.id2head and # parent doesn't has the same head  
                doc.id2head.get(doc.id2sparent.get(c_id)) == doc.id2head[c_id]):
            yield c 

semeval2010_data_dir = 'data/SEMEVAL/'
    
class FrameNetSemEval2010(object):
    
    def __init__(self):
        self.train_path = semeval2010_data_dir + 'Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingFN/tiger/TigerOfSanPedro.withHeads.xml' 
        self.test_path = semeval2010_data_dir + 'task10_test_data-full+ni_only/ni_only/FrameNet/no_unseen'
        self.gold_path = semeval2010_data_dir + 'test_gold/FrameNet'
        self.test2gold = {'13.headful.xml.no_unseen.xml_no-nullinstantiation.xml': '13.headful.xml.no_unseen.coref.xml',
                          '14.headful.xml.no_unseen.xml_no-nullinstantiation.xml': '14.headful.xml.no_unseen.coref.xml',}
    
    def predict_doc(self, doc, predict_func):
        added_fe_elems = []
        for frame_elem in doc.frames:
            overt_roles, overt_fillers = find_all_overt(frame_elem)
            pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
            pos = pred.get('pos').lower()[0]
            predicted_pairs = predict_func(doc, frame_elem, pos, 
                                           overt_roles, overt_fillers)
            for role, filler_id in predicted_pairs:
                if filler_id == 'INI':
                    # (Minh 2017-09-25) A INI can be added here but the official
                    # scorer breaks whenever I insert an INI (with empty filler)
                    # so I leave them out for now
                    # TODO: write a new scorer and add them back
#                     fe_elem = etree.SubElement(frame_elem, 'fe', attrib={'name': role})
#                     etree.SubElement(fe_elem, 'flag', attrib={'name': 'Indefinite_Interpretation'})
#                     added_fe_elems.append(fe_elem)
                    pass
                else:
                    fe_elem = etree.SubElement(frame_elem, 'fe', attrib={'name': role})
                    etree.SubElement(fe_elem, 'fenode', attrib={'idref': filler_id})
                    etree.SubElement(fe_elem, 'flag', attrib={'name': 'Definite_Interpretation'})
                    added_fe_elems.append(fe_elem)
        return added_fe_elems

    def predict_file(self, gold_path, out_path, predict_func):
        test_doc = Document(gold_path)
        self.predict_doc(test_doc)
        with open(out_path, 'wb') as f:
            f.write(etree.tostring(test_doc.elem, pretty_print=True))        
        cmd = ('java -jar sem10scorer.jar gold=%s annotation=%s '
               'task=NoInstantiation' %(gold_path, out_path))
        print(cmd)
        os.system(cmd)

    def predict_all(self, predict_func):
        for test_name in sorted(self.test2gold):
            gold_path = os.path.join(self.gold_path, self.test2gold[test_name])
            out_path = os.path.join(out_dir, test_name)
            self.predict_file(gold_path, out_path)      

    def resolve_dnis_in_doc(self, doc, predict_func):
        added_fe_elems = []
        for flag_elem in doc.elem.iterfind('.//flag[@name="Indefinite_Interpretation"]'):
            added_fe_elems.append(flag_elem.getparent()) # because we only focus on DNIs
        for flag_elem in doc.elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
            fe_elem = flag_elem.getparent()
            fenode_elem = fe_elem.find('fenode')
            if fenode_elem is not None:
                fe_elem.remove(fenode_elem)
                frame_elem = fe_elem.getparent()
                
                overt_roles, overt_fillers = find_all_overt(frame_elem)
                pos = doc.id2elem[frame_elem.find('target/fenode').get('idref')].get('pos')
                role = fe_elem.get('name')
                
                predicted_id = predict_func(doc, frame_elem, pos, role, overt_roles, overt_fillers)
                if predicted_id is not None:
                    etree.SubElement(fe_elem, 'fenode', attrib={'idref': predicted_id})
                    added_fe_elems.append(fe_elem)
                else:
                    sys.stderr.write('Ignored one DNI because prediction function returns None.\n')
            else:
                sys.stderr.write('Ignored one DNI because no gold fenode was found.\n')
        return added_fe_elems
    
class PropBankSemEval2010(FrameNetSemEval2010):
    
    def __init__(self):
        self.train_no_dni_path = semeval2010_data_dir + 'Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.no_dni.txt'
        self.train_path = semeval2010_data_dir + 'Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.txt'
        self.test_path = semeval2010_data_dir + 'task10_test_data-full+ni_only/ni_only/PropBank/'
        self.gold_path = semeval2010_data_dir + 'test_gold/PropBank'
        self.test2gold = {'13_PBNB.stripped.txt': '13_PBNB.txt',
                          '14_PBNB.stripped.txt': '14_PBNB.txt'}
    
    def predict_file(self, predict_func, in_path, out_conll_path, out_xml_path, gold_path):
        test_doc = Document(in_path, Pbparser())
        fes = self.predict_doc(test_doc, predict_func)
        add_fe_to_file(test_doc, fes, in_path, out_conll_path)
        cmd = ('java -jar sem10scorer.jar gold=%s annotation=%s '
               'task=NoInstantiation' %(gold_path, out_conll_path))
        print(cmd)
        os.system(cmd)    
    
    def predict_all(self, predict_func, model_name):
        for test_name in sorted(self.test2gold):
            in_path = os.path.join(self.test_path, test_name)
            out_conll_path = os.path.join(out_dir, '%s-%s.txt' %(model_name, test_name))
            out_xml_path = os.path.join(out_dir, '%s-%s.xml' %(model_name, test_name))
            gold_path = os.path.join(self.gold_path, self.test2gold[test_name])
            self.predict_file(predict_func, in_path, out_conll_path, out_xml_path, gold_path)
    
    def resolve_dnis_in_file(self, predict_func, in_path, out_conll_path, out_xml_path, gold_path):
        doc = Document(gold_path, Pbparser()) # will remove fenodes later
        fes = self.resolve_dnis_in_doc(doc, predict_func)
        add_fe_to_file(doc, fes, in_path, out_conll_path)
        cmd = ('java -jar sem10scorer.jar gold=%s annotation=%s '
               'task=NoInstantiation' %(gold_path, out_conll_path))
        print(cmd)
        os.system(cmd)
    
    def resolve_dnis(self, predict_func, model_name):
        train_fname = os.path.basename(self.train_path)
        out_conll_path = os.path.join(out_dir, '%s-%s.txt' %(model_name, train_fname))
        out_xml_path = os.path.join(out_dir, '%s-%s.xml' %(model_name, train_fname)) 
        self.resolve_dnis_in_file(predict_func, self.train_no_dni_path, 
                                  out_conll_path, out_xml_path, self.train_path)
        
        for test_name in sorted(self.test2gold):
            in_path = os.path.join(self.test_path, test_name)
            out_conll_path = os.path.join(out_dir, '%s-%s.txt' %(model_name, test_name))
            out_xml_path = os.path.join(out_dir, '%s-%s.xml' %(model_name, test_name))
            gold_path = os.path.join(self.gold_path, self.test2gold[test_name])
            self.resolve_dnis_in_file(predict_func, in_path, out_conll_path, out_xml_path, gold_path)

    def predict_doc_hit_at_n(self, doc, predict_func, n=5):
        found = 0
        total = 0
        for flag_elem in doc.elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
            fe_elem = flag_elem.getparent()
            fenode_elem = fe_elem.find('fenode')
            if fenode_elem is not None:
                y = fenode_elem.get('idref')
                y_coref = doc.coref[y] if y in doc.coref else {y}
                frame_elem = fe_elem.getparent()
                
                overt_roles, overt_fillers = find_all_overt(frame_elem)
                frame = frame_elem.get('name')
                pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
                pos = pred.get('pos')
                role = fe_elem.get('name')
                
                candidates = list(iter_candidates(doc, frame_elem, overt_fillers))
                probs = predict_func(doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates)
                arg_ids = [c.get('id') for c in candidates]
                best_n = [arg_ids[i] for i in np.argsort(-probs)][:n]
                
                print('-'*40)
                s = doc.id2sent[frame_elem.get('id')]
                for i in range(max(0, s-2), s+1):
                    print("sentence %d: %s" %(i-s, " ".join(doc.id2words[doc.sentences[i].get('id')])))
                print('Frame: %s, role: %s' %(frame, role))
                print(('answer: %s, best candidates: %s' 
                       %(doc.head_word(y), ', '.join(' '.join(doc.id2words[i]) 
                                                       for i in best_n))))
                print('--> ' + ('found' if y_coref.intersection(best_n) else 'missed !!'))
                if not y_coref.intersection(arg_ids): 
                    sys.stderr.write('correct answer not among candidates\n')
                if y_coref.intersection(best_n): found += 1
                total += 1
        print("%d found / %d total = %f" %(found, total, found/float(total)))

    def predict_all_hit_at_n(self, predict_func, n):
        for test_name in sorted(self.test2gold):
            in_path = os.path.join(self.test_path, test_name)
            gold_path = os.path.join(self.gold_path, self.test2gold[test_name])
            test_doc = Document(gold_path, Pbparser()) # will remove fenodes later
            fes = self.predict_doc_hit_at_n(test_doc, predict_func, n)
    
    def evaluate_hit_at_n(self, model, n):
        self.predict_all_hit_at_n(model, n)
    
fn_semeval = FrameNetSemEval2010()
pb_semeval = PropBankSemEval2010()

class Scorer(object):
    
    def __init__(self, mode):
        '''
        mode can be either 'semeval' or 'gerberchai'.
        'semeval' mode counts each filler that span the gold head as correct
        'gerberchai' model computes the Dice coefficient of gold and predicted filler spans
        '''
        self.mode = mode
        self.p_vals = []
        self.r_vals = []
        self.f1_vals = []
        
    def clear(self):
        self.p_vals, self.r_vals, self.f1_vals = [], [], []

    def evaluate_ni_resolution(self, test_dir, predict_func):
        total_score = 0
        predicted_dnis = 0
        annotated_dnis = 0
        for fname in os.listdir(test_dir):
            if re.search('\.xml$', fname):
                path = os.path.join(test_dir, fname)
                doc = Document(path)
                for frame_elem in doc.frames:
                    role2gold_fillers = {}
                    for flag_elem in frame_elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
                        fe = flag_elem.getparent()
                        idref = fe.find('fenode').get('idref')
                        assert fe.get('name') not in role2gold_fillers
                        if idref in doc.coref:
                            gold_fillers = doc.coref[idref]
                        else: 
                            gold_fillers = (idref,)
                        role2gold_fillers[fe.get('name')] = gold_fillers
                    annotated_dnis += len(role2gold_fillers)

                    overt_roles, overt_fillers = find_all_overt(frame_elem)
                    pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
                    pos = pred.get('pos').lower()[0]
                    predicted_pairs = predict_func(doc, frame_elem, pos, 
                                                   overt_roles, overt_fillers)
                    for role, predicted_filler in predicted_pairs:
                        if predicted_filler != 'INI':
                            predicted_dnis += 1
                            total_score += self.score_instance(doc, predicted_filler, 
                                                               role2gold_fillers.get(role, []))
        p = total_score / predicted_dnis if predicted_dnis > 0 else 0
        r = total_score / annotated_dnis if annotated_dnis > 0 else 0
        f1 = 2 / (1/p + 1/r) if (p > 0 and r > 0) else 0
        self.p_vals.append(p)                        
        self.r_vals.append(r)
        self.f1_vals.append(f1)
        
    def evaluate_dni_linking(self, test_dir, cv_out_dir, predict_func):
        total_score = 0
        predicted_dnis = 0
        annotated_dnis = 0
        if os.path.exists(cv_out_dir):
            shutil.rmtree(cv_out_dir, ignore_errors=True)
        os.makedirs(cv_out_dir)

        for fname in os.listdir(test_dir):
            if re.search('\.xml$', fname):
                path = os.path.join(test_dir, fname)
                doc = Document(path)
                for frame_elem in doc.frames:
                    for flag_elem in frame_elem.iterfind('.//flag[@name="Definite_Interpretation"]'):
                        fe_elem = flag_elem.getparent()
                        role = fe_elem.get('name')
                        idref = fe_elem.find('fenode').get('idref')
                        if idref in doc.coref:
                            gold_fillers = doc.coref[idref]
                        else: 
                            gold_fillers = (idref,)
                        annotated_dnis += 1

                        overt_roles, overt_fillers = find_all_overt(frame_elem)
                        pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
                        pos = pred.get('pos').lower()[0]
                        predicted_filler = predict_func(doc, frame_elem, pos, role, overt_roles, overt_fillers)
                        
                        if predicted_filler is not None:
                            predicted_dnis += 1
                            total_score += self.score_instance(doc, predicted_filler, gold_fillers)
                            
                        # override gold fillers
                        if predicted_filler is None:
                            fe_elem.remove(fe_elem.find('fenode'))
                        else:
                            fe_elem.find('fenode').set('idref', predicted_filler)
            
                out_path = os.path.join(cv_out_dir, fname)
                doc.elem.write(out_path, pretty_print=True, encoding='utf-8')
        p = total_score / predicted_dnis if predicted_dnis > 0 else 0
        r = total_score / annotated_dnis if annotated_dnis > 0 else 0
        f1 = 2 / (1/p + 1/r) if (p > 0 and r > 0) else 0
        self.p_vals.append(p)                        
        self.r_vals.append(r)
        self.f1_vals.append(f1)
        
    def score_instance(self, doc, predicted_filler, gold_fillers):
        scores = [0.0]
        predicted_tokens = doc.collect_tokens(predicted_filler)
        if self.mode == 'gerberchai':
            for gold_filler in gold_fillers:
                gold_tokens = doc.collect_tokens(gold_filler)
                scores.append(dice(predicted_tokens, gold_tokens))
        elif self.mode == 'semeval':
            for gold_filler in gold_fillers:
                gold_head = doc.id2head.get(gold_filler)
                scores.append(1 if gold_head in predicted_tokens else 0)
        else:
            raise ValueError('Unsupported mode: %s' %self.mode)
        return max(scores)

class ON5VCrossValidation:

    def __init__(self, scorer):
        self.scorer = scorer
    
    def resolve_dnis(self, predict_func, model_name):
        self.scorer.clear()
        from preprocess_on5v import out_dir_cv
        for dir_cv in out_dir_cv:
            test_dir = os.path.join(dir_cv, 'test')
            cv_out_dir = os.path.join(out_dir, 'on5v_%s' %model_name, os.path.basename(dir_cv))
            self.scorer.evaluate_dni_linking(test_dir, cv_out_dir, predict_func)
        print(self.scorer.p_vals)
        print(self.scorer.r_vals)
        print(self.scorer.f1_vals)

on5v_cv = ON5VCrossValidation(Scorer('semeval'))                

def remove_relation_tag(tag):
    return re.sub('(-[\d\w]+)+', '', tag)

def silberer_frank_filter(subtree):
    '''Accept only subtrees that are labeled NPB, S, VP, SBAR, or SG as 
    described in Silberer & Frank (2012).
    '''
    accepted_phrases = set(['S', 'VP', 'SBAR'])
    label = remove_relation_tag(subtree.label())
    if label in accepted_phrases:
        return True
    # detect NPB
    elif label == 'NP':
        found_non_possessive_np = False
        for s2 in subtree.subtrees(lambda t: t != subtree):
            if (remove_relation_tag(s2.label()) == 'NP' and 
                not any(1 for s3 in s2 if remove_relation_tag(s3.label()) == 'POS')):
                found_non_possessive_np = True
                break
        if not found_non_possessive_np:
            return True
    # no need to detect SG since S already covers all SG's
    return False 

def silberer_frank_filter_elem(doc, candidate):
    cat = remove_relation_tag(candidate.get('cat', ''))
    if cat in ['S', 'VP', 'SBAR']:
        return True
    # detect NPB
    elif cat == 'NP':
        found_non_possessive_np = False
        for child_id in doc.id2schildren[candidate.get('id')]:
            child_elem = doc.id2elem[child_id]
            if remove_relation_tag(child_elem.get('cat', '')) == 'NP':
                gchild_elems = [doc.id2elem[p] for p in doc.id2schildren[child_id]]
                if not any(1 for e in gchild_elems if e.get('pos') == 'POS'):
                    found_non_possessive_np = True
                    break
        if not found_non_possessive_np:
            return True
    return False

def silberer_frank_filter_candidates(candidate_func):
    '''Create a new candidate funciton that accepts only subtrees that are 
    labeled NPB, S, VP, SBAR, or SG as described in Silberer & Frank (2012).
    
    Input: iterator of <t> or <nt> elements (in Tiger format).
    Output: iterator of those elements that are eligible
    '''
    def new_func(doc, frame_elem, overt_fillers, prev_sents=2):
        for candidate in candidate_func(doc, frame_elem, overt_fillers, prev_sents):
            if silberer_frank_filter_elem(doc, candidate):
                yield candidate
    return new_func

