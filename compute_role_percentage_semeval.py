from data import train_semeval_pb_path, test_pb_path, test2gold, Document,\
    Pbparser
import os
from collections import Counter, defaultdict

c = Counter()
c2 = defaultdict(Counter)
paths = [train_semeval_pb_path] + [os.path.join(test_pb_path, fname) for fname in test2gold]
for path in paths:
    doc = Document(path, Pbparser())
    for t in doc.elem.iterfind('.//t'):
        c[t.get('lemma')] += 1
    for fe in doc.iterfind('.//fe'):
        if fe.find('fenode') is not None:
            role = fe.get('name')
            filler_id = fe.find('fenode').get('idref')
            if filler_id in doc.id2head:
                head = doc.id2elem[doc.id2head[filler_id]].get('lemma')
                c2[head][role] += 1
         
for lemma in sorted(c2):
    total_freq = c[lemma]
    for role in c2[lemma]:
        print('%s\t%s\t%f' %(lemma, role, c2[lemma][role]/float(total_freq)))