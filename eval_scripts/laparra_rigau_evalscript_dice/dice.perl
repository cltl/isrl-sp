use strict;

if ($#ARGV < 0) { die "perl dice.perl [-v] gold-standard prediction\n\n" };

sub dice
{
    my $prediction=$_[0];
    my $gs=$_[1];

    my $dice=0;

    foreach my $true (@{ $gs })
    {
	my $head=$true->{head};
	my @heads = split(/\#/,$head);

	my $intersection=0;
	my $head_covered=0;

	for (my $i=0;$i<=$#{ $prediction };$i++)
	{
	    my $p=${ $prediction }[$i];
	    
	    foreach my $h (@heads)
	    {
		if ($p eq $h)
		{
		    $head_covered=1;
		}
	    }

	    for (my $j=0;$j<=$#{ $true->{tokens} };$j++)
	    {
		my $t=${ $true->{tokens} }[$j];
		
		if ($p eq $t)
		{
		    $intersection++;
		}
	    }
	}

	my $coeficient=(2*$intersection/(($#{ $prediction }+1)+($#{ $true->{tokens} }+1)));

	if ($coeficient>$dice && $head_covered)
	{
	    $dice=$coeficient;
	}
    }

    return $dice;
}


open(ANS,$ARGV[1]);
my $prediction;
my $total_predictions=0;
my $total_predictions_per_predicate;
my $scores;
while (my $line=<ANS>)
{
    chop $line;
    
    if ($line ne "")
    {
	my ($target,$pred,$arg,@filler)=split(" ",$line);
	
	#my $score = pop @filler;
	#$scores->{$target." ".$pred." ".$arg} = $score;
	my @aux;
	foreach my $f (@filler)
	{
	    my $is_new = 1;
	    foreach my $a (@aux)
	    {
		if ($f eq $a)
		{
		    $is_new = 0;
		}
	    }
	    if ($is_new)
	    {
		push @aux,$f;
	    }
	}

	@{ $prediction->{$target." ".$arg} } = @aux;

	if ($pred ne "_")
	{
	    $total_predictions_per_predicate->{$pred}++;
	}
	$total_predictions++;
    }
}
close(ANS);

open(GS,$ARGV[0]);
my $gs;
my $total_trues=0;
my $total_trues_per_predicate;
while (my $line=<GS>)
{
    chop $line;

    if ($line ne "")
    {
	my ($target,$predicate,$arg,$head,@trues)=split(" ",$line);

	my $true_filler;
	$true_filler->{head} = $head;
	
	my @aux;
	foreach my $t (@trues)
	{
	    my $is_new = 1;
	    foreach my $a (@aux)
	    {
		if ($t eq $a)
		{
		    $is_new = 0;
		}
	    }
	    if ($is_new)
	    {
		push @aux,$t;
	    }
	}

	@{ $true_filler->{tokens} } = @aux;


	if (not exists $gs->{$target." ".$arg} )
	{
	    if ($predicate ne "_")
	    {
		$total_trues_per_predicate->{$predicate}++;
	    }
	    $total_trues++;
	}

	$gs->{$target." ".$arg}->{predicate} = $predicate;
	push @{ $gs->{$target." ".$arg}->{fillers} }, $true_filler;
    }
}
close(GS);

my $dice_value;
my $dice_value_per_predicate;
foreach my $target_arg (keys %{ $gs })
{
    my $dice=&dice($prediction->{$target_arg},$gs->{$target_arg}->{fillers});
    
    my ($target,$arg) = split(/ /,$target_arg);
    
    if (exists $prediction->{$target_arg})
    {
	print $target." ".$gs->{$target_arg}->{predicate}." ".$arg." ".join("-",@{ $prediction->{$target_arg} })." ".$scores->{$target." ".$gs->{$target_arg}->{predicate}." ".$arg}." ".$dice."\n";
    }
    else
    {
	print $target." ".$gs->{$target_arg}->{predicate}." ".$arg." _ 0 ".$dice."\n";
    }

#      if ($dice != 0)
#      {
# 	 $dice_value_per_predicate->{$gs->{$target_arg}->{predicate}}++;
# 	 $dice_value++;
#      }
    
    if ($gs->{$target_arg}->{predicate} ne "_")
    {
	$dice_value_per_predicate->{$gs->{$target_arg}->{predicate}} = $dice_value_per_predicate->{$gs->{$target_arg}->{predicate}}+$dice;
    }
    $dice_value=$dice_value+$dice;
}
close(GS);


print "\n";

foreach my $predicate (keys %{ $total_trues_per_predicate })
{
    print "===== ".$predicate." =======\n";

    my $total_trues = $total_trues_per_predicate->{$predicate};
    my $total_predictions = $total_predictions_per_predicate->{$predicate};
    my $dice_value = $dice_value_per_predicate->{$predicate};

    my $precision;
    if ($total_predictions == 0)
    {
	$precision=0;
    }
    else
    {
	$precision=$dice_value/$total_predictions;
    }
    my $recall=$dice_value/$total_trues;
    my $fscore;
    if ($precision+$recall==0)
    {
	$fscore=0;
    }
    else
    {
	$fscore=2*$precision*$recall/($precision+$recall);
    }
    
    
    print "Precision: ".$precision."\n";
    print "Recall: ".$recall."\n";
    print "F-score: ".$fscore."\n";   
    print "\n";
}


my $precision;
if ($total_predictions == 0)
{
    $precision=0;
}
else
{
    $precision=$dice_value/$total_predictions;
}
my $recall=$dice_value/$total_trues;
my $fscore;
if ($precision+$recall==0)
{
    $fscore=0;
}
else
{
    $fscore=2*$precision*$recall/($precision+$recall);
}

print "===== Overall =======\n";
print "Precision: ".$precision."\n";
print "Recall: ".$recall."\n";
print "F-score: ".$fscore."\n";

