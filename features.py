from data import Document, mark_role, get_wordnet_pos, lemmatizer
from collections import Counter
from isrl import fn_semeval
import math
import count_lemma_frequency_ontonotes
import compute_role_percentage_ontonotes
import codecs

def c_command(doc, pred, arg, frame, role):
    return ""

def candidate_head_pos(doc, pred, arg, frame, role):
    if arg.get("id") in doc.id2head:
        return doc.id2elem[doc.id2head[arg.get("id")]].get("pos")
    else:
        return "__NULL__"

def candidate_constituent_type(doc, pred, arg, frame, role):
    if arg.tag == 'nt':
        return arg.get("cat")
    elif arg.tag == 't':
        return arg.get("pos")
    else:
        raise "Unsupported tag: " + arg

def candidate_salience(doc, pred, arg, frame, role):
    '''
    "Whether the candidate realization's head word is included in a 
    non-singleton coreference chain" (Feizabadi and Pado, 2015) 
    '''
    return doc.id2head.get(arg.get("id")) in doc.coref

def distance(doc, pred, arg, frame, role):
    return abs(doc.id2sent[pred.get('id')] - doc.id2sent[arg.get('id')])

def previouse_role(doc, pred, arg, frame, role):
    return bool(doc.id2ovrs[arg.get('id')])

def same_role(doc, pred, arg, frame, role):
    for fe_id in doc.id2ovrs[arg.get('id')]:
        if doc.id2elem[fe_id].get('name') == role:
            return True
    return False

class CandidateSemanticType(object):
    '''
    The semantic type of a candidate filler.
    '''
    
    def __init__(self, follow_coref):
        self.__name__ = 'CandidateSemanticType(follow_coref=%s)' %str(follow_coref)
        self.follow_coref = follow_coref

    def __call__(self, doc, pred, arg, frame, role):
        arg_id = arg.get("id")
        supersense = doc.id2supersense.get(doc.id2head.get(arg_id))
        if not supersense and self.follow_coref:
            for c in doc.coref.get(arg_id) or []:
                supersense = doc.id2supersense.get(doc.id2head.get(c))
                if supersense: break 
        if not supersense and arg_id in doc.id2head:
            head_pos = doc.id2elem[doc.id2head[arg_id]].get('pos')
            if 'NN' in head_pos: # all nouns
                supersense = 'noun.Tops'
            elif 'JJ' in head_pos: # all adj 
                supersense = 'adj.all'
            elif 'RB' in head_pos:
                supersense = 'adv.all'
        return supersense or "__NULL__"

class CandidateLemmaFrequency:
    
    def __init__(self, path):
        self.__name__ = 'CandidateLemmaFrequency(%s)' %path
        self.c = Counter()
        with codecs.open(path, 'r', 'utf-8') as f:
            for line in f:
                lemma, count = line.strip().split('\t')
                self.c[lemma] = int(count)

    def __call__(self, doc, pred, arg, frame, role):
        return self.from_id(doc, arg.get('id'))
        
    def from_id(self, doc, idref):
        if idref not in doc.id2head: return '__NULL__'
        head_elem = doc.id2elem[doc.id2head[idref]]
        lemma = head_elem.get('lemma')
        if not lemma:
            word = head_elem.get('word')
            pos = head_elem.get('pos')
            lemma = lemmatizer.lemmatize(word, get_wordnet_pos(pos) or 'n')
        return self.from_lemma(lemma)
    
    def from_lemma(self, w):
        if w not in self.c: return '__NULL__'
        count = self.c[w]
        return round(math.log10(count)*2)/2

candidate_lemma_frequency_semeval = CandidateLemmaFrequency('data/lemma-frequency-semeval.txt')
candidate_lemma_frequency_ontonotes = CandidateLemmaFrequency('out/lemma_frequency_ontonotes.2018-01-04-d8c47e8.txt')

class RolePercentage:
    
    def __init__(self, path):
        self.__name__ = 'RolePercentage(%s)' %path
        self.c = {}
        with codecs.open(path, 'r', 'utf-8') as f:
            for line in f:
                lemma, role, count = line.strip().split('\t')
                self.c[(lemma, role)] = float(count)

    def __call__(self, doc, pred, arg, frame, role):
        head_lemma = None
        head_elem = doc.id2elem.get(doc.id2head.get(arg.get('id')))
        if head_elem is not None:
            head_lemma = head_elem.get('lemma')
            if not head_lemma:
                word = head_elem.get('word')
                pos = head_elem.get('pos')
                head_lemma = lemmatizer.lemmatize(word, get_wordnet_pos(pos) or 'n')
        if (head_lemma, role) not in self.c: 
            return '__NULL__'
        return round(self.c[(head_lemma, role)]*2, 1)/2

role_percentage_semeval = RolePercentage('data/role-percentage-semeval.txt')
role_percentage_ontonotes = RolePercentage('out/role-percentage-ontonotes.2018-01-26-cfbc1ca.txt')

class ExpectedRoles:
    '''
    A delexicalized representation of the predicate
    '''
    
    def __init__(self, path):
        self.__name__ = 'ExpectedRoles(%s)' %path
        self.c = {}
        with codecs.open(path, 'r', 'utf-8') as f:
            for line in f:
                roleset, roles = line.strip().split('\t')
                self.c[roleset] = roles

    def __call__(self, doc, pred, arg, frame, role):
        assert frame is not None
        return self.c.get(frame) or '__NULL__'

expected_roles = ExpectedRoles('data/expected-roles-nombank-propbank.txt')

class FeatureSet:
    
    def __init__(self, feature_funcs):
        self.feature_funcs = feature_funcs
        
    def __len__(self):
        return len(self.feature_funcs)
        
    def __call__(self, doc, pred, arg, frame, role):
        return [f(doc, pred, arg, frame, role) for f in self.feature_funcs]

feizabadi_pado_features_10 = FeatureSet((
        expected_roles,
        CandidateSemanticType(True),
        candidate_lemma_frequency_semeval,
        candidate_head_pos, 
        candidate_constituent_type, 
        distance, 
        candidate_salience,
        previouse_role, 
        same_role,
        role_percentage_semeval,
    ))

feizabadi_pado_features_10_ontonotes = FeatureSet((
        expected_roles,
        CandidateSemanticType(True),
        candidate_lemma_frequency_ontonotes,
        candidate_head_pos, 
        candidate_constituent_type, 
        distance, 
        candidate_salience,
        previouse_role, 
        same_role,
        role_percentage_ontonotes,
    ))

feizabadi_pado_candidate_features = FeatureSet((
        CandidateSemanticType(True),
        candidate_lemma_frequency_semeval,
        candidate_head_pos, 
        candidate_constituent_type, 
        distance, 
        candidate_salience,
        previouse_role, 
        same_role,
        role_percentage_semeval,
    ))

feizabadi_pado_features_7 = FeatureSet((
        # expected_roles
        CandidateSemanticType(True),
        # candidate_lemma_frequency_semeval,
        candidate_head_pos,
        candidate_constituent_type, 
        distance, 
        candidate_salience,
        previouse_role, 
        same_role,
        # RolePercentage('data/role-percentage-semeval.txt'),
    ))

feizabadi_pado_features_6 = FeatureSet((
        # expected_roles,
        # semantic_type,
        # candidate_lemma_frequency_semeval,
        candidate_head_pos,
        candidate_constituent_type, 
        distance, 
        candidate_salience,
        previouse_role, 
        same_role,
        # RolePercentage('data/role-percentage-semeval.txt'),
    ))

synsem_features = FeatureSet((
        expected_roles, CandidateSemanticType(True), 
        candidate_lemma_frequency_semeval, candidate_head_pos, 
        candidate_constituent_type
    ))

synsem_features_for_candidates = FeatureSet((
        CandidateSemanticType(True), candidate_lemma_frequency_semeval, 
        candidate_head_pos, candidate_constituent_type
    ))

if __name__ == '__main__':
    doc = Document(fn_semeval.train_path)
    pred = doc.id2elem["s335_f1229595861.27062"]
    arg = doc.id2elem["s335_5"]
    frame = "Revenge"
    role = "Offender"
    print(feizabadi_pado_features_10(doc, pred, arg, frame, role))
