import os
import re
from lxml import etree
import sys

# replace with your paths
nombank_path = '/Users/cumeo/Downloads/nombank.1.0'
ontonotes_path = '/Users/cumeo/Downloads/ontonotes-release-5.0'

nombank_dict_path = os.path.join(nombank_path, 'nombank-dict.1.0')
ontonotes_frames_path = os.path.join(ontonotes_path, 'data/files/data/english/metadata/frames')

if __name__ == '__main__':
    with open(nombank_dict_path) as f:
        s = f.read()
        parts = s.split('(ROLE-SET')
        for part in parts[1:]:
            role_set = re.search(r':ID "(\S+)"', part).group(1)
            roles = ",".join('A' + m.group(1) for m in 
                             re.finditer(r'\(ROLE.+:N\s+"(\d+)"', part))
            if roles:
                print('%s\t%s' %(role_set, roles))
            else:
                sys.stderr.write('Skipped one role set: %s\n' %role_set)
            
    for fname in sorted(os.listdir(ontonotes_frames_path)):
        if re.search(r'\.xml$', fname):
            root = etree.parse(os.path.join(ontonotes_frames_path, fname))
            for roleset_elem in root.iterfind('.//roleset'):
                role_set = roleset_elem.get('id')
                roles = ','.join('A' + role_elem.get('n') for role_elem in
                                 roleset_elem.iterfind('roles/role'))
                if roles:
                    print('%s\t%s' %(role_set, roles))
                else:
                    sys.stderr.write('Skipped one role set: %s\n' %role_set)