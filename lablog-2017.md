*** The content of this file is imported from a previous lab log in which I 
took note of multiple projects. All dates are in 2017 ***


## Tue 11 Apr 2017

Will talk to Tommaso tomorrow. Now I will work a bit on iSRL, trying to make the IJCNLP deadline (July 7). 
Enabled coreference resolution on iSRL. Reading Moor et al. (2013)...

## Fri 14 Apr

(isrl) The modified model works equally well. Now I need to actually untie the weight matrices.

## Wed 19 Apr

In meantime, I'll work on iSRL. The deadline of RANLP is 28 Apr (abstract) and
5 May (full paper). I still want to try IJCNLP (July 7).

## Tue 25 Apr

Right now I have to go back to iSRL. The RANLP deadline is approaching very fast.

## Sun 30 Apr

Test these models:

```
ubuntu@packer-ubuntu-16:/data/isrl-sp$ grep "saved" run-neural-vm0.sh.out | uniq
Best model saved into out/bd79cd8999cee495a63047f4897bbdb81ff1d232/872642/coherence-model.npz
Best model saved into out/working-dir/356881/predicate-only.npz
Best model saved into out/working-dir/2892/synsem.npz
Best model saved into out/working-dir/95827552/predicate-with-synsem-features.npz
Best model saved into out/working-dir/1785262/coherence-with-synsem-features.npz
```

- cohrence-model: 0.14150943396226415
- predicate-only: 0.02358490566037736
- synsem: 0.018867924528301886
- predicate-with-synsem-features: 0.009433962264150943
- coherence-with-synsem-features: 0.10377358490566038

## Wed 10 May

iSRL: testing the idea of resuming training on iSRL data. 
I implemented one experiment with the multi-way
model. Running now:

```
nohup python3 -u exp-coherence-with-semeval-data.py 3 &
```

## Fri 12 May

(iSRL) Ran some statistics (`compare_semeval_and_ontonotes.py`). The coverage
is not bad so it's not that OntoNotes can't help here.

```
Frame with DNI: Unique=80, count=118
Overlap: 389
+OntoNotes, -SemEval: 5201
-OntoNotes, +SemEval(train+test): 143
-OntoNotes, +SemEval(test): 89 (count=1674)
-OntoNotes, +SemEval(test,DNI): 13 (count=16)
```


## Sat 16 Sep

Trying to reproduce iSRL for LREC. Now I need to re-install everything. Luckily
I still have enough scripts.

Found [a very weird commit](https://bitbucket.org/cltl/isrl-sp/commits/4fbce8279c8185c87868d61a3abf73575bf19705) in April that breaks everything:

``` 
4fbce8279c8185c87868d61a3abf73575bf19705
switched to separate weights for different roles
```

I guess it was close to a deadline, I panicked (just as I'm now) 
and try to get some results.

OK, I get it now. The idea is to fix the position of roles (i.e. the filler of
role A0 always take up the first place, A1 second, etc.). Roles that are not
found in the text is filled with a dummy value. See the code 
[here](https://bitbucket.org/cltl/isrl-sp/commits/4fbce8279c8185c87868d61a3abf73575bf19705#Lextract_ontonotes_frames.pyT79)


Rerun preparation scripts though I might need to run them again when I manage
to change the model back to normal. 

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ cat ./prepare.sh
python3 -u convert-ontonotes-dependency.py
python3 -u extract_ontonotes_frames.py
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup ./prepare.sh &
[1] 7933
```

I should have done this long ago but probably I was too bogged down to the 
details that I forgot. Aggregation function (to summarize f_j), the use of 
non-pronoun coreferent expressions, activation function, dropout are also important 
hyperparameters that I really need to tune. I tried only cube without dropout
and was so sad that it doesn't work. But I should have run all of them and
picked the best.

So now I'll do just that:
1. max + cube
2. max + tanh
3. max + sigmoid
4. max + cube + dropout(0.5)
5. max + tanh + dropout(0.5)
6. max + sigmoid + dropout(0.5)
7. sum + tanh + dropout(0.5)
8. max + tanh + dropout(0.5) + coref
9. max + sigmoid + dropout(0.5) + coref
10. sum + tanh + dropout(0.5) + coref

OK, started on HPC Cloud:

```
ssh ubuntu@145.100.58.213
nohup pair_models/exp_hyperparameters.sh &
tail -f out/exp_hyperparameters-01.out
tail -f out/exp_hyperparameters-02.out
```

Now I'll just wait...

When comparing my model with Filip's, I realize a potential problem: I was applying
softmax on the set of candidates instead of the whole vocabulary. And each
set of candidates has only about 100 elements (?) i.e. many words in the 
vocabulary is not updated.

## Mon 18 Sep

Experiments finish already. At least that means my code is still good.
However, the results on OntoNotes is still kind of the same. So basically 
the method is already as good as it could be.

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-01.out | sort | tail -n 1
    Valid accuracy: 0.472405
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-02.out | sort | tail -n 1
    Valid accuracy: 0.466671
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-03.out | sort | tail -n 1
    Valid accuracy: 0.427514
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-04.out | sort | tail -n 1
    Valid accuracy: 0.459254
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-05.out | sort | tail -n 1
    Valid accuracy: 0.439481
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-06.out | sort | tail -n 1
    Valid accuracy: 0.382530
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-07.out | sort | tail -n 1
    Valid accuracy: 0.440930
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-08.out | sort | tail -n 1
    Valid accuracy: 0.459862
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-09.out | sort | tail -n 1
    Valid accuracy: 0.409439
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep "Valid accuracy" out/exp_hyperparameters-10.out | sort | tail -n 1
    Valid accuracy: 0.454174
```

Now I really need to do ON5V. Otherwise it makes no sense to publish anything.

Found footnote 12 in Moor et al. (2013). It's like my master's study all over
again. Perhaps the poor results I got before were due to this footnote (did I 
really get any results on ON5V? I can only vaguely remember).

## Thu 21 Sep

Fixed some bugs, run ON5V evaluation using my own code. Got terrible result (8%).

Before going home, I started another experiment:

```
nohup python3 -m pair_models.exp_coherence_with_synsem_features 927 2>&1 > out/coherence-with-synsem-on5v.txt &
```

Found 3 instances of "A1" in OntoNotes (A1 Tank and A1 Grand Prix) but they 
ain't head of any filler. Anyway, I should have separate roles and tokens
in the vocabulary. I tried to do it but it's way too complicated now.

```
Minhs-MacBook-Pro:annotations cumeo$ pwd
/Users/cumeo/ulm4/isrl-sp/data/ontonotes-release-5.0/data/files/data/english/annotations
Minhs-MacBook-Pro:annotations cumeo$ python3 count_roles.py
Examined 13108 files
Found 0 instances of role A0
./bn/cnn/02/cnn_0254.parse
./bn/cnn/02/cnn_0266.parse
./wb/sel/27/sel_2752.parse
Examined 13108 files
Found 3 instances of role A1
Examined 13108 files
Found 0 instances of role A2
Examined 13108 files
Found 0 instances of role A3
Examined 13108 files
Found 0 instances of role A4
```

## Mon 25 Sep 2017

Implemented a new experiment: use selectional preferences as a feature for
Feizabadi and Pado's model. Ran on HPC, revision 5aabd7d29ce6eb12283cdfe3f95edd2a7c92d3d0

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup ./try_coherence_as_feature.sh &
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ cat nohup.out
Output dir: out/2017-09-25-5aabd7d/9272
Log is written to: out/2017-09-25-5aabd7d/9272/exp_coherence_model.py.out
Model: out/2017-09-25-5aabd7d/9272/coherence-model.pkl
Reading data... Done.
Train: 577595, valid: 64178
Building model...
Reading embeddings...
 1000  2000  3000  4000  5000  6000  7000  8000  9000 10000
```

I want to experiment with the SP model being a candidate filter too but that'll
have to wait until tomorrow.

## Tue 26 Sep 2017

New results of coherence+F&P at:

- `out/2017-09-26-ff72233/292751/`
- `out/2017-09-26-ff72233/2965292/`
- `out/2017-09-26-ff72233/9272/`

2 other attempts failed because learning rate was too big.


Started to implement ON5V preprocessing.

## Wed 27 Sep 2017

Process ON5V into Tiger XML format and do cross-validation. The preprocessing
takes quite some time so I run it on server:

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup python3 preprocess_on5v.py 2>&1 > preprocess_on5v.py.out &
```

Evaluated F&P on ON5V (features were computed on SemEval). Got embarrassing results:

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ tail out/2017-09-27-working/exp_dni_on5v.py.out
[0.2, 0.2222222222222222, 0.0, 0.058823529411764705, 0.024242424242424242, 0.1, 0.0, 0.0, 0.05555555555555555, 0.0]
[0.15555555555555556, 0.13333333333333333, 0.0, 0.043478260869565216, 0.012121212121212121, 0.041666666666666664, 0.0, 0.0, 0.04, 0.0]
[0.175, 0.16666666666666666, 0, 0.05, 0.01616161616161616, 0.058823529411764705, 0, 0, 0.046511627906976744, 0]
```
(first row: P, second: R, last: F1, 10-fold cross validation)

Mean F1 = 5.1%

## Thu 28 Sep 2017

Got even more embarrassing results with coherence+F&P. It doesn't make sense...

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ tail out/2017-09-28-working/9272/exp_coherence_model_as_feature_for_feizabadi_pado.py.out
[0, 0.3333333333333333, 0, 0.0, 0.0, 0, 0, 0, 0.0, 0.0]
[0.0, 0.06666666666666667, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
[0, 0.1111111111111111, 0, 0, 0, 0, 0, 0, 0, 0]
```

Found out why: all positive examples get low selectional preference scores: 

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep Yes out/2017-09-28-working/9272/exp_coherence_model_as_feature_for_feizabadi_pado.py.out
"A0,A1,A2",noun.Tops,__NULL__,NN,NP,0,False,False,False,__NULL__,q2,Yes
"A0,A1,A2",noun.Tops,__NULL__,PRP,PRP,0,True,False,False,__NULL__,q1,Yes
"A0,A1,A2,A3",noun.Tops,__NULL__,NNS,NP,0,False,False,False,__NULL__,q1,Yes
"A0,A1,A2",noun.Tops,__NULL__,NN,NP,1,False,False,False,__NULL__,q1,Yes
"A0,A1,A2,A3",noun.Tops,__NULL__,NNS,NP-LGS,0,False,False,False,__NULL__,q3,Yes
"A0,A1,A2",noun.Tops,__NULL__,NNS,NP-SBJ,2,False,True,True,__NULL__,q1,Yes
```

More precisely, 486/549 examples get <0.2:

```
In [12]: lines = open('out/2017-09-28-working/9272/exp_coherence_model_as_feature_for_feizabadi_pado.py.out').readlines()

In [13]: lines_yes = [l for l in lines if 'Yes' in l]

In [14]: import csv

In [15]: from collections import Counter

In [16]: c = Counter()

In [17]: for row in csv.reader(lines_yes):
   ....:         c[row[10]] += 1
   ....:

In [18]: c
Out[18]: Counter({'q1': 486, 'q2': 45, 'q4': 9, 'q3': 9})
```

I think the problem is simply there're too many candidates and the model wasn't
good enough to have a sharp distinction between them. Maybe it's better to use
relative ranks. Let's try that.

Switching to ranks reveals a clear signal:

```
In [1]: lines = open('out/2017-09-28-working/9272/exp_coherence_model_as_feature_for_feizabadi_pado.py.out').readlines()

In [2]: lines_yes = [l for l in lines if 'Yes' in l]

In [3]: import csv

In [4]: from collections import Counter

In [5]: c = Counter()

In [6]: for row in csv.reader(lines_yes):
   ...:     c[row[10]] += 1
   ...:

In [7]: c
Out[7]: Counter({'top50': 15, 'top5': 12, 'top20': 11, '50+': 8, 'top10': 5})

In [8]: lines_no = [l for l in lines if ',No' in l]

In [9]: c2 = Counter()

In [10]: for row in csv.reader(lines_no):
    c2[row[10]] += 1
   ....:

In [11]: c2
Out[11]: Counter({'50+': 959552, 'top50': 337500, 'top20': 113816, 'top10': 57854, 'top5': 52677})

In [12]: sum(c2.values())
Out[12]: 1521399

In [14]: [(k, v/1521399) for k,v in c2.items()]
Out[14]:
[('top50', 0.22183529764381335),
 ('top20', 0.07481009255297262),
 ('top10', 0.03802684239965979),
 ('top5', 0.03462405325624639),
 ('50+', 0.6307037141473079)]
```

Holy shit, I still get a lot of zeros:

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ grep '\[0' out/2017-09-28-working/9272/exp_coherence_model_as_feature_for_feizabadi_pado.py.out
[0, 0.3333333333333333, 0, 0.0, 0, 0.0, 0, 0, 0.3333333333333333, 0.0]
[0.0, 0.06666666666666667, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.04, 0.0]
[0, 0.1111111111111111, 0, 0, 0, 0, 0, 0, 0.07142857142857142, 0]
```

why???

meeting with Antske: 
- coreference resolution --> finding out sets of errors where it can solve
e.g. inanimate things that after linking, does animate actions, events that
aren't supposed to go with each other (what???)

## Fri 29 Sep 2017

Trying to do an error analysis for ON5V. Why does my system get such bad results?
Is it because the frames are particularly hard?

To do an error analysis, I need a development set (taken from OntoNotes) but
I already used everything for training. So now I have to train everything
again >"<

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup python3 -m pair_models.extract_on5v_frames &
...
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup python3 -m pair_models.exp_coherence_model 92285 &
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ tail -f nohup.out
Model: out/2017-09-29-working/92285/coherence-model.pkl
Reading data... Done.
Train: 570304, valid: 63368
...
```

Another problem is that F&P features were based on statistics of SemEval data.
I need to compute new statistics against OntoNotes.

It doesn't make sense because there're so many unique values of frequency
in OntoNotes. I'll need to take logarithm of it.

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ cut -f 2 data/lemma-frequency-semeval.txt | sort -g | uniq | wc -l
86
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ cut -f 2 preprocessed_data/lemma_frequency_ontonotes.v1.txt | sort -g | uniq | wc -l
987
```

Done counting lemmas. Started computing role frequencies:

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup python3 compute_role_percentage_ontonotes.py > compute_role_percentage_ontonotes.py.out 2>&1 &
```

Retraining of coherence model is also running:

```
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ nohup python3 -m pair_models.extract_ontonotes_frames &
ubuntu@ip-145-100-58-213:/mnt/isrl-sp$ tail -f nohup.out
...
Epoch 890:
    Train NLL: 1.414794
    Train accuracy: 0.565400
    Valid NLL: 1.753261
    Valid accuracy: 0.464272
    Example:
        Predicate: be.01
        Role: A2
        Predicted filler: worse - worse (wrong)
Best model saved into out/2017-09-29-working/92285/coherence-model.pkl
```

TODO (iSRL): make a simple baseline using crude selectional preference rules
(e.g. A0 is always a person, A1, A2 cannot be certain things). make a slightly
more complicated model using frequencies measured on OntoNotes. what is the
overlap of verbs between OntoNotes and SemEval?

TODO: (iSRL) recency weighting as in Do et al. 2017
