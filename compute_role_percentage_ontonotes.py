import os
from collections import Counter, defaultdict
import ontonotes
from ontonotes import ontonotes_en, ontonotes2tiger, OntonotesDocument
from data import Document
from isrl import version
import codecs

if __name__ == '__main__':
    out_path = 'out/role-percentage-ontonotes.%s.txt' %version
    
    c = Counter()
    c2 = defaultdict(Counter)
    doc_count = 0
    for base_path in ontonotes.list_docs(ontonotes_en):
        if os.path.relpath(base_path, ontonotes_en) in ontonotes.dev_docs:
            print('Ignored one document because it is in dev set')
            continue
        onto_doc = OntonotesDocument(base_path)
        xml_doc, _ = ontonotes2tiger(onto_doc)
        doc = Document(xml_doc)
        for t in doc.elem.iterfind('.//t'):
            c[t.get('lemma')] += 1
        for fe in doc.elem.iterfind('.//fe'):
            if fe.find('fenode') is not None:
                role = fe.get('name')
                filler_id = fe.find('fenode').get('idref')
                if filler_id in doc.id2head:
                    head = doc.id2elem[doc.id2head[filler_id]].get('lemma')
                    c2[head][role] += 1
        doc_count += 1
#         if doc_count >= 10: break # for debugging
    print('Read %d documents' %doc_count)
    with codecs.open(out_path, 'w', 'utf-8') as f:
        for lemma in sorted(c2):
            total_freq = c[lemma]
            for role in c2[lemma]:
                f.write('%s\t%s\t%f\n' %(lemma, role, c2[lemma][role]/float(total_freq)))
    print('Written results to %s' %out_path)