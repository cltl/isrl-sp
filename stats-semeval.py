from isrl import fn_semeval
from data import Document
import codecs
from ontonotes import ontonotes_en
import os
import re
import sys

if __name__ == '__main__':
    sys.stdout.write('Reading CLMET... ')
    clmet_words = set()
    with codecs.open('out/clmet.preprocessed.txt', 'r', 'utf-8') as f:
        for line in f:
            for word in line.strip().split():
                clmet_words.add(word.lower())
    sys.stdout.write('Done.\n')
    
    sys.stdout.write('Reading OntoNotes... ')
    ontonotes_words = set()
    for root, _, fnames in os.walk(ontonotes_en):
        for fname in fnames:
            if fname.endswith('.dep'):
                with codecs.open(os.path.join(root, fname), 'r', 'utf-8') as f:
                    for line in f:
                        if line.strip() != '':
                            fields = re.split(r'\s+', line.strip())
                            ontonotes_words.add(fields[1].lower())
    sys.stdout.write('Done.\n')
    
    print('Training set:')
    num_found_in_clmet = 0
    num_found_in_ontonotes = 0
    num_total = 0
    num_dni_found_in_clmet = 0
    num_dni_found_in_ontonotes = 0
    num_dni_total = 0
    doc = Document(fn_semeval.train_path)
    for frame_elem in doc.frames:
        if frame_elem.get('name') != 'Coreference':
            pred_id = frame_elem.find('target/fenode').get('idref')
#             print(doc.id2words[pred_id], frame_elem.get('id'))
            flag = frame_elem.find('.//flag[@name="Definite_Interpretation"]')
            has_dni = flag is not None
            pred, = doc.id2words[pred_id]
            if pred.lower() in clmet_words:
                num_found_in_clmet += 1
                if has_dni:
                    num_dni_found_in_clmet += 1
            if pred.lower() in ontonotes_words:
                num_found_in_ontonotes += 1
                if has_dni:
                    num_dni_found_in_ontonotes += 1
            if has_dni:
                num_dni_total += 1
            num_total += 1
    print('\tFrames found in CLMET: %d/%d = %.2f%%' %(num_found_in_clmet, num_total, 
                                         num_found_in_clmet*100.0/num_total))
    print('\tFrames found in OntoNotes: %d/%d = %.2f%%' %(num_found_in_ontonotes, num_total, 
                                             num_found_in_ontonotes*100.0/num_total))
    print('\tFrames with DNI found in CLMET: %d/%d = %.2f%%' 
          %(num_dni_found_in_clmet, num_dni_total, 
            num_dni_found_in_clmet*100.0/num_dni_total))
    print('\tFrames with DNI found in OntoNotes: %d/%d = %.2f%%' 
          %(num_dni_found_in_ontonotes, num_dni_total, 
            num_dni_found_in_ontonotes*100.0/num_dni_total))
     