import sys

print('no-name-context')
for line in sys.stdin:
    if line.strip():
        fields = line.strip().split('\t')
        lemma = fields[3]
        pos = fields[4].lower()[0]
        id_ = "s%s_%s" %(fields[0], fields[1])
        sys.stdout.write('%s#%s#%s#1 ' %(lemma, pos, id_))