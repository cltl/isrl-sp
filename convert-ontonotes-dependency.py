import sys
import re
import os
from ontonotes import ontonotes_en

if __name__ == '__main__':
    count = 0
    for root, _, fnames in os.walk(ontonotes_en):
        for fname in fnames:
            if re.search(r'\.parse$', fname): 
                inp_path = os.path.join(root, fname)  
                out_path = os.path.join(root, re.sub(r'\.parse', '.dep', fname))
                print(inp_path)
                exit_code = os.system(
                        "java -cp stanford-parser-full-2013-11-12/stanford-parser.jar "
                        "edu.stanford.nlp.trees.EnglishGrammaticalStructure -basic -conllx "
                        "-treeFile %s > %s" %(inp_path, out_path))
                assert exit_code == 0, "error while converting"
                count += 1
                if count % 10 == 0:
                    sys.stdout.write('%d files so far...\n' %count)
    print("Finished converting %d files." %count)