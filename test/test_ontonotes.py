from ontonotes import OntonotesDocument
import ptb
import conll
import re
import ontonotes

def _get_tokens(tree, pointer):
    return [' '.join(t[0] for t in terminals) 
            for terminals in OntonotesDocument._resolve_pointer(tree, pointer)] 

def test_resolve_pointer():
    tree = ptb._tree_from_string('(S (NP-1 (NN John) (VP (VB wants) (S (NP (-NONE- *-1)) (VP (TO to) (V swim))))))')
    assert _get_tokens(tree, '2:1') == ['*-1']
    assert _get_tokens(tree, '0:2') == ['John wants *-1 to swim']
    assert _get_tokens(tree, "2:0*0:0") == ['*-1', 'John']
    tree = ptb._tree_from_string('(S (NP (NN John)) (VP (VB keeps) (PRT on) (NP ...)))')
    assert _get_tokens(tree, "1:0,2:0") == ['keeps on']

def test_token_ids():
    tree = ptb._tree_from_string('(S (NP-1 (NN John) (VP (VB wants) (S (NP (-NONE- *-1)) (VP (TO to) (V swim))))))')
    ids = [t.token_id for t in OntonotesDocument._resolve_pointer(tree, '0:2')[0]]
    assert ids == [0, 1, None, 2, 3]
    
sample_dep = '''1    We    _    PRP    PRP    _    3    nsubj    _    _
2    respectfully    _    RB    RB    _    3    advmod    _    _
3    invite    _    VB    VB    _    0    root    _    _
4    you    _    PRP    PRP    _    3    dobj    _    _
5    to    _    TO    TO    _    6    aux    _    _
6    watch    _    VB    VB    _    3    xcomp    _    _
7    a    _    DT    DT    _    9    det    _    _
8    special    _    JJ    JJ    _    9    amod    _    _
9    edition    _    NN    NN    _    6    dobj    _    _
10    of    _    IN    IN    _    9    prep    _    _
11    Across    _    NNP    NNP    _    12    nn    _    _
12    China    _    NNP    NNP    _    10    pobj    _    _
13    .    _    .    .    _    3    punct    _    _'''
sample_dep = re.sub(' {3,}', '\t', sample_dep).split('\n')

sample_dep2 = '''1    In    _    IN    IN    _    17    prep    _    _
2    the    _    DT    DT    _    3    det    _    _
3    summer    _    NN    NN    _    1    pobj    _    _
4    of    _    IN    IN    _    3    prep    _    _
5    2005    _    CD    CD    _    4    pobj    _    _
6    ,    _    ,    ,    _    17    punct    _    _
7    a    _    DT    DT    _    8    det    _    _
8    picture    _    NN    NN    _    17    nsubj    _    _
9    that    _    WDT    WDT    _    14    dobj    _    _
10    people    _    NNS    NNS    _    14    nsubj    _    _
11    have    _    VBP    VBP    _    14    aux    _    _
12    long    _    RB    RB    _    14    advmod    _    _
13    been    _    VBN    VBN    _    14    aux    _    _
14    looking    _    VBG    VBG    _    8    rcmod    _    _
15    forward    _    RB    RB    _    14    advmod    _    _
16    to    _    IN    IN    _    15    prep    _    _
17    started    _    VBD    VBD    _    0    root    _    _
18    emerging    _    VBG    VBG    _    17    xcomp    _    _
19    with    _    IN    IN    _    18    prep    _    _
20    frequency    _    NN    NN    _    19    pobj    _    _
21    in    _    IN    IN    _    18    prep    _    _
22    various    _    JJ    JJ    _    26    amod    _    _
23    major    _    JJ    JJ    _    26    amod    _    _
24    Hong    _    NNP    NNP    _    25    nn    _    _
25    Kong    _    NNP    NNP    _    26    nn    _    _
26    media    _    NNS    NNS    _    21    pobj    _    _
27    .    _    .    .    _    17    punct    _    _'''
sample_dep2 = re.sub(' {3,}', '\t', sample_dep2).split('\n')

sample_dep3 = '''1    They    _    PRP    PRP    _    2    nsubj    _    _
2    wanted    _    VBD    VBD    _    0    root    _    _
3    to    _    TO    TO    _    4    aux    _    _
4    have    _    VB    VB    _    2    xcomp    _    _
5    ,    _    ,    ,    _    4    punct    _    _
6    overseas    _    JJ    JJ    _    7    amod    _    _
7    compatriots    _    NNS    NNS    _    8    nsubj    _    _
8    get    _    VB    VB    _    4    ccomp    _    _
9    together    _    RB    RB    _    8    advmod    _    _
10    so    _    IN    IN    _    14    mark    _    _
11    that    _    IN    IN    _    14    mark    _    _
12    they    _    PRP    PRP    _    14    nsubj    _    _
13    could    _    MD    MD    _    14    aux    _    _
14    support    _    VB    VB    _    2    advcl    _    _
15    one    _    NN    NN    _    14    dobj    _    _
16    another    _    DT    DT    _    15    dep    _    _
17    in    _    IN    IN    _    14    prep    _    _
18    wiping    _    VBG    VBG    _    17    pcomp    _    _
19    out    _    RP    RP    _    18    prt    _    _
20    the    _    DT    DT    _    23    det    _    _
21    Eighth    _    NNP    NNP    _    22    nn    _    _
22    Route    _    NNP    NNP    _    23    nn    _    _
23    Army    _    NNP    NNP    _    18    dobj    _    _
24    and    _    CC    CC    _    18    cc    _    _
25    foiling    _    VBG    VBG    _    18    conj    _    _
26    our    _    PRP$    PRP$    _    27    poss    _    _
27    movement    _    NN    NN    _    25    dobj    _    _
28    to    _    TO    TO    _    29    aux    _    _
29    destroy    _    VB    VB    _    27    infmod    _    _
30    roads    _    NNS    NNS    _    29    dobj    _    _
31    .    _    .    .    _    2    punct    _    _'''
sample_dep3 = re.sub(' {3,}', '\t', sample_dep3).split('\n')

def test_dep_tree():
    tree = conll._parse_dep_tree(sample_dep)
    assert tree[5]['tokens'] == set([4, 5, 6, 7, 8, 9, 10, 11])
    tree = conll._parse_dep_tree(sample_dep2)
    assert tree[0]['tokens'] == set([0, 1, 2, 3, 4])
    tree = conll._parse_dep_tree(sample_dep3)
    assert tree[14]['tokens'] == set([14, 15])
    
coref_str = '''<COREF ID="0" TYPE="IDENT">A much better looking News Night</COREF> <COREF ID="m_17" TYPE="IDENT" SPEAKER="speaker1">I</COREF> might add *?* as <COREF ID="m_12" TYPE="IDENT" SPEAKER="Paula_Zahn">Paula Zahn</COREF> sits in for <COREF ID="3" TYPE="IDENT"><COREF ID="30" TYPE="IDENT">Anderson</COREF> and <COREF ID="31" TYPE="IDENT">Aaron</COREF></COREF> /.
<COREF ID="3" TYPE="IDENT">They</COREF> 're both off /-
*PRO* Look at <COREF ID="m_12" TYPE="IDENT" SPEAKER="Paula_Zahn">that</COREF> /.'''

def test_coref():
    chains = ontonotes.load_coref(coref_str)
    assert len(chains['0']) == 1
    assert len(chains['m_12']) == 2
    
def test_coref_in_doc():
    doc = OntonotesDocument('test/small')
    assert doc.coref()[(0, 5)] == ((0, 5), (8, 3), (22, 0))