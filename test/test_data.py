from data import Document

def test_document():    
    doc = Document('data/sample.xml')
    assert len(doc.sentences) == 3
    assert len(doc.frames) == 20
    assert doc.id2sent['s1_f1229595850.89254'] == 0
    assert doc.id2sent['s1_f1229595850.89254_e2'] == 0
    assert doc.id2sent['s2_f3'] == 1
    assert doc.id2sent['s3_1'] == 2
    assert doc.id2elem['s2'].get('id') == 's2'
    assert doc.id2elem['s2_504'].get('id') == 's2_504'
    assert ' '.join(doc.id2words['s2_503']) == 'a slate-coloured sky .'
    assert doc.id2head['s2_503'] == 's2_18'
    assert doc.id2head['s2_16'] == 's2_16'
    assert doc.coref['s2_6'] == set(('s2_6', 's1_11'))
    assert doc.coref['s1_11'] == set(('s2_6', 's1_11'))
    assert doc.id2ovrs['s2_12'] == set(('s2_f1229595850.94803_e1', 's2_f1_e1', 's2_f3_e2'))