from classifier import NaiveBayes, LogisticRegression
from data import Indexer

# features: Name, Over_170CM, Eye, Hair_length 
x = [['Drew', 'No', 'Blue', 'Short',],
     ['Claudia', 'Yes', 'Brown', 'Long',],
     ['Drew', 'No', 'Blue', 'Long',],
     ['Drew', 'No', 'Blue', 'Long',],
     ['Alberto', 'Yes', 'Brown', 'Short',],
     ['Karin', 'No', 'Blue', 'Long',],
     ['Nina', 'Yes', 'Brown', 'Short',],
     ['Sergio', 'Yes', 'Blue', 'Long',]]
# prediction: Sex
y = ['Male', 'Female', 'Female', 'Female',
     'Male', 'Female', 'Female', 'Male',]
# test data
x_test = [x[0], x[1], x[2], 
          ['Drew', 'Yes', 'Blue', 'Long'],
          ['NULL', 'NULL', 'NULL', 'NULL',]]

def test_naive_bayes():
    nb = NaiveBayes(4)
    nb.fit(x, y)
    print(nb.predict(x_test))
    
def test_logistic():
    input_indexer = Indexer()
    x2 = [ [input_indexer.index(val) for val in row] for row in x ]
    input_indexer.seal(True)
    x_test2 = [ [input_indexer.index(val) for val in row] for row in x_test ]
    output_indexer = Indexer()
    y2 = [ output_indexer.index(val) for val in y ]
    output_indexer.seal(False)
    
    ld = LogisticRegression(len(input_indexer), len(output_indexer), drop_prob=0.5)
    ld.fit(x2, y2)
    print([output_indexer.token(i) for i in ld.predict(x_test2)])
    ld = LogisticRegression(len(input_indexer), len(output_indexer), drop_prob=0)
    ld.fit(x2, y2)
    print([output_indexer.token(i) for i in ld.predict(x_test2)])
    