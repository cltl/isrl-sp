from conll import find_tokens

tree = [{'token': 'And', 'head': 3, 'label': 'cc'},
        {'token': 'we', 'head': 3, 'label': 'nsubj'},
        {'token': "'re", 'head': 3, 'label': 'aux'},
        {'token': 'going', 'head': -1, 'label': 'root'},
        {'token': 'to', 'head': 6, 'label': 'aux'},
        {'token': 'get', 'head': 6, 'label': 'auxpass'},
        {'token': 'started', 'head': 3, 'label': 'xcomp'},
        {'token': 'here', 'head': 6, 'label': 'advmod'},
        {'token': '.', 'head': 3, 'label': 'punct'},
        ]

def test_find_tokens():
#     print(find_tokens(3, tree))
    assert sorted(find_tokens(3, tree)) == list(range(9))
    assert sorted(find_tokens(1, tree)) == [1]