from util import dice

def test_dice_simple():
    assert dice(set([1,2,3]), set([2,4])) == 0.4
    assert dice(set([1,2,3]), set([4,5])) == 0.0
    
if __name__ == '__main__':
    test_dice_simple()