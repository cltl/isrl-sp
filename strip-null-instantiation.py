import sys
import re

s = sys.stdin.read()
s = re.sub(r';[\w\d]+_[DI]NI=\([s\d_,]+\)', '', s)
s = re.sub(r'[\w\d]+_[DI]NI=\([s\d_,]+\);?', '', s)
sys.stdout.write(s)