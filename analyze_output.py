import glob
import os
from data import Document, Pbparser
from isrl import pb_semeval, version
import pandas as pd
from collections import defaultdict
import codecs
from _collections import OrderedDict

output_dir = 'out/2018-01-05-working/'
predicate_only_pattern = output_dir + '39724/predicate-only-*.txt'
coherence_pattern = output_dir + '43980/predicate-with-synsem-features-*.txt'
output_path_patterns = [predicate_only_pattern, coherence_pattern]
#                         output_dir + '80860/synsem-*.txt',
#                         output_dir + '39312/coherence-model-*.txt']
#                         output_dir + '12441/coherence-with-synsem-features-*.txt'

def write_html(path, doc, highlights):
    ''' Write the document in a human-readable way, highlighting tokens specified
    by <code>highlights</code>.
    
    highlights: a list of highlights, each one is a tuple (name, tokens) 
                in which:
                - name: the name of the element (e.g. a frame element or a predicate) 
                - tokens (list): the tokens (id of <t> elements) to be highlighted.
    '''
    with codecs.open(path, 'w', 'utf-8') as f:
        tokens = OrderedDict()
        for s in doc.elem.iterfind('.//s'):
            for t in s.iterfind('.//t'):
                tokens[t.get('id')] = t.get('word') 
            tokens[s.get('id')] = '<br>'
        notes = defaultdict(list)
        for fe_id, highlighted_tokens in highlights:
            for t_id in highlighted_tokens:
                notes[t_id].append(fe_id)
        for t_id, note in notes.items():
            tokens[t_id] = ('<span style="color:red; font-weight: bold;" title="%s">%s</span>' 
                            %(' '.join(note), tokens[t_id]))
        for token in tokens.values():
            f.write(token)
            f.write(' ')

def doc_key(path):
    bn = os.path.basename(path)
    if '13' in bn: return '13'
    elif '14' in bn: return '14'
    else: raise ValueError('Unknown key for path: %s' %path)

if __name__ == '__main__':
    output_paths = dict((pattern, glob.glob(pattern)) for pattern in output_path_patterns)
    assert all(len(paths) == 2 for paths in output_paths.values())
    output_docs = dict((pattern, dict((doc_key(p), Document(p, Pbparser())) for p in paths))
                       for pattern, paths in output_paths.items())
    gold_docs = dict((doc_key(p), Document(os.path.join(pb_semeval.gold_path, p), Pbparser()))
                     for p in pb_semeval.test2gold.values())
#     print(gold_docs)
    
    columns = ['doc_key', 'fe_id', '#coref_heads_gold', '#predicate_get_correct', '#cohrerence_get_correct']
    data = []
    for doc_key, gold_doc in gold_docs.items():
        highlights = defaultdict(list)
        for frame_elem in gold_doc.frames:
            predicate_id = frame_elem.get('id')
            predicate_idref = frame_elem.find('target').find('fenode').get('idref')
            for fe_elem in frame_elem.iterfind('fe'):
                if (fe_elem.find("flag[@name='Definite_Interpretation']") is not None and
                    fe_elem.find('fenode[@idref]') is not None):
                    fe_id = fe_elem.get('id')
                    idref = fe_elem.find('fenode').get('idref')
                    if idref in gold_doc.coref:
                        coref_idrefs = gold_doc.coref[idref]
                    else: 
                        coref_idrefs = (idref,)
                    gold_heads = [gold_doc.id2head.get(idref) for idref in coref_idrefs]
#                     print(gold_heads)

                    sys_get_correct = {}                    
                    sys_tokens = {}
                    for pattern, key2sys_doc in output_docs.items():
                        sys_doc = key2sys_doc[doc_key]
                        sys_fe_elem = sys_doc.elem.find(".//fe[@id='%s']" %fe_id)
                        sys_fenode_elem = None if sys_fe_elem is None else sys_fe_elem.find('fenode')
                        sys_idref = None if sys_fenode_elem is None else sys_fenode_elem.get('idref') 
                        sys_tokens[pattern] = sys_doc.collect_tokens(sys_idref)
#                         print(sys_tokens)
                        sys_get_correct[pattern] = any(head in sys_tokens[pattern] for head in gold_heads)
#                     print(truepos_count)

                    new_highlights = [(predicate_id, [predicate_idref]),
                                      ('coherence: ' + fe_id, sys_tokens[coherence_pattern]),
                                      ('predicate_only: ' + fe_id, sys_tokens[predicate_only_pattern]),]
                    if sys_get_correct[coherence_pattern] and not sys_get_correct[predicate_only_pattern]:
                        highlights[coherence_pattern].extend(new_highlights)
                    elif sys_get_correct[predicate_only_pattern] and not sys_get_correct[coherence_pattern]:
                        highlights[predicate_only_pattern].extend(new_highlights)

                    data.append([doc_key, fe_id, len(gold_heads),
                                 int(sys_get_correct[predicate_only_pattern]),
                                 int(sys_get_correct[coherence_pattern])])
        write_html('out/compare-one-and-multiway.%s.coherence-is-correct.%s.html' %(doc_key, version), 
                   gold_doc, highlights[coherence_pattern])
        write_html('out/compare-one-and-multiway.%s.predicate-is-correct.%s.html' %(doc_key, version), 
                   gold_doc, highlights[predicate_only_pattern])
    df = pd.DataFrame(data, columns=columns)
    df.to_csv('out/compare-one-and-multiway.%s.csv' %version)
    