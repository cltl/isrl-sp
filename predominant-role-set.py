'''
Extract predominant role sets from Ontonotes
'''

import os
import re
from collections import Counter, defaultdict
import sys
from ontonotes import ontonotes_en

nombank_root = 'data/nombank.1.0'
nombank_path = os.path.join(nombank_root, 'nombank.1.0')
assert os.path.exists(nombank_root), 'please link/put nombank into data directory'

def iter_ontonotes():
    for root, _, fnames in os.walk(ontonotes_en):
        for fname in fnames:
            if re.search(r'\.prop$', fname):
                yield os.path.join(root, fname)

assert len(sys.argv) == 2, "Usage: predominant-role-set.py <nombank|ontonotes>"
switch = sys.argv[1]
assert switch in ('ontonotes', 'nombank'), 'Expected the first argument to be either "nombank" or "ontonotes".'
paths = iter_ontonotes() if switch == 'ontonotes' else [nombank_path]
path_count = 0
counters = defaultdict(Counter)

for path in paths:
    path_count += 1
    with open(path) as inp:
        for line in inp:
            fields = line.strip().split(' ')
            if switch == 'ontonotes':
                pos = fields[4].split('-')[-1]  
                frame = fields[5]
                roles = fields[8:] 
            else:
                pos = 'n'
                frame = fields[3] + "." + fields[4]
                roles = fields[5:]
#             if frame == 'accounting.01': print(roles)
            roles = (f for f in roles if '-rel' not in f.lower() and '-support' not in f.lower())
            roles = ('-'.join(f.split('-')[1:]) for f in roles) # remove pointers
            roles = (f for f in roles if 'LINK-' not in f) # remove links in Ontonotes
            roles = (re.sub(r'(A\d+)-\w+', r'\1', # remove -PRD and the like after A0, A1,...  
                            re.sub('-H\d+', '',  # remove hyphen tags
                                   f.replace('ARG', 'A'))) for f in roles)
            roles = ','.join(sorted(roles)) # represent a set by a string
            counters[frame][roles] += 1
                
sys.stderr.write("Read %d files\n" %path_count)
for k in sorted(counters):
    c = sorted(counters[k].items(), key=lambda x:(-x[1], len(x[0])))[0]
    print('\t'.join((k, c[0] or '_', str(c[1]))))