public class PBLib
{
	static final String BLANK     = "_";
	
	static final String PREDICATE = "predicate";
	static final String NOUN      = "noun";
	static final String LEMMA     = "lemma";
	static final String ARGMAP    = "argmap";
	static final String ROLESET   = "roleset";
	static final String ROLE      = "role";
	static final String FN_FRAME  = "fn-frame";
	static final String FN_ROLE   = "fn-role";
	static final String PB_ARG    = "pb-arg";
	static final String NB_ROLE   = "nb-role";
}
