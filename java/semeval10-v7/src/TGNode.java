import java.util.ArrayList;
import java.util.Arrays;

public class TGNode
{
	public String word;
	public String lemma;
	public String pos;
	public String phraseInfo;
	public String phraseHead;
	public ArrayList<String> rolesetIDs;
	public ArrayList<String> inArgs;
	public ArrayList<String> exArgs;
//	private ArrayList<String> arrArgs;
	
	public TGNode(String word, String lemma, String pos)
	{
		this.word  = word;
		this.lemma = lemma;
		this.pos   = pos;
		phraseInfo = "*";
		phraseHead = "*";
		rolesetIDs = new ArrayList<String>();
		inArgs     = new ArrayList<String>();
		exArgs     = new ArrayList<String>();
	//	arrArgs    = new ArrayList<String>();
	}
	
	public boolean isPosx(String posx)
	{
		return (pos.length() >= posx.length() && (pos.substring(0,posx.length()).equals(posx)));	
	}
	
	public boolean isNoun()
	{
		return isPosx("NN");
	}
	
	public boolean isVerb()
	{
		return isPosx("VB");
	}
	
	public boolean isPredicate()
	{
		return rolesetIDs.size() > 0;
	}
	
	public boolean isPunc()
	{
		return isPosx("PUNC");
	}
	
/*	public void addArg(String label)
	{
		arrArgs.add(label);
	}
	
	public void setArg(String label)
	{
		arrArgs.set(arrArgs.size()-1, label);
	}*/
	
	public String toString()
	{
		String in = "", ex = "";
		
		for (int i=0; i<rolesetIDs.size(); i++)
		{
			String rolesetId = rolesetIDs.get(i);
			String inArg     = inArgs.get(i);
			String exArg     = exArgs.get(i);
			
			String[] inA = inArg.split(TGLib.ARG_DELIM);	Arrays.sort(inA);
			String[] exA = exArg.split(TGLib.ARG_DELIM);	Arrays.sort(exA);
			
			inArg = (inA.length > 0) ? inA[0] : "";
			exArg = (exA.length > 0) ? exA[0] : "";
			
			for (int j=1; j<inA.length; j++)	inArg += TGLib.ARG_DELIM + inA[j];
			for (int j=1; j<exA.length; j++)	exArg += TGLib.ARG_DELIM + exA[j];
			
		//	if (!inArg.equals(""))	in += TGLib.ARG_DELIM + rolesetId+"{"+inArg+"}";
		//	if (!exArg.equals(""))	ex += TGLib.ARG_DELIM + rolesetId+"{"+exArg+"}";
			in += TGLib.ARG_DELIM + rolesetId+"{"+inArg+"}";
			ex += TGLib.ARG_DELIM + rolesetId+"{"+exArg+"}";
		}
		
		in = (in.equals("")) ? PBLib.BLANK : in.substring(1);
		ex = (ex.equals("")) ? PBLib.BLANK : ex.substring(1);
		
		String str = word + TGLib.FILED_DELIM + lemma + TGLib.FILED_DELIM + pos + TGLib.FILED_DELIM + 
		             phraseInfo + TGLib.FILED_DELIM + phraseHead + TGLib.FILED_DELIM + in + TGLib.FILED_DELIM + ex;
	//	for (String arg : arrArgs)	str += TGLib.FILED_DELIM + arg;
		
		return str;
	}
}
