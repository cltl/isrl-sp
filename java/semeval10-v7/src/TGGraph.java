import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TGGraph
{
	private String sen_id;
	private ArrayList<TGNode>                    a_terminal;
	private Hashtable<String, ArrayList<String>> h_nonterminal;
	
	/**
	 * @param eGraph <graph>...</graph>
	 * @param senId sentence ID
	 */
	public TGGraph(Element eGraph, String senId)
	{
		this.sen_id = senId;
		
		Element eTerminals = (Element)eGraph.getElementsByTagName(TGLib.TERMINALS).item(0);
		setTerminals(eTerminals);
		
		Element eNonTerminals = (Element)eGraph.getElementsByTagName(TGLib.NON_TERMINALS).item(0);
		setNonTerminals(eNonTerminals);
		
		String rootID = eGraph.getAttribute(TGLib.ROOT);
		setPhrase(rootID);	
	}
	
	/** @param eTerminals <terminals>...</terminals> */
	private void setTerminals(Element eTerminals)
	{
		NodeList lsT = eTerminals.getElementsByTagName(TGLib.T);
		a_terminal   = new ArrayList<TGNode>(lsT.getLength());
		
		System.out.println("------------");
		for (int i=0; i<lsT.getLength(); i++)
		{
			Element eT    = (Element)lsT.item(i);
			int     index = idrefToIndex(eT.getAttribute(TGLib.ID));
//			System.out.println(index);
			String  word  = eT.getAttribute(TGLib.WORD);
			String  lemma = eT.getAttribute(TGLib.LEMMA);
			String  pos   = eT.getAttribute(TGLib.POS);
			TGNode  node  = new TGNode(word, lemma, pos);
			
			a_terminal.add(index, node);
		}
		System.out.println("------------");
	}

	/** @param eNonTerminals <nonterminals>...</nonterminals> */
	private void setNonTerminals(Element eNonTerminals)
	{
		NodeList lsNT = eNonTerminals.getElementsByTagName(TGLib.NT);
		h_nonterminal = new Hashtable<String, ArrayList<String>>();
		
		System.out.println("------------");
		for (int i=0; i<lsNT.getLength(); i++)
		{
			Element eNT    = (Element)lsNT.item(i);
			String  id     = eNT.getAttribute(TGLib.ID);
//			System.out.println(id);
			String  cat    = eNT.getAttribute(TGLib.CAT);
			String  head   = eNT.getAttribute(TGLib.HEAD);
			
			NodeList         lsEdge = eNT.getElementsByTagName(TGLib.EDGE);
			ArrayList<String> aEdge = new ArrayList<String>(lsEdge.getLength()+1);
			aEdge.add(cat);		// first element is reserved for pos-tag
			aEdge.add(head);	// first element is reserved for head-lemma
			
			for (int j=0; j<lsEdge.getLength(); j++)
			{
				Element eEdge = (Element)lsEdge.item(j);
				String  idref = eEdge.getAttribute(TGLib.ID_REF);
				aEdge.add(idref);
			}
			
			h_nonterminal.put(id, aEdge);
		}
		System.out.println("------------");
	}
	
	/** @param parentID idref of parent node of this phrase s*/
	private void setPhrase(String parentID)
	{
		ArrayList<String> phraseInfo = h_nonterminal.get(parentID);
		String phrase = phraseInfo.get(0);	// pos-tag
		String head   = phraseInfo.get(1);	// head-id
		
		ArrayList<Integer> children = new ArrayList<Integer>();
		getTterminalIndices(parentID, children);
		Collections.sort(children);
		
		boolean isFound = false;
		for (int cId : children)
		{
			if (a_terminal.get(cId).word.equals(head))
			{
				head = phrase + TGLib.HEAD_DELIM + Integer.toString(cId + 1);
				isFound = true;
				break;
			}
		}
		
		if (!isFound)
		{
			System.err.println("- Warning: head-word not found, senId="+sen_id+", head-wrod=\""+head+"\"");
			head = phrase;
		}
		
		TGNode firstChild = a_terminal.get(children.get(0));
		if (firstChild.phraseInfo.equals("*"))
		{
			firstChild.phraseInfo = "("+phrase;
			firstChild.phraseHead = "("+head;
		}
		else
		{
			firstChild.phraseInfo += "("+phrase;
			firstChild.phraseHead += "("+head;
		}
		
		TGNode lastChild = a_terminal.get(children.get(children.size()-1));
		lastChild.phraseInfo += ")";
		lastChild.phraseHead += ")";
		
		for (int i=2; i<phraseInfo.size(); i++)
		{
			String childID = phraseInfo.get(i);
			if (!isTerminal(childID))	setPhrase(childID);
		}
	}
	
	/**
	 * @param idref id-reference
	 * @return true if <code>idref</code> is terminal
	 */
	public boolean isTerminal(String idref)
	{
		int index = idrefToIndex(idref);
		return 0 <= index && index < a_terminal.size();
	}
	
	/**
	 * 
	 * @param idref id-reference
	 * @param arr PRE : empty<br>
	 *            POST: list of all terminal indices under <code>idref</code>
	 */
	public void getTterminalIndices(String idref, ArrayList<Integer> arr)
	{
		if (!h_nonterminal.containsKey(idref))
			arr.add(idrefToIndex(idref));
		else
		{
			ArrayList<String> children = h_nonterminal.get(idref);
			for (int i=2; i< children.size(); i++)
				getTterminalIndices(children.get(i), arr);
		}
	}
	
	/**
	 * @param idref id-reference (e.g., "s1_0")
	 * @return index of <code>idref</code> (e.g., 0)
	 */
	private int idrefToIndex(String idref)
	{
		int firstUS = idref.indexOf('_');
		int lastUS  = idref.lastIndexOf('_');
		
		if (firstUS != lastUS)
		{
			System.err.println("- Warning: word splitted, senId="+sen_id+", idref="+idref);
			return Integer.parseInt(idref.substring(firstUS+1, lastUS));
		}
		
		return Integer.parseInt(idref.substring(firstUS+1));
	}
	
	/**
	 * @param idref terminal id-reference
	 * @return terminal node with respect to <code>idref</code>
	 */
	public TGNode getTerminalNode(String idref)
	{
		System.out.println(idref);
		return a_terminal.get(idrefToIndex(idref));
	}
	
	public boolean isSenId(String senId)
	{
		return sen_id.equals(senId);
	}
	
	public String toString()
	{
		int  senId = Integer.parseInt(sen_id.substring(1));
		String tmp = "";
		
		for (int i=0; i<a_terminal.size(); i++)
			tmp += senId + TGLib.FILED_DELIM + (i+1) + TGLib.FILED_DELIM + a_terminal.get(i) + "\n";
		
		return tmp.trim();
	}
	
/*	public void finalizeArgs()
	{
		for (TGNode node : a_terminal)
		{
			if (!node.isPredicate())	continue;
			
			String[] arrArg = node.args.split(" ");
			boolean isFirst = true;
			
			for (String arg : arrArg)
			{
				String[] argIdx = arg.split(":");
				String   label  = argIdx[0];
				ArrayList<Integer> arr = new ArrayList<Integer>();
				
				for (int i=1; i<argIdx.length; i++)
					arr.add(Integer.parseInt(argIdx[i]));
				Collections.sort(arr);
				
				for (int i=0; i<arr.size(); i++)
				{
					int index = arr.get(i);
					if (index < a_terminal.size() && a_terminal.get(index).isPunc())
						arr.remove(i);
					else
						break;
				}
				
				for (int i=arr.size()-1; i>0; i--)
				{
					int index = arr.get(i);
					if (index < a_terminal.size() && a_terminal.get(index).isPunc())
						arr.remove(i);
					else
						break;
				}
				
				if (isFirst)
				{
					for (int i=0; i<a_terminal.size(); i++)
					{
						if (arr.contains(i))	a_terminal.get(i).addArg(label);
						else					a_terminal.get(i).addArg(PBLib.BLANK);
					}
					
					isFirst = false;
				}
				else
				{
					for (int i=0; i<a_terminal.size(); i++)
					{
						if (arr.contains(i))	a_terminal.get(i).setArg(label);
					}
				}
			}
		}
	}*/
}
