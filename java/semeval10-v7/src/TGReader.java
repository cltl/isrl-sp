import java.io.File;
import java.io.PrintStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TGReader
{
	public TGReader() { }
	
	private void read(String tigreFile, String outFile, String pbFile, String nbFile, int max) throws Exception
	{
		DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
		System.out.println("Intialize FramNet-ProBbank mapping");
		PBReader pbXml = new PBReader(pbFile, "pb");
		System.out.println();
		
		System.out.println("Intialize FramNet-Nombank mapping");
		PBReader nbXml = new PBReader(nbFile, "nb");
		System.out.println();
		
		DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
		Document        doc      = dBuilder.parse(new File(tigreFile));
		Element         eBody    = (Element)doc.getElementsByTagName(TGLib.BODY).item(0);
		NodeList        lsS      = eBody.getElementsByTagName(TGLib.S);
		PrintStream     fout     = new PrintStream(new FileOutputStream(outFile));
		
		System.out.println("Convert Tiger to Semeval format");
		ArrayList<TGGraph> dGraph = new ArrayList<TGGraph>(max);
		
		for (int i=0; i<lsS.getLength(); i++)
		{
			Element eS    = (Element)lsS.item(i);
			String  senId = eS.getAttribute(TGLib.ID);
			
			NodeList lsGraph = eS.getElementsByTagName(TGLib.GRAPH);
			if (lsGraph.getLength() < 1)
			{
				System.err.println("- Error  : <graph> does not exist, senId="+senId);
				continue;
			}
			
			Element eGraph = (Element)lsGraph.item(0);
			TGGraph graph  = new TGGraph(eGraph, senId);
			if (dGraph.size() == max)	dGraph.remove(0);
			dGraph.add(graph);
			
			NodeList lsFrames = eS.getElementsByTagName(TGLib.FRAMES);
			if (lsFrames.getLength() > 0)
			{
				Element eFrames = (Element)lsFrames.item(0);
				new TGFrames(eFrames, dGraph, pbXml, nbXml, senId);
			}
			
		//	graph.finalizeArgs();
			fout.println(graph+"\n");
		}
	}

	static public void main(String[] args) throws Exception
	{
		new TGReader().read(args[0], args[1], args[2], args[3], Integer.parseInt(args[4]));
	}
}
