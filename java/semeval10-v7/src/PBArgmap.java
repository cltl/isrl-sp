import java.util.Hashtable;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PBArgmap
{
	public  String                    roleset;
	private Hashtable<String, String> h_role; 
	
	public PBArgmap(Element eArgmap, String pnb)
	{
		String PNB_ARG = (pnb.equals("pb")) ? PBLib.PB_ARG : PBLib.NB_ROLE;
		roleset        = eArgmap.getAttribute(pnb+"-"+PBLib.ROLESET).trim();
		h_role         = new Hashtable<String, String>();
		
		NodeList lsRole =  eArgmap.getElementsByTagName(PBLib.ROLE);
		for (int i=0; i<lsRole.getLength(); i++)
		{
			Element eRole = (Element)lsRole.item(i);
			String fnRole = eRole.getAttribute(PBLib.FN_ROLE).trim();
			String pbArg  = eRole.getAttribute(PNB_ARG).trim();
			
			if (h_role.containsKey(fnRole))
				System.err.println("- Warning: duplicated fn-role, roleset="+roleset+", fn-role="+fnRole);
			else
				h_role.put(fnRole, pbArg);
		}
	}
	
	public String getPBArg(String fnRole)
	{
		return h_role.containsKey(fnRole) ? "A" + h_role.get(fnRole) : null;
	}
}
