public class TGLib
{
	static final String FILED_DELIM   = "\t";
	static final String ARG_DELIM     = ";";
	static final String IDX_DELIM     = ",";
	static final String HEAD_DELIM    = ":";
	
	static final String BODY          = "body";
	static final String S             = "s";
	static final String GRAPH         = "graph";
	static final String FRAMES        = "frames";
	static final String ROOT          = "root";
	
	static final String TERMINALS     = "terminals";
	static final String T             = "t";
	static final String ID            = "id";
	static final String WORD          = "word";
	static final String LEMMA         = "lemma";
	static final String POS           = "pos";
	
	static final String NON_TERMINALS = "nonterminals";
	static final String NT            = "nt";
	static final String CAT           = "cat";
	static final String HEAD          = "head";
	static final String EDGE          = "edge";
	static final String ID_REF        = "idref";
	
	static final String FRAME         = "frame";
	static final String NAME          = "name";
	static final String TARGET        = "target";
	static final String FE            = "fe";
	static final String FE_NODE       = "fenode";
	
	static final String COREF         = "Coreference";
	static final String FLAG          = "flag";
	static final String DNI           = "Definite_Interpretation";
	static final String INI           = "Indefinite_Interpretation";
	static final String COREFERENT    = "Coreferent";
	static final String CURRENT       = "Current";
}
