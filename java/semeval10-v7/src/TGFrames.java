import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class TGFrames
{
	private ArrayList<TGGraph> d_graph;
	private PBReader pb_xml;
	private PBReader nb_xml;
	private String   sen_id;
	
	private Set<String> auxiliaryFrames = new HashSet<>();
	
	{
		auxiliaryFrames.add("Coreference");
		auxiliaryFrames.add("Support");
		auxiliaryFrames.add("Relativization");
	}

	
	/** @param eFrames <frames>...</fromes> */
	public TGFrames(Element eFrames, ArrayList<TGGraph> dGraph, PBReader pbXml, PBReader nbXml, String senId)
	{
		d_graph = dGraph;
		pb_xml  = pbXml;
		nb_xml  = nbXml;
		sen_id  = senId;
		
		NodeList lsFrame = eFrames.getElementsByTagName(TGLib.FRAME);
		for (int i=0; i<lsFrame.getLength(); i++)
		{
			Element eFrame     = (Element)lsFrame.item(i);
			String  frameName  = eFrame.getAttribute(TGLib.NAME).trim();
			TGNode  targetNode;
			try {
				targetNode = getTargetNode(eFrame);
			} catch (Exception e) {
				System.out.println("Ignore 1 frame: " + eFrame.getAttribute("id"));
				continue;
			}
			
			PBArgmap argmap = getArgmap(frameName, targetNode);
			if (argmap == null && !frameName.equals(TGLib.COREF))	continue;
			
			String rolesetId = (argmap != null) ? argmap.roleset : "coref.01";
			
			NodeList lsFe = eFrame.getElementsByTagName(TGLib.FE);
			String inArgs = "", exArgs = "";
			
			for (int j=0; j<lsFe.getLength(); j++)
			{
				Element  eFe = (Element)lsFe.item(j);
				String[] arg = getFePBArgs(eFe, argmap);
				
				if (!arg[0].equals(PBLib.BLANK))	inArgs += TGLib.ARG_DELIM + arg[0];
				if (!arg[1].equals(PBLib.BLANK))	exArgs += TGLib.ARG_DELIM + arg[1];
			}

			// eliminate ARG_DELIM
			if (!inArgs.equals("") && inArgs.charAt(0) == ';')
				inArgs = inArgs.substring(1);
			if (!exArgs.equals("") && exArgs.charAt(0) == ';')
				exArgs = exArgs.substring(1);
			
			targetNode.rolesetIDs.add(rolesetId);
			targetNode.inArgs.add(inArgs);
			targetNode.exArgs.add(exArgs);
		}
	}
	
	/**
	 * @param eFrame <frame>...</frame>
	 * @return target node of the frame
	 */
	private TGNode getTargetNode(Element eFrame)
	{
		Element eTarget     = (Element)eFrame .getElementsByTagName(TGLib.TARGET) .item(0);
		Element eFeNode     = (Element)eTarget.getElementsByTagName(TGLib.FE_NODE).item(0);
		String  targetIdref = eFeNode.getAttribute(TGLib.ID_REF).trim();
		
		return d_graph.get(d_graph.size()-1).getTerminalNode(targetIdref);
	}
	
	/**
	 * @param frameName name of the frame
	 * @param targetNode {@link TGFrames#getTargetNode(Element)}
	 * @return argument mapping with respect to <code>frameName</code> and <code>targetNode</code>
	 */
	private PBArgmap getArgmap(String frameName, TGNode targetNode)
	{
		PBArgmap argmap = null;
		
		if      (targetNode.isNoun())	argmap = nb_xml.getArgmap(targetNode.lemma, frameName);
		else if (targetNode.isVerb())	argmap = pb_xml.getArgmap(targetNode.lemma, frameName);
		
		return argmap;
	}

	/**
	 * @param eFe <fe>...</fe>
	 * @param argmap {@link TGFrames#getArgmap(String, TGNode)}
	 * @return argument label and indices delimited by {@link TGFrames#IDX_DELIM} (e.g., "A1:0:2:3")
	 */
	private String[] getFePBArgs(Element eFe, PBArgmap argmap)
	{
		String[] args = {PBLib.BLANK, PBLib.BLANK};
		String feName = eFe.getAttribute(TGLib.NAME).trim();
		String pbArg;
		if      (feName.equals(TGLib.CURRENT))		pbArg = "A0";
		else if (feName.equals(TGLib.COREFERENT))	pbArg = "A1";
		else										pbArg = argmap.getPBArg(feName);
		if (pbArg == null)	return args;
		
		NodeList lsFeNode = eFe.getElementsByTagName(TGLib.FE_NODE);
		ArrayList<String> aInArgs = new ArrayList<String>();
		ArrayList<String> aExArgs = new ArrayList<String>();
		
		for (int i=0; i<lsFeNode.getLength(); i++)
		{
			Element eFeNode = (Element)lsFeNode.item(i);
			String  idref   = eFeNode.getAttribute(TGLib.ID_REF);
			String  senId   = idref.split("_")[0];
			TGGraph graph   = getGraph(senId);
			
			if (graph == null)
			{
				System.err.println("- Warning: coreference out of range, "+sen_id+" -> "+senId);
				continue;
			}
			
			ArrayList<Integer> arr = new ArrayList<Integer>();
			graph.getTterminalIndices(idref, arr);
			Collections.sort(arr);
			
			if (sen_id.equals(senId))
				for (int index : arr)	aInArgs.add(senId+"_"+(index+1));
			else
				for (int index : arr)	aExArgs.add(senId+"_"+(index+1));
		}
		
		String  indices = "";
	//	boolean isOVE   = aInArgs.size() > 1;
		for (String arg : aInArgs)	indices += TGLib.IDX_DELIM + arg;
		if (aInArgs.size() == 0)	args[0] = PBLib.BLANK;
		else						args[0] = pbArg+getFlag(eFe)+"=("+indices.substring(1)+")";
		
		indices = "";
	//	isOVE   = aExArgs.size() > 1;
		for (String arg : aExArgs)	indices += TGLib.IDX_DELIM + arg;
		if (aExArgs.size() == 0)	args[1] = PBLib.BLANK;
		else						args[1] = pbArg+getFlag(eFe)+"=("+indices.substring(1)+")";
		
		return args;
	}
	
	private TGGraph getGraph(String senId)
	{
		for (int i=d_graph.size()-1; i>=0; i--)
		{
			TGGraph graph = d_graph.get(i);
			if (graph.isSenId(senId))	return graph;
		}
		
		return null;
	}
	
	private String getFlag(Element eFe)
	{
		NodeList lsFlag = eFe.getElementsByTagName(TGLib.FLAG);
		
		for (int i=0; i<lsFlag.getLength(); i++)
		{
			Element eFlag = (Element)lsFlag.item(i);
			String  name  = eFlag.getAttribute(TGLib.NAME);
			if      (name.equals(TGLib.DNI))	return "_DNI";
			else if (name.equals(TGLib.INI))	return "_INI";
		}
		
		return "_OVE";
	}
}
