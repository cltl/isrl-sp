import numpy as np
import pickle
from data import Indexer
import ontonotes
import features
import ptb
from ontonotes import ontonotes_en, OntonotesDocument
import sys
import os

data_dir = os.path.join('preprocessed_data', 'full_models')
os.makedirs(data_dir, exist_ok=True)
index_path = os.path.join(data_dir, 'ontonotes-frames.index')
role_index_path = os.path.join(data_dir, 'ontonotes-roles.index') 
examples_path = os.path.join(data_dir, 'ontonotes-examples.npz')
roles_of_interest = set(['A0', 'A1', 'A2', 'A3', 'A4'])

NUM_CONTEXT_ROLES = 30 # can do with 27
NUM_CANDIDATES = 100

class __cols(object):
    def __init__(self):
        self.CONTEXT_FRAME = 0
        self.CONTEXT_ROLE = 1
        self.CONTEXT_EXPECTED_ROLES = 2
        self.CONTEXT_OTHER_ROLES_START = 3
        self.CONTEXT_OTHER_ROLES_END = self.CONTEXT_OTHER_ROLES_START + NUM_CONTEXT_ROLES * 2

        self.CANDIDATE_TOKEN = 0
        self.CANDIDATE_COREF = 1
        self.CANDIDATE_FEATURES_START = 2
        self.CANDIDATE_FEATURES_END = 6
                
cols = __cols()

def synsem_features_ontonotes(doc, sent, pred, arg, frame, role):
    '''
    A version of features.synsem_features that work with OntoNotes data format.
    This function covers only features of candidates. Expected_roles is
    a feature of the target predicate therefore is encoded 
    '''
    return [# equivalent to CandidateSemanticType
            doc.supersense_of(sent, arg) or '__NULL__', 
            # lemma is not available in OntoNotes so we pretend token is lemma
            features.candidate_lemma_frequency_semeval.from_lemma(doc.deps()[sent][arg]['token']),
            # equivalent to candidate_head_pos 
            doc.deps()[sent][arg]['pos'], 
            # equivalent to candidate_constituent_type
            ptb.find_constituent(doc.deps()[sent][arg]['tokens'], doc.trees()[sent]).label()]

def non_pronoun_coref(doc, sent, head):
    '''
    Return the first non-pronoun coreferring word or, if there's none, return
    the provided word itself. 
    '''
    deps = doc.deps()
    if 'PRP' not in deps[sent][head]['pos']:
        return deps[sent][head]['token']
    chain = doc.coref().get((sent, head))
    if chain:
        for sent2, head2 in chain:
            if 'PRP' not in deps[sent2][head2]['pos']:
                return deps[sent2][head2]['token']
    return deps[sent][head]['token']

if __name__ == '__main__':
    context = []
    candidates = []
    
    doc_count = 0
    frame_count = 0
    indexer = Indexer()
    role_indexer = Indexer()
    role_indexer.index(roles_of_interest)
    
    print('Extracting from %s' %ontonotes_en)
    with open('out/frames.csv', 'w') as f1:
        for base_path in ontonotes.list_docs(ontonotes_en):
            doc = OntonotesDocument(base_path)
            found_examples_in_doc = False
            for f in doc.iter_frames():
                if f.frame_elements:
                    frame = f.predicate                  
                    for i, fe in enumerate(f.frame_elements):
                        coreferring_heads = fe.filler_heads
                        if fe.role in roles_of_interest and len(coreferring_heads) >= 1:
                            # there might be more than one correct candidates (when they are coreferent)
                            for head in coreferring_heads:
                                context_row = []
                                context_row += indexer.index([frame, fe.role,
                                        features.expected_roles(None, None, None, frame, None),])
                                other_roles_start = len(context_row)
                                context_row += [indexer.index('__NULL__')] * (NUM_CONTEXT_ROLES*2)
                                other_roles_ids = (j for j in range(len(f.frame_elements)) if j != i)
                                for j in other_roles_ids:
                                    if len(f.frame_elements[j].filler_heads) > 0:
                                        role_j = f.frame_elements[j].role
                                        other_head = f.frame_elements[j].filler_heads[0] # there's place for only one of them
                                        feature_index = other_roles_start + role_indexer.index(role_j)*2 
                                        context_row[feature_index] = indexer.index(doc.deps()[f.sent][other_head]['token'])
                                        context_row[feature_index+1] = indexer.index(non_pronoun_coref(doc, f.sent, other_head))
                                
                                candidate_row = []
                                head_token = doc.deps()[f.sent][head]['token']
                                coref = non_pronoun_coref(doc, f.sent, head)
                                candidate_row.append(indexer.index([head_token, coref] +
                                                                   synsem_features_ontonotes(doc, f.sent, None, head, frame, fe.role)))       
                                for k, v in enumerate(doc.deps()[f.sent]):
                                    if k not in fe.filler_heads:
                                        coref = non_pronoun_coref(doc, f.sent, k)
                                        candidate_row.append(indexer.index([v['token'], coref] +
                                                             synsem_features_ontonotes(doc, f.sent, None, k, frame, fe.role)))
                                if len(candidate_row) <= NUM_CANDIDATES:
                                    while len(candidate_row) < NUM_CANDIDATES:
                                        candidate_row.append([indexer.index('__NULL__')]*6)
                                    context.append(context_row)
                                    candidates.append(candidate_row)
                                else:
                                    sys.stderr.write('Skipped one example because there were too many candidates.\n')
                                
                            if np.random.rand() < 0.0001:
                                print('-------- example ---------')
                                print('Frame: %s, role: %s' %(f.predicate, fe.role))
                                print('Known explicit roles:')
                                for j, other_fe in enumerate(f.frame_elements):
                                    if j != i and len(other_fe.filler_heads) >= 1:
                                        coreferent_heads = '='.join(doc.deps()[f.sent][h]['token'] 
                                                                    for h in other_fe.filler_heads)
                                        print('\t%s\t%s' %(other_fe.role, coreferent_heads))
                                print('Candidates: ' + ' '.join(('*' if c in fe.filler_heads else '') + v['token'] 
                                                                for c, v in enumerate(doc.deps()[f.sent])))
    #                     else:
    #                         sys.stderr.write('No head was found for role %s, frame %s, sentence %d, file %s'
    #                                          %(role, frame, sent, doc.base_path + ".*\n"))
                            found_examples_in_doc = True
                    frame_count += 1
            if found_examples_in_doc: doc_count += 1
    #         if doc_count >= 5: break # for debugging
    indexer.seal(with_oov=True)
    role_indexer.seal(with_oov=False)
    
    context = np.array(context)
    candidates = np.array(candidates)
    assert(candidates.ndim == 3)
    assert(len(context) == len(candidates))
    
    with open(index_path, 'wb') as f: pickle.dump(indexer, f)
    with open(role_index_path, 'wb') as f: pickle.dump(role_indexer, f)
    np.savez(examples_path, context=context, candidates=candidates)
    
    print("Read %d files" %doc_count)
    print("Found %d frames" %frame_count)
    print("Found %d examples" %len(context))
