import os
import numpy as np
from full_models import model
from full_models.model import model_func
import sys
import math
from data import Document, Pbparser, add_fe_to_file
from lxml import etree
from isrl import find_all_overt, iter_candidates
import isrl
from features import expected_roles, feizabadi_pado_candidate_features
from extract_ontonotes_frames import cols, NUM_CONTEXT_ROLES, index_path
import extract_semeval_frames
import pickle

with open(index_path, 'rb') as f: indexer = pickle.load(f)

def translate_example1(context, candidates):
    missing_features = np.full((candidates.shape[0], candidates.shape[1], 5), 
                               indexer.index('__NULL__'))
    return context, np.dstack((candidates, missing_features))

def translate_example2(context, candidates):
    return context, candidates

class ModelWrapper(object):
    
    def __init__(self, predict_func, indexer, role_indexer):
        self.predict_func = predict_func
        self.indexer = indexer
        self.role_indexer = role_indexer
    
    def __call__(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        frame = frame_elem.get('name')
        context = [frame, role, expected_roles(doc, None, None, frame, None)]
        other_roles_start = len(context)
        context += ['__NULL__'] * (NUM_CONTEXT_ROLES*2)
        for r, g in zip(overt_roles, overt_fillers):
            if r in self.role_indexer.vocab:
                feature_index = other_roles_start + self.role_indexer.index(r)*2 
                context[feature_index] = doc.head_word(g)
                context[feature_index+1] = doc.nonpronoun_coref(g)
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] + 
             feizabadi_pado_candidate_features(doc, frame_elem, arg, frame, role) 
             for arg in candidates]
        return self.predict_func(self.indexer.index([context]),
                                 self.indexer.index([c]))[0]


if __name__ == '__main__':
    isrl.setup_experiment(sys.argv[1])
    model_name = 'coherence-with-semeval-data'
    model_path1 = '%s/%s.ontonotes.npz' %(isrl.out_dir, model_name)
    model_path2 = '%s/%s.npz' %(isrl.out_dir, model_name)

    model.learning_rate = 0.001
    model.train(model_func(), translate_example1, model_path1)
    model.resume_training(model_func(), model_path1, translate_example2, 
                          model_path2, extract_semeval_frames.train_path)
    m = model.load(model_path2, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
#     evaluate_hit_at_n(ModelWrapper, model_path, 5)