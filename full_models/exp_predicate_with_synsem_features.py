import os
import numpy as np
from full_models import model
from full_models.model import model_func
import sys
import math
from data import Document, Pbparser, add_fe_to_file
from lxml import etree
from isrl import find_all_overt, iter_candidates
import isrl
from features import expected_roles, synsem_features_for_candidates
from extract_ontonotes_frames import cols

_empty_matrix = np.matrix([], dtype='int32')

def translate_example(context, candidates):
    return context[:,:cols.CONTEXT_EXPECTED_ROLES+1], candidates

class ModelWrapper(object):
    
    def __init__(self, predict_func, indexer, role_indexer):
        self.predict_func = predict_func
        self.indexer = indexer
        self.role_indexer = role_indexer
    
    def __call__(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        frame = frame_elem.get('name')
        context = [frame, role, expected_roles(doc, None, None, frame, None)]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] + 
             synsem_features_for_candidates(doc, None, arg, frame, role) 
             for arg in candidates]
        return self.predict_func(self.indexer.index([context]),
                                 self.indexer.index([c]))[0]


if __name__ == '__main__':
    isrl.setup_experiment(sys.argv[1])
    model_name = 'predicate-with-synsem-features'
    model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)
    
    model.learning_rate = 0.01
    model.train(model_func(), translate_example, model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
#     evaluate_hit_at_n(ModelWrapper, model_path, 5)