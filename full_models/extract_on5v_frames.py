import os
from extract_ontonotes_frames import index_path, role_index_path,\
    NUM_CONTEXT_ROLES, non_pronoun_coref, synsem_features_ontonotes
from ontonotes import OntonotesDocument, ontonotes_en
from collections import defaultdict
import re
import features
import pickle
import conll
import numpy as np
import sys

on5v_examples_path = 'out/on5v-examples.npz'
on5v_path = 'data/conll_impl_roles_release/all.conll'

assert os.path.exists(index_path), "Remember to run extract_ontonotes_frames.py first"
assert os.path.exists(on5v_path), "ON5V data file not found"

def read_on5v():
    on5v = defaultdict(dict)
    with open(on5v_path) as f:
        for line in f:
            line = line.strip()
            if line:
                doc, loc, word, args = line.split('\t')
                if args != '-':
                    args = re.sub(r'\}$', '', re.sub(r'^\{', '', args))
                    args = args.split('),')
                    new_args = []
                    for arg in args:
                        arg = re.sub(r'^Arg', 'A', arg).split('=')
                        role, toks = arg
                        res = re.findall('[+-]res$', role)
                        res = '' if len(res) == 0 else res[0]
                        role = re.sub('[+-]res$', '', role)
                        if res == '+res':
                            dni_sents = set(re.findall(r's(\d+)_', toks))
                            if len(dni_sents) != 1:
                                sys.stderr.write('WARNING: DNI is not in one sentence (line: %s)\n' %line)
                                continue
#                             assert len(dni_sents) == 1, line
                            dni_sent = int(list(dni_sents)[0])
                            toks = re.sub(r's\d+_', '', 
                                          re.sub(r'^\(', '',
                                          re.sub(r'\)$', '', toks)))
                            toks = toks.split(',')
                            toks = [int(val) for val in toks]
                        else:
                            toks = []
                            dni_sent = -1
                        arg = (role, res, dni_sent, toks)
                        new_args.append(arg)
                    pred_sent, pred_tok = loc[1:].split('_')
                    on5v[doc][(int(pred_sent), int(pred_tok))] = (word, new_args)
    return on5v

def convert(on5v):
    with open(index_path, 'rb') as f: indexer = pickle.load(f)
    with open(role_index_path, 'rb') as f: role_indexer = pickle.load(f)

    context = []
    candidates = []
    max_candidates = 0
    for doc_path in on5v:
        doc = OntonotesDocument(os.path.join(ontonotes_en, doc_path))
        found = 0
        for sent, tok_id, f in doc.iter_frames():
#             print(repr((sent, tok_id)))
#             print(on5v[doc_path].keys())
#             print((sent, tok_id) in on5v[doc_path])
#             print('----')
            if (sent, tok_id) in on5v[doc_path]:
                found += 1

                frame = f[0]
                pred_lemma, args = on5v[doc_path][(sent, tok_id)]
#                 assert pred_lemma in frame, (pred_lemma, frame, doc_path, sent, tok_id)
                if pred_lemma not in frame:
                    sys.stderr.write('WARNING: Frame disagreement between OntoNotes and ON5V %s\n'
                                     %str((pred_lemma, frame, doc_path, sent, tok_id)))
                    continue
                for role, res, dni_sent, dni_toks in args:
                    if res == '+res':
                        context_row = []
                        context_row += indexer.index([frame, role,
                                features.expected_roles(None, None, None, frame, None),])
                        other_roles_start = len(context_row)
                        context_row += [indexer.index('__NULL__')] * (NUM_CONTEXT_ROLES*2)
                        for j in range(1, len(f), 2):
                            if len(f[j+1]) > 0:
                                role_j = f[j]
                                if role_j in role_indexer.vocab:
                                    other_head = f[j+1][0] # there's place for only one of them
                                    feature_index = other_roles_start + role_indexer.index(role_j)*2 
                                    context_row[feature_index] = indexer.index(doc.deps()[sent][other_head]['token'])
                                    context_row[feature_index+1] = indexer.index(non_pronoun_coref(doc, sent, other_head))
                                else:
                                    sys.stderr.write('WARNING: new role: %s\n' %role_j)
                        candidate_row = []
                        dni_head = conll.find_head(dni_toks, doc.deps()[dni_sent])
                        head_token = doc.deps()[dni_sent][dni_head]['token']
                        coref = non_pronoun_coref(doc, dni_sent, dni_head)
                        candidate_row.append(indexer.index([head_token, coref] +
                                                           synsem_features_ontonotes(doc, dni_sent, None, dni_head, frame, role)))       
                        for i in range(max(0, sent-2), sent+1):
                            for k, v in enumerate(doc.deps()[i]):
                                coref = non_pronoun_coref(doc, i, k)
                                candidate_row.append(indexer.index([v['token'], coref] +
                                                     synsem_features_ontonotes(doc, i, None, k, frame, role)))
                        max_candidates = max(max_candidates, len(candidate_row))
                        context.append(context_row)
                        candidates.append(candidate_row)
        if found != len(on5v[doc_path]):
            print('WARNING: Found only %d in %d frame in docuemnt %s'
                 %(found, len(on5v[doc_path]), doc_path))
    for candidate_row in candidates:
        while len(candidate_row) < max_candidates:
            candidate_row.append([indexer.index('__NULL__')]*6)
        
    context = np.array(context)
    candidates = np.array(candidates)
    return context, candidates 
    
if __name__ == '__main__':
    on5v = read_on5v()
    context, candidates = convert(on5v)
    assert(candidates.ndim == 3)
    assert(len(context) == len(candidates))
    
    np.savez(on5v_examples_path, context=context, candidates=candidates)
    print("Found %d examples" %len(context))
        
        
