from data import read_embeddings
import pickle
import sys
import math
from collections import OrderedDict
import theano
from theano import function
from theano import tensor as T
import numpy as np
from full_models.extract_ontonotes_frames import index_path, role_index_path, examples_path

def adagrad(grads, params, learning_rate=1.0, epsilon=1e-6):
    """Adagrad updates (borrow from Lasagne)
    Scale learning rates by dividing with the square root of accumulated
    squared gradients. See [1]_ for further description.
    Parameters
    ----------
    grads : list of gradient expressions
    params : list of shared variables
        The variables to generate update expressions for
    learning_rate : float or symbolic scalar
        The learning rate controlling the size of update steps
    epsilon : float or symbolic scalar
        Small value added for numerical stability
    Returns
    -------
    OrderedDict
        A dictionary mapping each parameter to its update expression
    Notes
    -----
    Using step size eta Adagrad calculates the learning rate for feature i at
    time step t as:
    .. math:: \\eta_{t,i} = \\frac{\\eta}
       {\\sqrt{\\sum^t_{t^\\prime} g^2_{t^\\prime,i}+\\epsilon}} g_{t,i}
    as such the learning rate is monotonically decreasing.
    Epsilon is not included in the typical formula, see [2]_.
    References
    ----------
    .. [1] Duchi, J., Hazan, E., & Singer, Y. (2011):
           Adaptive subgradient methods for online learning and stochastic
           optimization. JMLR, 12:2121-2159.
    .. [2] Chris Dyer:
           Notes on AdaGrad. http://www.ark.cs.cmu.edu/cdyer/adagrad.pdf
    """

    updates = OrderedDict()
    accus = []
    for param, grad in zip(params, grads):
        value = param.get_value(borrow=True)
        accu = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                             broadcastable=param.broadcastable)
        accus.append(accu)
        accu_new = accu + grad ** 2
        updates[accu] = accu_new
        updates[param] = param - (learning_rate * grad /
                                  T.sqrt(accu_new + epsilon))
    return updates, accus

def override_embeddings(E, indexer, word2id, embeddings):
    count = 0
    for i, w in enumerate(indexer.tokens):
        i2 = word2id.get(w)
        if i2 is not None:
            E[i] = embeddings[i2]
            count += 1
    print('Override %d embeddings' %count)
    return E

class Cube(object):

    def __init__(self):
        context = T.matrix('context', dtype='int64')
        candidates = T.tensor3('candidates', dtype='int64')
        W1 = theano.shared(np.empty((0, 0)), name='W1')
        W2 = theano.shared(np.empty((0, 0)), name='W2')
        b1 = theano.shared(np.empty((0)), name='b1')
        E_in = theano.shared(np.empty((0, 0)), name='E_in')
        E_out = theano.shared(np.empty((0, 0)), name='E_out')
        self.params = [W1, b1, W2, E_in, E_out]
        
        context_emb = E_in[context.flatten()].reshape((context.shape[0], -1))
        context_emb.name = 'context_emb'
        hidden = T.power(T.dot(context_emb, W1) + b1, 3)
        hidden.name = 'hidden'
        pre_output = T.dot(hidden, W2)
        pre_output.name = 'pre_output'
        candidates_emb = E_out[candidates.flatten()].reshape((candidates.shape[0], candidates.shape[1], -1))
        candidates_emb.name = 'candidates_emb'
        # both matrices have to be of rank 3. this is required by gpu backend 
        # but not cpu backend (!)
        z = T.batched_dot(pre_output.dimshuffle(0, 'x', 1), 
                          candidates_emb.dimshuffle(0, 2, 1)).dimshuffle(0, 2)
        z.name = 'z'
        o = T.nnet.softmax(z)
        o.name = 'o'
        y_hat = T.argmax(o, axis=1)
        y_hat.name = 'y_hat'
        nll = -T.log(o[:,0]).mean()
        acc = T.eq(y_hat, 0).mean()
        one_grads = T.grad(nll, self.params) 
        
        self.predict = function([context, candidates], y_hat, on_unused_input='ignore')
        self.predict_probs = function([context, candidates], o, on_unused_input='ignore')
        grad_updates = OrderedDict()
        self.grads = [theano.shared(np.empty_like(p.get_value()), name=p.name+'_grad') 
                      for p in self.params]
        for g, og in zip(self.grads, one_grads):
            grad_updates[g] = g+og
        self.accu_grads = function([context, candidates], 
                                   [nll, acc, o], updates=grad_updates,
                                   on_unused_input='ignore')

    def init_params(self, indexer, word2id, embeddings, 
                    num_context_features, num_hidden_unit, num_candidate_features):
        vocab_size = len(indexer.vocab)
        embedding_dim = embeddings.shape[1]
        W1, b1, W2, E_in, E_out = self.params
        W1.set_value(np.random.rand(embedding_dim*num_context_features, 
                                    num_hidden_unit)*0.02-0.01)
        b1.set_value(np.zeros((num_hidden_unit)))
        W2.set_value(np.random.rand(num_hidden_unit, 
                                    embedding_dim*num_candidate_features)*0.02-0.01)
        E_in.set_value(override_embeddings(
            np.random.rand(vocab_size, embedding_dim)*0.02-0.01,
            indexer, word2id, embeddings))
        E_out.set_value(override_embeddings(
            np.random.rand(vocab_size, embedding_dim)*0.02-0.01,
            indexer, word2id, embeddings))
        for p, g in zip(self.params, self.grads):
            g.set_value(np.empty_like(p.get_value()))


model_func = Cube
# max_epochs = 10 # for debugging
max_epochs = 1000
max_resuming_epochs = 20
batch_size = 10000
learning_rate = 0.01
early_stopping = 0

def train(m, example_func, out_params_path, data_path=examples_path):
    '''
    num_filler_features: number of features used to represent a filler
                         (each feature is turned into a vector, all vectors
                         are concatenated to make a single vector representing
                         the filler)
    '''
    sys.stdout.write('Reading data... ')
    with open(index_path, 'rb') as f: indexer = pickle.load(f)
    data = np.load(data_path)
    context = data['context'] 
    candidates = data['candidates']
    context, candidates = example_func(context, candidates)
    sys.stdout.write('Done.\n')

    n = int(context.shape[0]*0.9)
    train_context, train_candidates = context[:n], candidates[:n]
    valid_context, valid_candidates = context[n:], candidates[n:]
    print('Train: %d, valid: %d' %(train_context.shape[0], valid_context.shape[0]))
    print('Building model... ')
    word2id, embeddings = read_embeddings()
    m.init_params(indexer, word2id, embeddings,
                      context.shape[1], 100, candidates.shape[2])
    regularize = function([], updates=OrderedDict((g, g+p*1e-4) for p, g in zip(m.params, m.grads)))
    clear_grads = function([], updates=OrderedDict((g, g*0) for g in m.grads))
    updates, ada_accus = adagrad(m.grads, m.params, learning_rate=learning_rate)
    step = function([], updates=updates)
    print('Building model... Done.\n')
    
    epochs_without_improvement = 0
    best_acc = -1
    for epoch in range(max_epochs):
        rows = np.random.permutation(train_context.shape[0])[:batch_size]
        batch_context, batch_candidates = \
                train_context[rows], train_candidates[rows]
        clear_grads()
        nll, acc, _ = m.accu_grads(batch_context, batch_candidates)
        if math.isnan(nll) or math.isinf(nll): 
            raise ValueError('Bad value is found: ' + str(nll))
        regularize()
        step() # it's important that we call this before monitoring because we reuse accu_grads

        if epoch % 10 == 0 or epoch == max_epochs-1:
            # monitor
            print('Epoch %d:' %epoch)
            print('\tTrain NLL: %f' %nll)
            print('\tTrain accuracy: %f' %acc)
            rows = np.random.permutation(valid_context.shape[0])[:batch_size]
            batch_context, batch_candidates = \
                    valid_context[rows], valid_candidates[rows]
            nll, acc, _ = m.accu_grads(batch_context, batch_candidates)
            print('\tValid NLL: %f' %nll)
            print('\tValid accuracy: %f' %acc)
            if acc > best_acc:
                best_acc = acc
                np.savez(out_params_path, *[p.get_value() for p in m.params])
                np.savez(out_params_path.replace('.npz', '.ada_accus.npz'), 
                         *[a.get_value() for a in ada_accus])   
                print('Best model saved into %s' %out_params_path)
                epochs_without_improvement = 0
            else:
                epochs_without_improvement += 1
                if early_stopping > 0 and epochs_without_improvement > early_stopping: 
                    print("Stopped early because no improvement was observed "
                          "after %d examinations." %early_stopping)
                    break

def resume_training(m, in_params_path, example_func, out_params_path, data_path):
    '''
    num_filler_features: number of features used to represent a filler
                         (each feature is turned into a vector, all vectors
                         are concatenated to make a single vector representing
                         the filler)
    '''
    sys.stdout.write('Reading data... ')
    data = np.load(data_path)
    context, candidates = data['context'], data['candidates'] 
    context, candidates = example_func(context, candidates)
    sys.stdout.write('Done.\n')

    train_context, train_candidates = context, candidates
    print('Examples (all used for training): %d' %(train_context.shape[0]))
    sys.stdout.write('Building model... ')
    regularize = function([], updates=OrderedDict((g, g+p*1e-4) for p, g in zip(m.params, m.grads)))
    clear_grads = function([], updates=OrderedDict((g, g*0) for g in m.grads))
    updates, ada_accus = adagrad(m.grads, m.params, learning_rate=learning_rate)
    step = function([], updates=updates)
    print('Done.\n')
    sys.stdout.write('Loading pretrained model... ')
    param_values = np.load(in_params_path)
    param_values = [param_values[k] for k in sorted(param_values.keys())] # arr_0, arr_1,...
    assert len(m.params) == len(param_values)
    for p, v in zip(m.params, param_values): p.set_value(v)
#     E_out = m.params[-1]
#     E_out_val = E_out.get_value()
#     row_sums = (E_out_val*E_out_val).sum(axis=1) + 1e-16
#     E_out.set_value(E_out_val / row_sums[:, np.newaxis])
    for p, g in zip(m.params, m.grads): g.set_value(np.empty_like(p.get_value()))
    accu_values = np.load(in_params_path.replace('.npz', '.ada_accus.npz'))
    accu_values = [accu_values[k] for k in sorted(accu_values.keys())] # arr_0, arr_1,...
    for a, v in zip(ada_accus, accu_values): a.set_value(v)
    print('Done.\n')
    
    for epoch in range(max_resuming_epochs):
        rows = np.random.permutation(train_context.shape[0])[:batch_size]
        batch_context, batch_candidates = \
                train_context[rows], train_candidates[rows]
        clear_grads()
        nll, acc, _ = m.accu_grads(batch_context, batch_candidates)
        if math.isnan(nll) or math.isinf(nll): 
            raise ValueError('Bad value is found: ' + str(nll))
        regularize()
        step() # it's important that we call this before monitoring because we reuse accu_grads
        
        if epoch % 10 == 0 or epoch >= max_resuming_epochs-1:
            print('Epoch %d:' %epoch)
            print('\tTrain NLL: %f' %nll)
            print('\tTrain accuracy: %f' %acc)
           
    np.savez(out_params_path, *[p.get_value() for p in m.params])
    print('New model saved into %s' %out_params_path)

def test(model, params_path, example_func, data_path=examples_path, my_batch_size=1000):
    param_values = np.load(params_path)
    param_values = [param_values[k] for k in sorted(param_values.keys())] # arr_0, arr_1,...
    assert len(model.params) == len(param_values)
    for p, v in zip(model.params, param_values):
        p.set_value(v)
    for p, g in zip(model.params, model.grads):
        g.set_value(np.empty_like(p.get_value()))

    data = np.load(data_path)
    context = data['context'] 
    candidates = data['candidates']
    context, candidates = example_func(context, candidates)
    total_acc = 0
    for batch_start in range(0, len(context), my_batch_size):
        batch_end = min(batch_start+my_batch_size, len(context))
        batch_context, batch_candidates = \
                context[batch_start:batch_end], candidates[batch_start:batch_end]
        _, acc, _ = model.accu_grads(batch_context, batch_candidates)
        total_acc += acc * (batch_end-batch_start)
    return total_acc / len(context)

def load(model_path, wrapper):
    with open(model_path, 'rb') as f:
        m = pickle.load(f)
    print('Normalizing output embeddings!')
    E_out = m.params[-1]
    E_out_val = E_out.get_value()
    row_sums = (E_out_val*E_out_val).sum(axis=1) + 1e-16
    E_out.set_value(E_out_val / row_sums[:, np.newaxis])    
    for p, g in zip(m.params, m.grads): g.set_value(np.empty_like(p.get_value()))
    with open(index_path, 'rb') as f: indexer = pickle.load(f)
    with open(role_index_path, 'rb') as f: role_indexer = pickle.load(f)
    return wrapper(m.predict, indexer, role_indexer)
