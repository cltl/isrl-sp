import numpy as np
from full_models import model
from full_models.model import model_func
import sys
import math
from lxml import etree
import isrl
from extract_ontonotes_frames import cols, NUM_CONTEXT_ROLES

def translate_example(context, candidates):
    return (context[:,[cols.CONTEXT_FRAME, cols.CONTEXT_ROLE] + 
                           list(range(cols.CONTEXT_OTHER_ROLES_START, 
                                      cols.CONTEXT_OTHER_ROLES_END))], 
            candidates[:,:,:2])

class ModelWrapper(object):
    
    def __init__(self, predict_func, indexer, role_indexer):
        self.predict_func = predict_func
        self.indexer = indexer
        self.role_indexer = role_indexer
    
    def __call__(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        frame = frame_elem.get('name')
        context = [frame, role]
        other_roles_start = len(context)
        context += ['__NULL__'] * (NUM_CONTEXT_ROLES*2)
        for r, g in zip(overt_roles, overt_fillers):
            if r in self.role_indexer.vocab:
                feature_index = other_roles_start + self.role_indexer.index(r)*2 
                context[feature_index] = doc.head_word(g)
                context[feature_index+1] = doc.nonpronoun_coref(g)
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] 
             for arg in candidates]
        return self.predict_func(self.indexer.index([context]),
                                 self.indexer.index([c]))[0]

if __name__ == '__main__':
    isrl.setup_experiment(sys.argv[1])
    model_name = 'coherence-model'
    model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)

    model.learning_rate = 0.005
    model.train(model_func(), translate_example, model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
#     evaluate_hit_at_n(model_path, ModelWrapper, 5)