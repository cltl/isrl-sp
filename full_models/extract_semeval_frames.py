'''
Created on 8 May 2017

@author: Minh Le
'''
from data import Pbparser, Document
from isrl import pb_semeval, find_all_overt, candidate_func
import numpy as np
import os
from extract_ontonotes_frames import index_path, role_index_path,\
    NUM_CONTEXT_ROLES, NUM_CANDIDATES
import pickle
import features
from features import feizabadi_pado_candidate_features

assert os.path.exists(index_path), "Remember to run extract_ontonotes_frames.py first"
train_path = 'out/semeval-frames.train.npz'
test_path = 'out/semeval-frames.test.pkl'

def convert(paths, feature_func=feizabadi_pado_candidate_features, same_candidate_num=False):
    with open(index_path, 'rb') as f: indexer = pickle.load(f)
    with open(role_index_path, 'rb') as f: role_indexer = pickle.load(f)

    context = []
    candidates = []
    if isinstance(paths, str): paths = (paths,)
    for path in paths:
        doc = Document(path, Pbparser())
        for frame_elem in doc.frames:
            overt_roles, overt_fillers = find_all_overt(frame_elem)
            frame = frame_elem.get('name')
            pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
#             pos = pred.get('pos').lower()[0]
            for fe_elem in frame_elem.iterfind('fe'):
                if (fe_elem.find("flag[@name='Definite_Interpretation']") is not None and
                        fe_elem.find('fenode[@idref]') is not None): # found a DNI
                    correct_filler = doc.id2elem[fe_elem.find('fenode').get('idref')]
                    role = fe_elem.get("name")
                    
                    context_row = []
                    context_row += indexer.index([frame, role,
                            features.expected_roles(None, None, None, frame, None),])
                    other_roles_start = len(context_row)
                    context_row += [indexer.index('__NULL__')] * (NUM_CONTEXT_ROLES*2)
                    for r, f in zip(overt_roles, overt_fillers):
                        if r in role_indexer.vocab:
                            feature_index = other_roles_start + role_indexer.index(r)*2 
                            context_row[feature_index] = indexer.index(doc.head_word(f))
                            context_row[feature_index+1] = indexer.index(doc.nonpronoun_coref(f))
                    
                    candidate_row = []
                    candidate_row.append(indexer.index([doc.head_word(correct_filler), 
                                                        doc.nonpronoun_coref(correct_filler)] +
                                                        feature_func(doc, pred, correct_filler, frame, role)))       
                    for arg in candidate_func(doc, frame_elem, overt_fillers):
                        assert type(arg) == type(correct_filler)
                        if arg != correct_filler:
                            candidate_row.append(indexer.index([doc.head_word(arg), 
                                                                doc.nonpronoun_coref(arg)] +
                                                               feature_func(doc, pred, arg, frame, role)))
                    if same_candidate_num:
                        if len(candidate_row) <= NUM_CANDIDATES:
                            while len(candidate_row) < NUM_CANDIDATES:
                                candidate_row.append([indexer.index('__NULL__')]*11)
                        else:
                            candidate_row = candidate_row[:NUM_CANDIDATES]
                    context.append(context_row)
                    candidates.append(candidate_row)
    return context, candidates

if __name__ == '__main__':
    context, candidates = convert(pb_semeval.train_path, same_candidate_num=True)
    context, candidates = np.array(context), np.array(candidates)
    np.savez(train_path, context=context, candidates=candidates)
    context, candidates = convert([os.path.join(pb_semeval.gold_path, name) 
                                   for name in pb_semeval.test2gold.values()], 
                                  same_candidate_num=False)
    with open(test_path, 'wb') as f:
        pickle.dump([context, candidates], f)