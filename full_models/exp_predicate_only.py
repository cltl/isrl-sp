import numpy as np
from full_models import model
from full_models.model import model_func
import isrl
from extract_ontonotes_frames import cols
import sys

def translate_example(context, candidates):
    return context[:,[cols.CONTEXT_FRAME, cols.CONTEXT_ROLE]], candidates[:,:,:2]

class ModelWrapper(object):
    
    def __init__(self, predict_func, indexer, role_indexer):
        self.predict_func = predict_func
        self.indexer = indexer
        self.role_indexer = role_indexer
    
    def __call__(self, doc, frame_elem, pos, role, overt_roles, overt_fillers, candidates):
        frame = frame_elem.get('name')
        context = [frame, role]
        c = [[doc.head_word(arg), doc.nonpronoun_coref(arg)] 
             for arg in candidates]
        return self.predict_func(self.indexer.index([context]),
                                 self.indexer.index([c]))[0]

if __name__ == '__main__':
    isrl.setup_experiment(sys.argv[1])
    model_name = 'predicate-only'
    model_path = '%s/%s.pkl' %(isrl.out_dir, model_name)
    
    model.train(model_func(), translate_example, model_path)
    m = model.load(model_path, ModelWrapper)
    isrl.pb_semeval.resolve_dnis(m, model_name)
