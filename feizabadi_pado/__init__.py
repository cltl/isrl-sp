import sys
from data import Document, Pbparser, is_dni
from features import feizabadi_pado_features_10, FeatureSet
from isrl import predominant_roles, read_predominant_roles, pb_semeval, find_all_overt,\
    iter_candidates
from collections import OrderedDict
import nltk
from util import dice
import numpy as np

candidate_func = iter_candidates
feat_func = feizabadi_pado_features_10

def as_dict(list_):
    return OrderedDict(('key%02d'%i, str(v)) for i, v in enumerate(list_))

def gen_training_examples_ni_resolution(doc, feature_func):
    assert predominant_roles, "Remember to call read_predominant_roles first"
    examples = []
    for frame_elem in doc.frames:
        overt_roles, overt_fillers = find_all_overt(frame_elem)
        frame = frame_elem.get('name')
        pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
        for role in predominant_roles[frame].difference(overt_roles):
            fe_elem = frame_elem.find('fe[@name="%s"]' %role)
            gold_filler = None
            if (fe_elem is not None and is_dni(fe_elem) and
                    fe_elem.find("fenode[@idref]") is not None):
                gold_filler = doc.id2elem[fe_elem.find('fenode').get('idref')]
            for arg in candidate_func(doc, frame_elem, overt_fillers):
                feats = feature_func(doc, pred, arg, frame, role)
                gold_class = ('Yes' if arg == gold_filler else 'No')
                examples.append((as_dict(feats), gold_class))
                if gold_class == 'Yes': 
                    break
    return examples

def gen_training_examples_dni_linking(doc, feature_func):
    examples = []
    for frame_elem in doc.frames:
        flags = list(frame_elem.findall('.//flag[@name="Definite_Interpretation"]'))
        if flags:
            _, overt_fillers = find_all_overt(frame_elem)
            frame = frame_elem.get('name')
            pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
            for flag in flags:
                fe_elem = flag.getparent()
                role = fe_elem.get('name')
                gold_filler = doc.id2elem[fe_elem.find('fenode').get('idref')]
                for arg in candidate_func(doc, frame_elem, overt_fillers):
                    feats = feature_func(doc, pred, arg, frame, role)
                    gold_class = ('Yes' if arg == gold_filler else 'No')
                    examples.append((as_dict(feats), gold_class))
                    if gold_class == 'Yes': 
                        break
                else:
                    reason = ''
                    if doc.id2sent[pred.get('id')] - doc.id2sent[gold_filler.get('id')] not in [0,1,2]:
                        reason = 'because it is not in candidate window'
                    sys.stderr.write('WARN: found no correct filler among candidates %s\n' %reason)
    return examples

def gen_training_examples_dni_linking_soft(doc, feature_func):
    ''' Go through the document, find all the roles that has a DNI and generate
    examples (a series of negative examples followed by a positive one).
    
    This function is more soft (or relaxed) than @gen_training_examples_dni_linking
    in that it allows partially correct examples. The function finds the span
    that maximizes Dice coefficient and makes it the correct example, everything goes before
    it becomes negative examples, everything after is ignored.
    '''
    examples = []
    for frame_elem in doc.frames:
        flags = list(frame_elem.findall('.//flag[@name="Definite_Interpretation"]'))
        if flags:
            _, overt_fillers = find_all_overt(frame_elem)
            frame = frame_elem.get('name')
            pred = doc.id2elem[frame_elem.find('target/fenode').get('idref')]
            for flag in flags:
                fe_elem = flag.getparent()
                role = fe_elem.get('name')
                gold_filler_id = fe_elem.find('fenode').get('idref')
                
                candidates = list(candidate_func(doc, frame_elem, overt_fillers))
                scores = [0 for _ in candidates]
                fillter_tokens = doc.collect_tokens(gold_filler_id)
                cand_tokens = [doc.collect_tokens(c.get('id')) for c in candidates]
                dice_scores = [dice(fillter_tokens, ts) for ts in cand_tokens]
                argmax = np.argmax(dice_scores)
                candidates = candidates[:argmax+1]
                scores = [0 for _ in candidates]
                scores[argmax] = dice_scores[argmax]
                
                for arg, score in zip(candidates, scores):
                    feats = feature_func(doc, pred, arg, frame, role)
                    gold_class = ('Yes' if score > 0 else 'No')
                    examples.append((as_dict(feats), gold_class))
                    if gold_class == 'Yes':
                        break
                else:
                    feats = feature_func(doc, pred, doc.id2elem[gold_filler_id], frame, role)
                    examples.append((as_dict(feats), 'Yes'))
#                     reason = ''
#                     if doc.id2sent[pred.get('id')] - doc.id2sent[gold_filler_id] not in [0,1,2]:
#                         reason = 'because it is not in candidate window'
#                     sys.stderr.write('WARN: found no correct filler among candidates %s\n' %reason)
    return examples
  
class DNIWrapper(object):
    
    def __init__(self, nb, feature_func):
        self.nb = nb
        self.feature_func = feature_func
        
    def __call__(self, doc, pred, pos, role, overt_roles, overt_fillers):
        candidates = list(candidate_func(doc, pred, overt_fillers))
        for i, arg in enumerate(candidates):
            feats = self.feature_func(doc, pred, arg, pred.get('name'), role)
            predicted_y = self.nb.classify(as_dict(feats))
            if predicted_y == 'Yes':
                return candidates[i].get('id')
        return None
    
class NIWrapper(object):
    
    def __init__(self, nb, feature_func):
        self.nb = nb
        self.feature_func = feature_func
    
    def __call__(self, doc, pred, pos, overt_roles, overt_fillers):
        pairs = []
        for role in predominant_roles[pred.get('name')]:
            if role not in overt_roles:
                candidates = list(candidate_func(doc, pred, overt_fillers))
                filler_found = False
                for i, arg in enumerate(candidates):
                    feats = self.feature_func(doc, pred, arg, pred.get('name'), role)
                    predicted_y = self.nb.classify(as_dict(feats))
                    if predicted_y == 'Yes':
                        pairs.append((role, candidates[i].get('id')))
                        filler_found = True
                        break
                if not filler_found:
                    pairs.append((role, 'INI'))
        return pairs
        