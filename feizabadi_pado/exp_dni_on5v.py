from data import Document
from isrl import Scorer, iter_candidates
import isrl
import nltk
from feizabadi_pado import DNIWrapper, gen_training_examples_dni_linking
import preprocess_on5v
import os
from features import feizabadi_pado_features_10_ontonotes

ff = feizabadi_pado_features_10_ontonotes

def run_experiment():
    scorer = Scorer('semeval')
    for i, dir_cv in enumerate(preprocess_on5v.out_dir_cv):
        examples = []
        train_dir = os.path.join(dir_cv, 'train')
        test_dir = os.path.join(dir_cv, 'test')
        out_dir = os.path.join(isrl.out_dir, 'feizabadi_pado_on5v', '%02d' %i)
        print('Training with %s, testing with %s' %(train_dir, test_dir))
        for fname in os.listdir(train_dir):
            if fname.endswith('.xml'):
                path = os.path.join(train_dir, fname)
                train_doc = Document(path)
                examples.extend(gen_training_examples_dni_linking(train_doc, ff))
        nb = nltk.NaiveBayesClassifier.train(examples)
        scorer.evaluate_dni_linking(test_dir, out_dir, DNIWrapper(nb, ff))
    print(scorer.p_vals)
    print(scorer.r_vals)
    print(scorer.f1_vals)

if __name__ == '__main__':
    isrl.setup_experiment(None)

    print("Without Silberer and Frank's filter")
    run_experiment()
    
    print("With Silberer and Frank's filter")
    import feizabadi_pado
    from isrl import silberer_frank_filter_candidates
    feizabadi_pado.candidate_func = silberer_frank_filter_candidates(iter_candidates)
    run_experiment()