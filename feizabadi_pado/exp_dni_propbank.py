from data import Document, Pbparser
from isrl import pb_semeval
import isrl
import nltk
from feizabadi_pado import DNIWrapper, gen_training_examples_dni_linking, feat_func

if __name__ == '__main__':
    isrl.setup_experiment(None)
    train_doc = Document(pb_semeval.train_path, Pbparser())
    examples = gen_training_examples_dni_linking(train_doc, feat_func)
    nb = nltk.NaiveBayesClassifier.train(examples)
    pb_semeval.resolve_dnis(DNIWrapper(nb, feat_func), 'feizabadi-pado')
