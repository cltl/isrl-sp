from data import Document, Pbparser
from features import feizabadi_pado_features_10, FeatureSet
from isrl import read_predominant_roles, pb_semeval
import isrl
import nltk
from feizabadi_pado import NIWrapper, gen_training_examples_dni_linking

if __name__ == '__main__':
    isrl.setup_experiment(None)
    read_predominant_roles()
    train_doc = Document(pb_semeval.train_path, Pbparser())
    feature_funcs = []
    for ff in feizabadi_pado_features_10.feature_funcs:
        feature_funcs.append(ff)
        print('=' * 50)
        print('Features: %s' %','.join(ff.__name__ for ff in feature_funcs))
        print('=' * 50)
        fs = FeatureSet(feature_funcs)
        examples = gen_training_examples_dni_linking(train_doc, fs)
        nb = nltk.NaiveBayesClassifier.train(examples)
        pb_semeval.predict_all(NIWrapper(nb, fs), 'feizabadi-pado')
