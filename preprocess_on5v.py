from on5v import on5v2tiger, on5v_path
import os
import re
import random
import shutil

cross_validation_k = 10
out_dir = os.path.join('preprocessed_data', 'on5v', 'v1.0')
out_dir_all = os.path.join(out_dir, 'all')
out_dir_cv = []
for i in range(cross_validation_k):
    out_dir_cv.append(os.path.join(out_dir, 'cv-%02d' %i))
    
def _copy_paths(paths, to_dir):
    for path in paths:
        shutil.copy(path, to_dir)
        
def _divide_by_document():
    ''' Divide documents into folds for cross validation. In my opinion, this
    is better than dividing the predicates since the quirks of a document
    won't appear in both training and testing.
    '''
    paths = [os.path.join(out_dir_all, fname) 
             for fname in os.listdir(out_dir_all) 
              if re.search(r'\.xml$', fname)]
    random.shuffle(paths)
    parts = [[] for _ in range(cross_validation_k)]
    for i, path in enumerate(paths):
        parts[i%cross_validation_k].append(path)
    for i in range(cross_validation_k):
        train_paths = []
        for j in range(cross_validation_k):
            if j != i:
                train_paths.extend(parts[j])
        test_paths = parts[i]
        
        train_dir = os.path.join(out_dir_cv[i], 'train')
        os.makedirs(train_dir)
        _copy_paths(train_paths, train_dir)
        
        test_dir = os.path.join(out_dir_cv[i], 'test')
        os.makedirs(test_dir)
        _copy_paths(test_paths, test_dir)

def _divide_by_predicate():
    '''Divide the predicates into folds (instead of documents) as described
    in Gerber and Chai (2012), Section 5.1.
    
    Gerber, M. S., & Chai, J. Y. (2012). Semantic role labeling of implicit 
    arguments for nominal predicates. Computational Linguistics, 38(4), 755–798. 
    http://doi.org/10.1162/COLI_a_00110
    '''
    with open(on5v_path) as f:
        predicates = [line for line in f if re.search(r'[+-]res\s*=', line)]
    random.shuffle(predicates) # important!
    parts = [[] for _ in range(cross_validation_k)]
    for i, pred in enumerate(predicates):
        parts[i%cross_validation_k].append(pred)
    for i in range(cross_validation_k):
        train_preds = []
        for j in range(cross_validation_k):
            if j != i:
                train_preds.extend(parts[j])
        test_preds = parts[i]
        
        train_conll_path = os.path.join(out_dir, 'cv_train_%02d.conll' %i)
        with open(train_conll_path, 'w') as f:
            f.writelines(train_preds)
        train_dir = os.path.join(out_dir_cv[i], 'train')
        os.makedirs(train_dir)
        on5v2tiger(train_conll_path, train_dir)
            
        test_conll_path = os.path.join(out_dir, 'cv_test_%02d.conll' %i)
        with open(test_conll_path, 'w') as f:
            f.writelines(test_preds)
        test_dir = os.path.join(out_dir_cv[i], 'test')
        os.makedirs(test_dir)
        on5v2tiger(test_conll_path, test_dir)

if __name__ == '__main__':
    random.seed(259723)
    if os.path.exists(out_dir_all):
        shutil.rmtree(out_dir_all)
    os.makedirs(out_dir_all, exist_ok=True)
    on5v2tiger(on5v_path, out_dir_all)

    for i in range(cross_validation_k):
        if os.path.exists(out_dir_cv[i]):
            shutil.rmtree(out_dir_cv[i])
        os.makedirs(out_dir_cv[i], exist_ok=True)
    _divide_by_predicate()
#     _divide_by_document()