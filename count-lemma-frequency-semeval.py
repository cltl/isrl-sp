from data import train_semeval_pb_path, test_pb_path, test2gold, Document,\
    Pbparser
import os
from collections import Counter

if __name__ == '__main__':
    c = Counter()
    paths = [train_semeval_pb_path] + [os.path.join(test_pb_path, fname) for fname in test2gold]
    for path in paths:
        doc = Document(path, Pbparser())
        for t in doc.elem.iterfind('.//t'):
             c[t.get('lemma')] += 1
             
    for lemma in sorted(c):
        print('%s\t%d' %(lemma, c[lemma]))