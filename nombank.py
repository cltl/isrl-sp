import os
import re
from nltk.tree import ParentedTree
from collections import defaultdict
import sys
import conll
import ptb

nombank_root = 'data/nombank.1.0'
assert os.path.exists(nombank_root), 'please link/put nombank into data directory'
nombank_path = os.path.join(nombank_root, 'nombank.1.0')

specificity = defaultdict(int, [('-NONE-', -5), ('IN', -4), ('RB', -3), 
                                ('PRP$', -2), ('PRP', -1), ('DT', -1),
                                ('VB', 1), ('VBN', 1), ('VBG', 1), ('NN', 1), ('NNS', 1), ('NNP', 1)])

class NombankDocument:
    
    def __init__(self, path=nombank_path):
        self.path = path
        self._constituent_trees = {}
        self._dependency_trees = {}
        
    def constituent_trees(self, fname):
        if fname not in self._constituent_trees:
            self._constituent_trees[fname] = ptb.load_trees(os.path.join(ptb.root_dir, fname))
        return self._constituent_trees[fname]
    
    def dependency_trees(self, fname):
        if fname not in self._dependency_trees:
            fname_dep = re.sub(r'\.mrg$', '.dep', fname)
            self._dependency_trees[fname] = conll.load_trees(os.path.join(ptb.root_dir, fname_dep))
        return self._dependency_trees[fname]
    
    @classmethod
    def _resolve_coordinate(cls, tree, terminal_number, height):
        u = tree.terminals[terminal_number]
        while height > 0: 
            u = u.parent()
            height -= 1
        return u
    
    @classmethod
    def _resolve_pointer(cls, tree, pointer):
        m14 = re.match(r'^\d+:\d+([,;\*]\d+:\d+)*\*?$', pointer)
        if m14: # form 1-4
            chain_parts = re.split('\*|;', pointer) # trace-chain operator has lower precedence
            coreferent_terminals = []
            for cp in chain_parts:
                split_parts = cp.split(',') 
                terminals = []
                for sp in split_parts:
                    m = re.match(r'^(\d+):(\d+)$', sp)
                    terminal_number = int(m.group(1))
                    height = int(m.group(2))
                    terminals.extend(cls._resolve_coordinate(tree, terminal_number, height).terminals)
                coreferent_terminals.append(terminals)
            return coreferent_terminals
        else:
            return None
        
    def iter_frames(self):
        with open(self.path) as inp:
            for line in inp:
                fields = line.strip().split(' ')
                fname = fields[0]
                sent = int(fields[1])
                frame = '%s.%s' %(fields[3], fields[4])
                roles = fields[5:] 
#                 print(frame)
                value = [frame]
                
                ctrees = self.constituent_trees(fname)
                dtrees = self.dependency_trees(fname)
                assert len(dtrees) == len(ctrees), 'constituent and dependency trees mismatch (%s)' %fname
                
                for f in roles:
                    if '-rel' not in f.lower() and '-support' not in f.lower():
                        fields = f.split('-')
                        pointer, rname = fields[0], '-'.join(fields[1:])
                        if 'link-' not in rname.lower(): # ignore links
                            coreferent_terminals = self._resolve_pointer(ctrees[sent], pointer) 
                            if coreferent_terminals is not None:
                                heads = []
                                for terminals in coreferent_terminals:
                                    if not (len(terminals) == 1 and terminals[0].label() == '-NONE-'):
                                        token_ids = set(t.token_id for t in terminals if t.token_id is not None)
                                        head = conll.find_head(token_ids, dtrees[sent])
                                        if head is None:
                                            sys.stderr.write('head not found: %s\n'
                                                             %" ".join(t[0] for t in terminals))  
                                        elif head != '__INGORE__':
                                            heads.append(head)
    #                                                 head_tokens = ' '.join(t[0] for t in terminals)
    #                                                 print('\t', head_token, '\t', rname, '\t', head_tokens)
                                value.extend([rname, heads])
                            else:
                                sys.stderr.write('Ignored pointer: %s\n' %pointer)
                for i in range(1, len(value), 2):
                    value[i] = re.sub(r'(A\d+)-\w+', r'\1', # remove -PRD and the like after A0, A1,...  
                                      re.sub('-H\d+', '',  # remove hyphen tags
                                             value[i].replace('ARG', 'A')))
                yield (fname, sent, value)
                
if __name__ == '__main__':
    doc = NombankDocument()
    for fname, sent, f in doc.iter_frames():
        sys.stdout.write('Sentence #%d, frame: %s, roles: ' %(sent, f[0])) 
        roles = []
        for i in range(1, len(f), 2):
            heads = []
            for j in f[i+1]:
                heads.append(doc._dependency_trees[fname][sent][j]['token'])
            roles.append('%s=%s' %(f[i], '/'.join(heads)))
        print(', '.join(roles))