import sys

for line in sys.stdin:
    if line.strip() == '':
        sys.stdout.write('\n')
    else:
        sys.stdout.write(line.split('\t')[2])
        sys.stdout.write(' ')