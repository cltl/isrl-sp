'''
Created on 11 May 2017

@author: cumeo
'''

import ontonotes
import ptb
from ontonotes import ontonotes_en, OntonotesDocument
from extract_ontonotes_frames import non_pronoun_coref

if __name__ == '__main__':
    for base_path in ontonotes.list_docs(ontonotes_en):
        doc = OntonotesDocument(base_path)
        found_examples_in_doc = False
        for sent, tok_id, f in doc.iter_frames():
            frame = f[0]
            for i in range(1, len(f), 2):
                role = f[i]
                coreferring_heads = f[i+1]
                if role in ('A0', 'A1', 'A2') and coreferring_heads:
                    head_tokens = ','.join(non_pronoun_coref(doc, sent, head) 
                                           for head in coreferring_heads)
                    supersenses = set(doc.supersense_of(sent, head) 
                                      for head in coreferring_heads)
                    supersenses = ','.join(s for s in supersenses if s)
                    supersenses = supersenses or head_tokens.lower()
                    print('%s\t%s\t%s\t%s\t%s\t%d' %(role, supersenses, frame, head_tokens, doc.base_path[-20:], sent))