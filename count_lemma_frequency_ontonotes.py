import os
from collections import Counter
from ontonotes import ontonotes2tiger, ontonotes_en, OntonotesDocument
import ontonotes
from isrl import version

out_path = 'out/lemma_frequency_ontonotes.%s.txt' %version

if __name__ == '__main__':
    c = Counter()
    doc_count = 0
    for base_path in ontonotes.list_docs(ontonotes_en):
        if os.path.relpath(base_path, ontonotes_en) in ontonotes.dev_docs:
            print('Ignored one document because it is in dev set')
            continue
        onto_doc = OntonotesDocument(base_path)
        doc, _ = ontonotes2tiger(onto_doc)
        for t in doc.iterfind('.//t'):
            c[t.get('lemma')] += 1
        doc_count += 1
#         if doc_count >= 10: break # for debugging
    with open(out_path, 'w') as f:
        for lemma in sorted(c):
            f.write('%s\t%d\n' %(lemma, c[lemma]))