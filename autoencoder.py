from ontonotes import ontonotes_en, OntonotesDocument
import codecs
import ontonotes

def write_all_frames(path, resolve_pronoun):
    doc_count = 0
    frame_count = 0
    print('Extracting from %s' %ontonotes_en)
    with codecs.open(path, 'w', 'utf-8') as f1:
        for base_path in ontonotes.list_docs(ontonotes_en):
            doc = OntonotesDocument(base_path)
            found_examples_in_doc = False
            last_sent = -1
            for f in doc.frames:
                if len(f) >= 3:
                    if f.sent != last_sent:
                        f1.write('# %s\n' %' '.join(d['token'] for d in doc.deps()[f.sent]))
                        last_sent = f.sent 
                    f1.write(f.predicate)
                    for fe in f.frame_elements:
                        if resolve_pronoun:
                            s = '='.join(fe.filler_corefs) 
                        else:
                            s = '='.join(fe.filler_strs)
                        if len(fe.filler_heads) > 0:
                            f1.write('\t%s\t%s' %(fe.role, s))
                    f1.write('\n')
                    
                    frame_count += 1
                    found_examples_in_doc = True
            if found_examples_in_doc: doc_count += 1
    print("Read %d files" %doc_count)
    print("Found %d frames" %frame_count)
    print('Frames written to %s' %path)

if __name__ == '__main__':
    write_all_frames('out/frames.tsv', False)
    write_all_frames('out/frames-with-coref.tsv', True)
