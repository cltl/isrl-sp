import sys
import re
from ontonotes import ontonotes_en
import os
import ptb

if __name__ == '__main__':
    count = 0
    for root_dir, fname_ext in ((ptb.root_dir, r'\.mrg$'),
                                #(ontonotes_en, r'\.parse$')
                                ):
        for root, _, fnames in os.walk(root_dir):
            for fname in fnames:
                if re.search(fname_ext, fname): 
                    inp_path = os.path.join(root, fname)  
                    out_path = os.path.join(root, re.sub(fname_ext, '.dep', fname))
                    print(inp_path, ' >> ', out_path)
                    exit_code = os.system(
                            "java -cp stanford-parser-full-2013-11-12/stanford-parser.jar "
                            "edu.stanford.nlp.trees.EnglishGrammaticalStructure -basic -conllx "
                            "-treeFile %s > %s" %(inp_path, out_path))
                    assert exit_code == 0, "error while converting"
                    count += 1
                    if count % 10 == 0:
                        sys.stdout.write('%d files so far...\n' %count)
    #                 if count >= 20: sys.exit(0)
    print("Finished converting %d files." %count)