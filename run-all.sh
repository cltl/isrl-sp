#
# preparation
#

sudo pip3 install nltk
sudo apt-get install -y python3-lxml
#sudo pip3 install lxml
python3 -c "import nltk; nltk.download()" # d > wordnet
sudo apt-get install -y python3-dev
sudo pip3 install Theano

# for Feizabadi and Pado (2015)

python count-lemma-frequency-semeval.py > data/lemma-frequency-semeval.txt
python compute-role-percentage.py > data/role-percentage-semeval.txt
python extract-expected-roles.py > data/expected-roles-nombank-propbank.txt
python predominant-role-set.py nombank > data/predominant-role-set-nombank.tsv
python predominant-role-set.py ontonotes > data/predominant-role-set-ontonotes.tsv

python strip-null-instantiation.py < data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.txt > data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.stripped.txt
python strip-null-instantiation.py < data/SEMEVAL/test_gold/PropBank/13_PBNB.txt > data/SEMEVAL/test_gold/PropBank/13_PBNB.stripped.txt
python strip-null-instantiation.py < data/SEMEVAL/test_gold/PropBank/14_PBNB.txt > data/SEMEVAL/test_gold/PropBank/14_PBNB.stripped.txt

python extract-ukb-context.py < data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.stripped.txt > data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.stripped.ukb.cxt
python extract-ukb-context.py < data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/13_PBNB.stripped.txt > data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/13_PBNB.stripped.ukb.cxt
python extract-ukb-context.py < data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/14_PBNB.stripped.txt > data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/14_PBNB.stripped.ukb.cxt

cd ukb/
bin/ukb_wsd -K wnet30g_rels.bin -D lkb_sources/30/wnet30_dict.txt --ppr data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.ukb.cxt > data/SEMEVAL/Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/correctedTiger.PB.ukb.out
bin/ukb_wsd -K wnet30g_rels.bin -D lkb_sources/30/wnet30_dict.txt --ppr data/SEMEVAL/test_gold/PropBank/13_PBNB.stripped.ukb.cxt > data/SEMEVAL/test_gold/PropBank/13_PBNB.stripped.ukb.out
bin/ukb_wsd -K wnet30g_rels.bin -D lkb_sources/30/wnet30_dict.txt --ppr data/SEMEVAL/test_gold/PropBank/14_PBNB.stripped.ukb.cxt > data/SEMEVAL/test_gold/PropBank/14_PBNB.stripped.ukb.out
cd ..

python convert-ukb-to-supersense.py data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/13_PBNB.stripped.ukb.out > data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/13_PBNB.stripped.supersense
python convert-ukb-to-supersense.py data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/14_PBNB.stripped.ukb.out > data/SEMEVAL/task10_test_data-full+ni_only/ni_only/PropBank/14_PBNB.stripped.supersense

# for my experiments

python preprocess-clmet.py

cd semafor-semantic-parser/release/
./cleanAndCompile.sh
./startMSTServer.sh # you need to manually run this in a separate terminal 
./fnParserDriver.sh "$(dirname ${0})/out/clmet.preprocessed.txt"
cd ../..

# python extract-clmet-frames.py # abandoned
find data/ontonotes-release-5.0/data/files/data/english/annotations -name *.parse | wc -l # answer: 13109
python -u convert-ontonotes-dependency.py > out/convert-ontonotes-dependency.out 2>&1
python -u extract_ontonotes_frames.py > out/extract-ontonotes-frames.out 2>&1

#
# run experiments
#
REV=`git rev-parse HEAD`
mkdir out/$REV
python -u exp-schenk-chiarcos.py > out/$REV/exp-schenk-chiarcos.py.out 2>&1
python -u exp-feizabadi-pado.py 6 > out/$REV/exp-feizabadi-pado.py.out 2>&1
python -u -m pair_models.exp_coherence_model > out/$REV/-m pair_models.exp_coherence_model.out 2>&1
python -u exp-predicate-only.py > out/$REV/exp-predicate-only.py.out 2>&1
python -u exp-predicate-with-synsem-features.py > out/$REV/exp-predicate-with-synsem-features.py.out 2>&1
python -u exp-coherence-with-synsem-features.py > out/$REV/exp-coherence-with-synsem-features.py.out 2>&1