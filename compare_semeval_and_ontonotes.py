'''
Created on 12 May 2017

@author: cumeo
'''
from isrl import pb_semeval
import os
from data import Document, Pbparser
import ontonotes
from ontonotes import ontonotes_en, OntonotesDocument
from collections import Counter

if __name__ == '__main__':
    semeval_frames = set()
    semeval_test_frames = Counter()
    semeval_test_dni_frames = Counter()
    test_paths = []
    for fname in pb_semeval.test2gold.values():
        test_paths.append(os.path.join(pb_semeval.gold_path, fname))
        
    doc = Document(pb_semeval.train_path, Pbparser())
    for frame_elem in doc.frames:
        semeval_frames.add(frame_elem.get('name')) 
    for path in test_paths:
        doc = Document(path, Pbparser())
        for frame_elem in doc.frames:
            dnis = [fe_elem for fe_elem in frame_elem.iterfind('fe')
                    if (fe_elem.find("flag[@name='Definite_Interpretation']") is not None and
                        fe_elem.find('fenode[@idref]') is not None)]
            if len(dnis) > 0: # found a DNI
                semeval_test_dni_frames[frame_elem.get('name')] += 1
            semeval_test_frames[frame_elem.get('name')] += 1
    semeval_frames.update(semeval_test_frames)
    
    ontonotes_frames = set()        
    for i, base_path in enumerate(ontonotes.list_docs(ontonotes_en)):
        doc = OntonotesDocument(base_path)
        found_examples_in_doc = False
        for sent, tok_id, f in doc.iter_frames():
            if len(f) >= 3:
                frame = f[0]
                ontonotes_frames.add(frame)
#         if i >= 10: break # for debugging
                
    print('DNI Frames: ' + str(semeval_test_dni_frames))
    print('Unique=%d, count=%d' %(len(semeval_test_dni_frames), 
                                  sum(semeval_test_dni_frames[k] for k in semeval_test_dni_frames)))
                
    print('Overlap: %d' %len(ontonotes_frames.intersection(semeval_frames)))
    print('+OntoNotes, -SemEval: %d' %len(ontonotes_frames.difference(semeval_frames)))
    missing_frames = semeval_frames.difference(ontonotes_frames)
    missing_test_frames = set(semeval_test_frames).difference(ontonotes_frames)
    missing_test_dni_frames = set(semeval_test_dni_frames).difference(ontonotes_frames)
    print('-OntoNotes, +SemEval(train+test): %d' %len(missing_frames))
    print('-OntoNotes, +SemEval(test): %d (count=%d)' 
          %(len(missing_test_frames), sum(semeval_test_frames[f] for f in missing_test_frames)))
    print('-OntoNotes, +SemEval(test,DNI): %d (count=%d)' 
          %(len(missing_test_dni_frames), sum(semeval_test_dni_frames[f] for f in missing_test_dni_frames)))
    print('Missing frames:')
    print('\t' + '\n\t'.join(semeval_frames.difference(ontonotes_frames)))
    