

## 4 Jan

Set up experiments again on HPC Cloud: `ubuntu@145.100.58.232` (following `setup-vm.txt`). I created 
a VM with GPU but it wasn't accessible right after birth.

Imported data. Imported ontonotes dependency trees. Ran `python3 count_lemma_frequency_ontonotes.py`.
Committed and ran it again:

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ screen
    [detached from 8425.pts-0.packer-Ubuntu-16]

Finished. Started `python3 -m pair_models.extract_ontonotes_frames` in another
screen. Detached:

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ screen
    [detached from 8636.pts-0.packer-Ubuntu-16]

Finished.

I could start `exp_coherence_model` now. Let's make some models to enabler other 
experiments:

    python3 -u -m pair_models.exp_coherence_model 872642 && \
    python3 -u -m pair_models.exp_coherence_model 927242 && \
    python3 -u -m pair_models.exp_coherence_model 82625823 && \
    python3 -u -m pair_models.exp_coherence_model 28283 

Detached. Hopefully they will finish by tomorrow morning.

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ screen
    [detached from 9522.pts-0.packer-Ubuntu-16]

Found backed up results on DAS-5 at `/home/minhle/scratch/isrl-sp/out/2017-10-03-working`.
I can compare old and new files to check if the results are stable.

Accidentally terminated my programs when I hit Ctrl-C. Restarted:

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ cat out/temp-2018-01-04.sh
    python3 -u -m pair_models.exp_coherence_model 872642 && \
         python3 -u -m pair_models.exp_coherence_model 927242 && \
         python3 -u -m pair_models.exp_coherence_model 82625823 && \
         python3 -u -m pair_models.exp_coherence_model 28283
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ nohup out/temp-2018-01-04.sh 2>&1 > out/temp-2018-01-04.sh.out &
    [1] 9907
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ tail -f out/temp-2018-01-04.sh.out
    ...
    Best model saved into out/2018-01-04-7610bfa/872642/coherence-model.pkl
    Epoch 10:
        Train NLL: 2.871194
        Train accuracy: 0.163500
        Valid NLL: 2.894394
        Valid accuracy: 0.167182
    ...

## Fri 5 Jan

The random seeds I ran and ones that were used for LREC weren't the same. I can
still compare the performance though. Everything in the new batch is better :-O
Why??

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ grep -A 2 True out/2018-01-04-7610bfa/*/*.out
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out:True Positives: 8
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out-False Positives: 26
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out-False Negatives: 48
    --
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out:True Positives: 19
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out-False Positives: 32
    out/2018-01-04-7610bfa/28283/exp_coherence_model.py.out-False Negatives: 58
    ...
    [minhle@fs0 ~]$ grep -A 2 True /home/minhle/scratch/isrl-sp/out/2017-10-03-working/*/*coherence_model*.out
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out:True Positives: 5
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out-False Positives: 29
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out-False Negatives: 51
    --
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out:True Positives: 12
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out-False Positives: 39
    /home/minhle/scratch/isrl-sp/out/2017-10-03-working/1225/exp_coherence_model.py.out-False Negatives: 65
    ...

Started one with exactly the same random seed to double-check.

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ nohup python3 -u -m pair_models.exp_coherence_model 1225 2>&1 >out/exp_coherence_model.1225.out &
    [1] 11049
 
Comparing two versions: `git difftool af0593f`

1. some changes about INI in `data.py` but the INIs in the output are the same
2. new func: `traverse_tree` in `ontonotes.py`
3. Found a pronounced difference: all of the new models are trained for a full 1000 epochs
but it wasn't because I had disabled early-stopping. They found improvements well into
900s-th epochs.

Turned out the folder `2017-10-03-working` doesn't contain the reported results.
Training accuracy is conspicuously high (>90%) and validation accuracy is low
(<30%). Probably this is a test run with a reduced training set. Sigh...
So I wasted most of today checking a non-existing problem. 

Moved to a new VM that has 16 CPUs so that experiments will run more quickly:
`ubuntu@145.100.58.98`. Run everything I did before LREC submission:

    nohup scripts/run-before-lrec.job &
    
## Mon 8 Jan

Got somewhat similar results compared to what's in the paper (slightly lower,
but don't affect the conclusions). Results are stored in the folder:

    out/2018-01-05-working

All `coherence_with_synsem_features` experiments failed because of arithmetic 
overflow (`inf`). I deceased the learning rate and tried this:

    nohup python3 -m pair_models.exp_coherence_with_synsem_features 59061 >out/test-lr.out 2>&1 &
    
Tried to reduce learning rate but I recall that I could also clip the gradients.
NLL can still become `inf` but as long as the gradients are safe-guarded, `inf`
won't creep into my parameters and my model is safe too. It means I could 
actually increase the learning rate of other models (but I won't do it because
it might create a mess). Now I'll rerun everything from scratch. I'll use a new
VM to work on further experiments. 

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ python3 -c 'import isrl; print(isrl.version)'
    2018-01-08-ca2b84f
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ nohup scripts/run-before-lrec.job &

Started working on a script to compare output files. Tried to get a new, smaller VM
but it's pending.

## Tue 9 Jan

Got a new VM at `145.100.57.72`. Set it up, used it to test new code that trains
faster. Turns out, clipping can be expensive. I guess that gradients used
to be stored sparsely and clipping makes it dense. Therefore, if you clip
for each example, training suddenly becomes much more expensive.

After fixing, models suddenly get 100% accuracy on both training and validating
sets of OntoNotes (they only get slightly better on SemEval). Clearly the 
training problem has become too easy. I created a new list of validate documents
in the branch `new-valid-set` but changing the preprocessing code and
the training code is just to expensive now. I left it as it was and reran everything:

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ python3 -c 'import isrl; print(isrl.version)'
    2018-01-09-5b09a06
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ nohup scripts/run-before-lrec.job &
    [1] 330

## Wed 10 Jan

To make the training a bit harder, another solution is to include all tokens in the
document as candidates but that would be too much so I use two previous sentences.
This is also more similar to how the model is tested. 

I feel like I'm going down this rabbit hole again.
All the tiny adjustments will eat up my time and in the end I won't have a good
paper. Let's just stop here. Now.

Analyze the output and found this interesting case where the model works as I expected:

[You]\_coherence\_model\_predicted know the lie of the land best .
Creep forward quietly and see what [they]\_predicate\_only\_model\_predicted are doing - - but for heaven's sake don't [let]_predicate them know that they are watched ! " 

## Thu 11 Jan

... but actually it didn't work as expected because A1 was the full clause 
"them know that they are watched" ("them" plays the role of subject) but not just "them".
So the model actually only looks at the head, which is "know". It doesn't make sense
any more.

Started experiments yet again on 145.100.58.98. 
Because it takes only one day to finish, I can repeat it dozens of times :D

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ python3 -c 'import isrl; print(isrl.version)'
    2018-01-11-39bb50e
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ nohup scripts/run-before-lrec.job &
    [1] 20633

Really start working on ON5V now... It's a challenge...

Recovered the results Feizabadi and Pado reported in the paper:

    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ mkdir out/2ae722f142454db4e1dd796087ae220bcb24aa7e/
    ubuntu@packer-Ubuntu-16:/data/isrl-sp$ python3 exp-feizabadi-pado.py 6 | less
    
    True Positives: 10
    False Positives: 17
    False Negatives: 46
    
    True Positives: 18
    False Positives: 34
    False Negatives: 59

## Fri 12 Jan

Finished the full set of neural experiments. The results are similar to what I've
seen before.

    \begin{tabular}{lllllll}
    \hline
     expriment                             & mean(P)            & mean(R)            & mean(F1)           & std(P)                 & max(F1)            & num\_attempts \\
     exp\_predicate\_only.py                 & 36.23529411764706  & 23.157894736842106 & 28.256880733944957 & 1.6816791541122342     & 32.11009174311926  & 15           \\
     exp\_coherence\_model.py                & 31.529411764705877 & 20.15037593984962  & 24.587155963302752 & 1.2623710972813647     & 27.522935779816514 & 15           \\
     exp\_synsem.py                         & 18.745098039215687 & 11.979949874686717 & 14.617737003058103 & 1.4780484371369498     & 18.348623853211006 & 15           \\
     exp\_predicate\_with\_synsem\_features.py & 29.01960784313726  & 18.546365914786964 & 22.629969418960247 & 4.5891942464352455     & 27.522935779816514 & 15           \\
     exp\_coherence\_with\_synsem\_features.py & 20.000000000000007 & 12.781954887218047 & 15.59633027522936  & 2.7755575615628914e-15 & 15.596330275229356 & 15           \\
    \hline
    \end{tabular}

Apply Silber&Frank's filter on candidates. New problem arises: many roles
don't have a correct filter. Let's fix this. Options:

1. weighted positive examples (the weight is Dice coefficient)
2. one best filter per role (Dice coefficient) --> need to write more code...
3. one best filter per role (smallest span that covers the correct head) --> need to write more code...
4. unrestricted (possibly) many correct filters per role --> that would make 
S (sentence) always be a correct answer

Despite the changes, the results are still crappy.

## Fri 26 Jan

Investigate why I keep getting 0 F1. The first time I use Jupyter Notebook
and seaborn to debug a machine learning system. It's quite convenient actually :D

After training, the Naive Bayes classifier assign all examples in *training set*
to the negative class :-O Found two problems:

- Data file for "role percentage in OntoNotes" was empty
- The program throws away 2/3 of positive cases (and the associated negative cases), i.e. 132 cases
- But there are only 17 warnings    

The most important thing I fixed was removing "relation tags". An excerpt from
[this papge](https://www.clips.uantwerpen.be/pages/mbsp-tags) says:

> Relations tags describe the relation between different chunks, and clarify the role of a chunk in that relation. The most common roles in a sentence are SBJ (subject noun phrase) and OBJ (object noun phrase).

Before removing relation tags, the candidate filter accepts only 4 constituents:
`NP`, `VP`, `S`, `SBAR` but after, it accepts 53, e.g. `NP-1`, `NP-2`, `NP-SBJ-9`.
4 of the new values even end up in the list of "10 most informative features":
`NP-LGS`, `NP-PRD`, `NP-SBJ-1`, `NP-SBJ-2`.

Fix things...
Stop and go to bed at commit `3e2939f` with these results:

    P = [0.16666666666666666, 0.375, 0.25, 0.058823529411764705, 0.1, 0.16666666666666666, 0.0, 0.2, 0.08333333333333333, 0.16666666666666666]
    R = [0.1111111111111111, 0.2, 0.13636363636363635, 0.043478260869565216, 0.045454545454545456, 0.08333333333333333, 0.0, 0.125, 0.04, 0.09090909090909091]
    F1 = [0.13333333333333333, 0.2608695652173913, 0.1764705882352941, 0.05, 0.0625, 0.1111111111111111, 0, 0.15384615384615385, 0.05405405405405406, 0.11764705882352941]

At this point, the training example extraction phase still remove 30% of positive examples.

## Sat 27 Jan

Try adding the positive examples anyway, improved the F1 slightly to 13% in average.

Added ON5V evaluation to neural model experiements. Testing...

## Sun 28 Jan

Start experiments:

    [minhle@fs0 isrl-sp]$ python3 -c 'import isrl; print(isrl.version)'
    2018-01-28-8b51285
    [minhle@fs0 isrl-sp]$ sbatch scripts/run-before-lrec.job
    Submitted batch job 1745948

## Thu 1 Feb

The experiments finished on Sunday evening (~ half a day). 
Wrote `compare_models_on5v.py` to summarize the results
in a table. Pandas is so convenient!

Execute exp-baseline on ON5V. There're some errors.

## Fri 2 Feb

Found out why F&P is suddenly better: I used Silberer and Frank's filter for 
F&P but not for neural solvers.

    [minhle@fs0 isrl-sp]$ python3 -c 'import isrl; print(isrl.version)'
    2018-02-02-8c872ef
    [minhle@fs0 isrl-sp]$ python3 -m feizabadi_pado.exp_dni_on5v

Found out another problem of the reporting script: it ignores the full zeros. 
When they're taken into account, the results get even lower.

Turned out the baseline is wrong. I suspected the 100% precision but didn't
check it carefully :-( It didn't compute any prototypical vector because it
mistake the head index for head tokens. It does stuff now. Rerun:

    [minhle@fs0 isrl-sp]$ python3 -c 'import isrl; print(isrl.version)'
    2018-02-02-8ec9b8d
    [minhle@fs0 isrl-sp]$ python3 exp-baseline.py

I still don't know why MultiWay models aren't as effective.
Went through all the log files again and one thing strucks me: mostly all 
MultiWay models were trained for the maximum number of epochs while
OneWay models were trained for only around 600 epochs. Increasing learning rate
will make MultiWay models shoot to 100% on both training and validating sets. 
It's very suspicious... How come it gets all correct on an unseen set? Besides,
when tested on ON5V, the same model yields low performance.
I don't want go down this rabbit hole again, same as my notes on 10 Jan.

Update: check that other_roles doesn't include the right answer.

    In [3]:
    other_roles
    
    Out[3]:
    array([[    3,     4,     4, ...,     6,     7,     7],
           [    8,     9,     9, ...,    11,    12,    12],
           [    1,    13,    13, ...,    15,    16,    16],
           ...,
           [  981, 53162, 53162, ...,    10,     7,     7],
           [    8, 11703, 11703, ...,    10,    24,    25],
           [  175,  2327,  2327, ...,    10,     7,     7]])
    
    In [10]:
    ex0 = examples[0]
    other_roles[ex0[cols.EXAMPLE_OTHER_ROLES_START]:ex0[cols.EXAMPLE_OTHER_ROLES_END], :3]

    In [14]:
    candidates[ex0[cols.EXAMPLE_CANDIDATE_START]:ex0[cols.EXAMPLE_CANDIDATE_END], :2]
    
    Out[14]:
    array([[13, 13],
           [17, 17],
           [19, 19],
           [22, 22],
           [26, 26],
           [28, 28],
           [30, 30],
           [31, 31],
           [32, 32],
           [36, 36],
           [38, 38],
           [ 4,  4],
           [41, 41],
           [44, 44],
           [47, 47],
           [ 9,  9],
           [49, 49],
           [53, 53],
           [56, 56],
           [59, 59],
           [60, 60],
           [62, 62],
           [65, 65],
           [66, 66],
           [69, 69],
           [71, 71],
           [73, 73]])    
           
    Out[10]:
    array([[3, 4, 4],
           [8, 9, 9]])
    
    In [11]:
    ex1 = examples[1]
    other_roles[ex1[cols.EXAMPLE_OTHER_ROLES_START]:ex1[cols.EXAMPLE_OTHER_ROLES_END], :3]
    
    Out[11]:
    array([[ 1, 13, 13],
           [ 8,  9,  9]])
    
    In [12]:
    ex2 = examples[2]
    other_roles[ex2[cols.EXAMPLE_OTHER_ROLES_START]:ex2[cols.EXAMPLE_OTHER_ROLES_END], :3]
    
    Out[12]:
    array([[ 1, 13, 13],
           [ 3,  4,  4]])

## Sun 4 Feb

Upgraded my Theano version on DAS-5 by mistake. Now I don't know what version
I used to create my old models and they ain't compatible. Started retraining
just one model for the sake of development. I'll need to retrain everything. 

    [minhle@fs0 isrl-sp]$ git checkout 8b51285
    HEAD is now at 8b51285... ready to test neural nets on ON5V
    [minhle@fs0 isrl-sp]$ nohup python3 -m pair_models.exp_coherence_model 79861 &
    [1] 12941

## Mon 5 Feb

Fixed an error that makes scripts always get 100% on training set. Now I need
to retrain all models before running them on training set.

    [minhle@fs0 isrl-sp]$ git checkout 8b51285
    HEAD is now at 8b51285... ready to test neural nets on ON5V
    [minhle@fs0 isrl-sp]$ sbatch scripts/run-before-lrec.job
    Submitted batch job 1773476
    
Tomorrow I'll run the models. The command looks like this:
    
    git checkout 23021a0
    python3 -m pair_models.exp_coherence_model 12 --model=out/2018-01-28-8b51285/79861/coherence-model.pkl

## Fri 9 Feb

Generated output of models. It took longer than I expected. Just running all the
models through SemEval and ON5V took well over 3 hours.

## Sat 10 Feb

Found a problem with the code:
Why did I restrict the models to predominant roles 
(see `BaseDNIModelWrapper` and `DNIWrapper`)
while the role is already known to need a filler?

Added discussion to our LREC paper (finally!) It's only 5 days before the deadline.

## Fri 16 Feb

Proof-read. I could have tried to use the full span (e.g. by averaging) instead
of only the head word of candidates. Or even using a LSTM as in Do et al. (2017).
It's not possible now because I would need to change the architecture. 

## Wed 21 Feb

After a long day attending meetings, now I'm fixing a bug, 1 day before the deadline.
Antske found that many DNIs are missing and it turned out to be due to
[one line](https://bitbucket.org/cltl/isrl-sp/src/b514682d610b080316cac65cd6355a1b55c92ed4/pair_models/__init__.py?at=master&fileviewer=file-view-default#__init__.py-14) 
I copied from the Feizabadi&Pado's model. It's so stupid.

Finished. Now F&P is better than our models so we need to update the discussion again.

