import os
import shutil
import sys
from nltk.tokenize import sent_tokenize
import re
from html2text import html2text
import codecs

clmet_path = 'data/clmet'
output_path = 'out/clmet.preprocessed.txt'

def process_file(in_path, f2):
    print('processing %s...' %in_path)
    with codecs.open(in_path, 'rt', encoding='utf8', errors='replace') as f:
        content = f.read()
        if '<div' in content or '<p' in content:
            content = html2text(content)
        # remove copyright information
        m = re.search(r"\*+\s+END OF THE PROJECT GUTENBERG", content, flags=re.IGNORECASE)
        if m:
            content = content[:m.start()]
        m = re.search(r"\*END.THE SMALL PRINT.+\*END\*", content, flags=re.IGNORECASE)
        if m:
            content = content[m.end():]
        m = re.search(r"(this etext|these ebooks) was prepared by.+\n", content, flags=re.IGNORECASE)
        if m:
            content = content[m.end():]
        # remove book/chapter/section titles, footnotes, etc.
        # assume that only big paragraphs contain content
        paragraphs = re.split(r'(?:\s*\r?\n){2,}', content.strip())
        for i in range(len(paragraphs)-1, -1, -1):
            if len(re.split(r'\r?\n', paragraphs[i])) <= 2:
                del paragraphs[i]
        content = '\n'.join(paragraphs)
        # miscellaneous
        content = re.sub(r'^\s*\d+\s*$', '', content, flags=re.MULTILINE) # page number
        content = re.sub(r'&[rl]dquo;', '"', content)
        content = re.sub(r'\[\d+\.?\]', '', content)
        content = re.sub(r'\[[\*\+]\]', '', content)
        content = re.sub(r'\[Footnote(\s*\d+)?[:\.].+\]', '', content, flags=re.DOTALL)
        content = re.sub(r'\{\d+[\.\w]?\}', '', content)
        content = re.sub(r'\{See .+\}', '', content, flags=re.DOTALL)
        content = re.sub('[\r\n\t]', ' ', content)
        sents = sent_tokenize(content)
        for s in sents:
            f2.write(s)
            f2.write('\n')

with codecs.open(output_path, 'wt', encoding='utf8') as f2:
    for root, _, fnames in os.walk(clmet_path):
        for fname in fnames:
            if re.search(r'\.txt$', fname):
                in_path = os.path.join(root, fname)
                process_file(in_path, f2)
    print('written to %s.' %output_path)
                