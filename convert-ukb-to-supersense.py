from nltk.corpus import wordnet
import fileinput
import re
import sys

f = fileinput.input()
f.readline() # ignore first line
for line in f:
    fields = re.split(' +', line)
    offset, pos = fields[2].split('-')
    synset = wordnet._synset_from_pos_and_offset(pos, int(offset))
    supersense = synset.lexname()
    fields[2] = supersense
    sys.stdout.write(' '.join(fields))