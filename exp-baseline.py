"""Run experiments on the baseline model (prototypical vectors) 

Usage:
  exp-baseline.py [--model=<path>]
"""
from data import read_embeddings
from collections import defaultdict
import numpy as np
from scipy.spatial.distance import cosine
import ontonotes
from ontonotes import ontonotes_en, OntonotesDocument
from pair_models import BaseDNIModelWrapper
from docopt import docopt
import os
import pickle

word2id, embeddings = read_embeddings()

def compute_prototypical_fillers_from_ontonotes():
    ''' Return a mapping (predicate, role) --> vector computed on OntoNotes '''
    vectors = defaultdict(list)
    for base_path in ontonotes.list_docs(ontonotes_en):
        doc = OntonotesDocument(base_path)
        for f in doc.iter_frames():
            for fe in f.frame_elements:
                filler_vectors = []
                for h in fe.filler_heads:
                    word = doc.deps()[f.sent][h]['token'].lower()
                    if word in word2id:
                        filler_vectors.append(embeddings[word2id[word]])
                if filler_vectors:
                    vectors[(f.predicate, fe.role)].extend(filler_vectors)
    assert vectors
    return {k: np.vstack(vectors[k]).mean(axis=0) for k in vectors}

class ModelWrapper(BaseDNIModelWrapper):
    
    def __init__(self, prototypical_vectors):
        self.prototypical_vectors = prototypical_vectors
        super(ModelWrapper, self).__init__(None, None)
    
    def predict(self, doc, frame, pos, role, overt_roles, overt_fillers, candidates):
        pred = frame.get('name')
        pvector = self.prototypical_vectors.get((pred, role))
        if pvector is None: 
            print('Role not found: (%s, %s)' %(pred, role))
            return None
        candidate_heads = [doc.head_word(arg.get('id')) for arg in candidates]
        distances = [cosine(embeddings[word2id[h]], pvector)
                     if h in word2id else 1 for h in candidate_heads]
        return np.argmin(distances)

if __name__ == '__main__':
    arguments = docopt(__doc__)
    import isrl
    isrl.setup_experiment(None)
    model_path = arguments.get('--model')
    if not model_path:
        prototypical_vectors = compute_prototypical_fillers_from_ontonotes()
        model_path = os.path.join(isrl.out_dir, 'prototypical_vectors.npy')
        with open(model_path, 'wb') as f:
            pickle.dump(prototypical_vectors, f)
    else:
        with open(model_path, 'rb') as f:
            prototypical_vectors = pickle.load(f)
    m = ModelWrapper(prototypical_vectors)
    model_name = 'baseline'
    isrl.pb_semeval.resolve_dnis(m, model_name)
    isrl.on5v_cv.resolve_dnis(m, model_name)
    