import re
from collections import defaultdict
import codecs

def _parse_dep_tree(lines):
    tree = []
    for line in lines:
        fields = line.strip().split('\t')
        tree.append({'token': fields[1], 
                     'pos': fields[3],
                     'head': int(fields[6])-1,
                     'label': fields[7]})
    n = len(tree)
    count = [0] * n
    for i in range(n):
        tree[i]['tokens'] = set()
        if tree[i]['head'] >= 0:
            count[tree[i]['head']] += 1
    tokens_to_process = [i for i in range(n) if count[i] == 0]
    while len(tokens_to_process) > 0:
        i = tokens_to_process.pop()
        if count[i] == 0:
            tree[i]['tokens'].add(i)
            if tree[i]['head'] >= 0:
                h = tree[i]['head']
                tree[h]['tokens'].update(tree[i]['tokens'])
                count[h] -= 1
                if count[h] == 0:
                    tokens_to_process.append(h)
    return tree

def load_trees(path):
    trees = []
    with codecs.open(path, 'r', 'utf-8') as f:
        lines = []
        for line in f:
            if re.match(r'\r?\n', line):
                trees.append(_parse_dep_tree(lines))
                lines = []
            else:
                lines.append(line)
        if len(lines) > 0:
            trees.append(_parse_dep_tree(lines))
    return trees

def find_syntactic_head(token_ids, dep):
    if not isinstance(token_ids, set):
        token_ids = set(token_ids)
    minimum_containing_subtree_head = None
    for h in range(len(dep)):
        if dep[h]['tokens'] == token_ids:
            return h
        else:
            if dep[h]['tokens'].issuperset(token_ids) and h in token_ids:
                if (minimum_containing_subtree_head is None or 
                    len(dep[minimum_containing_subtree_head]['tokens']) > len(dep[h]['tokens'])):
                    minimum_containing_subtree_head = h
    return minimum_containing_subtree_head

specificity = defaultdict(int, [('-NONE-', -5), ('IN', -4), ('RB', -3), 
                                ('PRP$', -2), ('PRP', -1), ('DT', -1),
                                ('VB', 1), ('VBN', 1), ('VBG', 1), ('NN', 1), ('NNS', 1), ('NNP', 1)])

def find_head(token_ids, dep):
    ''' Return the index of head word.
    This function includes some special treatment for CoNLL data to avoid 
    uninformative head such as "to", "with", etc.
    '''
    h = find_syntactic_head(token_ids, dep)
    if h is not None and dep[h]['pos'] in ['IN', 'RB']:
        children = [c for c in range(len(dep)) if dep[c]['head'] == h]
        if len(children) > 1:
#                 sys.stderr.write('multiple dependents of "%s": %s\n' 
#                                  %(dep[h]['token'], 
#                                    str([dep[c]['token'] for c in children])))
            c = max(children, key=lambda c:specificity[dep[c]['pos']])
            return c
        if len(children) > 0:
            return children[0]
#             else:
#                 if dep[h]['pos'] == 'IN':
#                     sys.stderr.write('preposition without dependent: %s\n' %dep[h]['token'])
#                     return '__INGORE__'
    return h

def find_tokens(root, dep):
    ''' Find all tokens that are dominated by this root. The reverse of @find_head. '''
    tokens = []
    stack = [root]
    while stack:
        head = stack.pop()
        tokens.append(head)
        for i in range(len(dep)):
            if dep[i]['head'] == head:
                stack.append(i)
    return sorted(tokens)


def add_syntax_tags(doc_table_rows, leaf_pos, tree, dep):
    # add syntax tags
    col = len(doc_table_rows[0])
    for i in range(len(leaf_pos)):
        p = leaf_pos[i]
        for k in range(len(p)-2):
            tree[p[:k]].last_token = i
    for i in reversed(range(len(leaf_pos[i]))):
        p = leaf_pos[i]
        for k in range(len(p)-2):
            tree[p[:k]].first_token = i
    for st in tree.subtrees():
        if st.height() >= 3:
            tag = '(' + st.label()
            if dep is not None:
                head = find_head(range(st.first_token, st.last_token+1), dep)
                tag += '%d:' %(head+1)
            if len(doc_table_rows[st.first_token]) > col:
                doc_table_rows[st.first_token][col] += ' ' + tag
            else:
                doc_table_rows[st.first_token].append(tag)
    for st in tree.subtrees():
        if st.height() >= 3:
            if len(doc_table_rows[st.first_token]) > col:
                doc_table_rows[st.first_token][col] += ')'
            else:
                doc_table_rows[st.first_token].append('*)')
    for i in len(doc_table_rows):
        if len(doc_table_rows[i]) <= col:
            doc_table_rows[i].append('*')

def add_syntax_tree(doc_table_rows, tree, dep):
    ''' Add syntax without heads and syntax with heads as two columns in 
    <code>doc_table_rows</code>. The format is described in file
    Semeval2010Task10TrainingFN+PB/Semeval2010Task10TrainingPB/Semeval2010.Task10.TrainingData.PB.readme
    '''
    assert dep is not None
    leaf_pos = [tree.leaf_treeposition(i) for i in range(len(tree.leaves()))]
    leaf_pos = [p for p in leaf_pos if tree[p[:-1]].label() != '-NONE-']
    assert len(doc_table_rows) == len(leaf_pos)
    # add POS tags
    for i in range(len(leaf_pos)):
        p = leaf_pos[i]
        doc_table_rows[i].append(tree[p[:-1]].label())
    # add syntax tags
    add_syntax_tags(doc_table_rows, leaf_pos, tree, None)
    add_syntax_tags(doc_table_rows, leaf_pos, tree, dep)
